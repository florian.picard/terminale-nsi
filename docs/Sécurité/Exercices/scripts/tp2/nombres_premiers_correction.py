def nombres_premiers(n):
    """ int -> [int]
    Renvoie la liste des nombres premiers inférieurs ou égaux à n """
    if n <= 1:
        return []
    table = [True for _ in range(n + 1)]
    table[0], table[1] = False, False
    for i in range(2, n + 1):
        if not table[i]:
            continue
        for j in range(2*i, n + 1, i):
            table[j] = False
    return [i for i in range(n + 1) if table[i] == True]
