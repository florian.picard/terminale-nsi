class Graphe:
    """ Un graphe représenté par une matrice d'adjacence.
    Les sommets sont les nombres 0, 1, ..., n - 1. """
    def __init__(self, n):
        self.n = n
        self.adj = [[False]*n for _ in range(n)]

    def ajouter_sommet(self):
        """ Graphe -> Nonetype
        Ajoute un sommet au graphe """
        for row in self.adj:
            row.append(False)
        self.adj.append([False]*(self.n + 1))
        self.n += 1

    def ajouter_arc(self, source, destination):
        """ Graphe, int, int -> int
        Ajoute l'arc source -> destination """
        self.adj[source][destination] = True

    def supprimer_sommet(self, i):
        """ Graphe, int -> Nonetype
        Supprime le sommet i du graphe. Les sommets j > i sont renommés en j - 1. """
        for row in self.adj:
            row.pop(i)
        self.adj.pop(i)
        self.n -= 1

    def supprimer_arc(self, source, destination):
        """ Graphe, int, int -> Nonetype
        Supprime l'arc source -> destination """
        self.adj[source][destination] = False

    def est_voisin(self, source, destination):
        """ Graphe, int, int -> bool
        Détermine si source et destination sont voisins """
        return self.adj[source][destination]

    def sommets(self):
        """ Graphe -> [int]
        Renvoie la liste des sommets du graphe self """
        return [i for i in range(self.n)]

    def voisins(self, sommet):
        """ Graphe, int -> [int]
        Renvoie la liste des voisins de sommet """
        voisins = []
        for s in self.sommets():
            if self.est_voisin(sommet, s):
                voisins.append(s)
        return voisins

    def liste_arcs(self):
        """ Graphe -> [(int, int)]
        Renvoie la liste des arcs du graphe """
        arcs = []
        S = self.sommets()
        for source in S:
            for destination in S:
                if self.est_voisin(source, destination):
                    arcs.append((source, destination))
        return arcs

    def ordre(self):
        """ Graphe -> int
        Renvoie l'ordre du graphe  """
        return self.n

    def taille(self):
        """ Graphe -> int
        Renvoie la taille du graphe """
        return len(self.liste_arcs())

    def decrire(self):
        """ Graphe -> Nonetype
        Affiche une description du graphe self """
        print(f"Graphe d'ordre {self.ordre()} et de taille {self.taille()}")
        for s in self.sommets():
            print(f"Sommet {s} -> {' '.join(str(i) for i in self.voisins(s))}")

class Graphe:
    """ Un graphe représenté par un dictionnaire d'adjacence. """
    def __init__(self, n):
        self.n = n
        self.adj = [[False]*n for _ in range(n)]

    def ajouter_sommet(self, s):
        """ Graphe, str -> Nonetype
        Ajoute le sommet s au graphe self """
        # self.adj.setdefault(source, [])
        if s not in self.adj:
            self.adj[s] = []

    def ajouter_arc(self, source, destination):
        """ Graphe, str, str -> Nonetype
        Ajoute l'arc source -> destination au graphe self """
        self.adj[source].append(destination)

    def est_voisin(self, source, destination):
        """ Graphe, str, str -> bool
        Détermine si source et destination sont voisins """
        for v in self.adj[source]:
            if v == destination:
                return True
        return False
    
    def sommets(self):
        """ Graphe -> [str]
        Renvoie la liste des sommets du graphe self """
        return list(self.adj.keys())
    
    def voisins(self, s):
        """ Graphe, stsr -> [str]
        Renvoie la liste des voisins de sommet """
        return self.adj[s]
    
    def liste_arcs(self):
        """ Graphe -> [(str, str)]
        Renvoie la liste des arcs du graphe """
        arcs = []
        for u in self.sommets():
            for v in self.adj[u]:
                arcs.append((u, v))
        return arcs
    
    def supprimer_arc(self, source, destination):
        """ Graphe, int, int -> Nonetype
        Supprime le ou les arcs source -> destination """
        for i, v in enumerate(self.adj[source]):
            if v == destination:
                self.adj[source].pop(i)
            
    def supprimer_sommet(self, sommet):
        """ Graphe, str -> Nonetype
        Supprime le sommet i du graphe. """
        for s in self.sommets():
            self.supprimer_arc(s, sommet)
        self.adj.pop(sommet)
    
    def ordre(self):
        """ Graphe -> int
        Renvoie l'ordre du graphe  """
        return len(self.adj)
    
    def taille(self):
        """ Graphe -> int
        Renvoie la taille du graphe """
        return len(self.liste_arcs())
    
    def decrire(self):
        """ Graphe -> Nonetype
        Affiche une description du graphe self """
        print(f"Graphe d'ordre {self.ordre()} et de taille {self.taille()}")
        for s in self.sommets():
            print(f"Sommet {s} -> {' '.join(str(i) for i in self.voisins(s))}")

