def test_indices_occurrences():
    # Tests pour la fonction indices_occurrences
    assert indices_occurrences([], 0) == []
    assert indices_occurrences([0, 1, 2, 3], 0) == [0]
    assert indices_occurrences([0, 1, 2, 0], 0) == [0, 3]
    assert indices_occurrences([0, 1, 2, 3], 2) == [2]
    assert indices_occurrences([0, 1, 2, 3], 4) == []
    return True

# from tp import indices_occurrences
test_indices_occurrences()
