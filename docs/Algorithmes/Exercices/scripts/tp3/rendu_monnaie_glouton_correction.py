def rendu_monnaie_glouton(systeme, montant):
    """ [int], int -> int
    Renvoie le nombre de pièces minimales à utiliser pour rendre
    le montant avec les pièces de systeme """
    nb_pieces = 0
    while montant > 0:
        # On cherche la plus grande pièce à rendre
        plus_grande = systeme[0]
        for p in systeme:
            if p <= montant and p > plus_grande:
                plus_grande = p
        # on rend la pièce trouvée 
        montant -= plus_grande
        nb_pieces += 1
    return nb_pieces
