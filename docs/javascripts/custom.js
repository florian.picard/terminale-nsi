 document.addEventListener("DOMContentLoaded", function () {
    let exoCounter = 1;
    const exoElements = document.querySelectorAll(".exo");

    exoElements.forEach((element) => {
        // Create an <h2> element
        const heading = document.createElement("p");
        heading.textContent = `Exercice ${exoCounter} : `;

        // Insert the <h2> element before the .exo element
        element.parentNode.insertBefore(heading, element);

        exoCounter++;
    });
});
