def test_exemple():
    """ Tests pour la fonction exemple """
    print("Tests de exemple passés avec succès.")
    return True

# from tp import exemple
# test_exemple()
def test_creer_vide():
    """ Tests pour la fonction creer_vide """
    assert creer_vide() is None
    print("Tests de creer_vide passés avec succès.")
    return True

# from tp import creer_vide
# test_creer_vide()
def test_est_vide():
    """ Tests pour la fonction est_vide """
    assert est_vide(creer_vide()) == True
    assert est_vide(Maillon(1, creer_vide())) == False
    print("Tests de est_vide passés avec succès.")
    return True

# from tp import est_vide
# test_est_vide()
def test_tete():
    """ Tests pour la fonction tete """
    assert tete(Maillon(1, None)) == 1
    assert tete(Maillon(2, Maillon(1, None))) == 2
    print("Tests de tete passés avec succès.")
    return True

# from tp import tete
# test_tete()
def test_queue():
    """ Tests pour la fonction queue """
    m0 = Maillon(1, None)
    m1 = Maillon(2, m0)
    m2 = Maillon(3, m1)
    assert queue(m1) is m0
    assert queue(m2) is m1
    print("Tests de queue passés avec succès.")
    return True

# from tp import queue
# test_queue()
def test_ajoute_debut():
    """ Tests pour la fonction ajoute_debut """
    assert ajoute_debut(None, 3).valeur == 3
    assert ajoute_debut(None, 3).suivant == None
    print("Tests de ajoute_debut passés avec succès.")
    return True

# from tp import ajoute_debut
# test_ajoute_debut()
def message(input, output, expected):
    return f"Erreur :\n Entrée : {input}\n Sortie : {output}\n Attendu : {expected}"

def tab2maillon(tab):
    maillon = None
    for i in range(len(tab) - 1, -1, -1):
        maillon = Maillon(tab[i], maillon)
    return maillon

def égaux(m1, m2):
    if est_vide(m1) and est_vide(m2):
        return True
    elif est_vide(m1) ^ est_vide(m2):
        return False
    return tete(m1) == tete(m2) and égaux(queue(m1), queue(m2))

def test_longueur():
    """ Tests pour la fonction longueur """
    inputs = [tab2maillon([i for i in range(n)]) for n in range(10)]
    outputs_i = [longueur_i(m) for m in inputs]
    outputs_r = [longueur_r(m) for m in inputs]
    expecteds = [n for n in range(10)]
    for i in range(10):
        assert outputs_i[i] == expecteds[i], message(inputs[i], outputs[i], expecteds[i])
        assert outputs_r[i] == expecteds[i], message(inputs[i], outputs[i], expecteds[i])
    print("Tests de longueur passés avec succès.")
    return True

# from tp import longueur
# test_longueur()
def test_element():
    """ Tests pour la fonction element """
    inputs = tab2maillon([i for i in range(10)])
    outputs_i = [element_i(inputs, i) for i in range(10)]
    outputs_r = [element_r(inputs, i) for i in range(10)]
    expecteds = [i for i in range(10)]
    for i in range(10):
        assert outputs_i[i] == expecteds[i], message(inputs, outputs[i], expecteds[i])
        assert outputs_r[i] == expecteds[i], message(inputs, outputs[i], expecteds[i])
    print("Tests de element passés avec succès.")
    return True
# from tp import element
# test_element()
def test_remplace():
    """ Tests pour la fonction remplace """
    print("Tests de remplace passés avec succès.")
    return True

# from tp import remplace
# test_remplace()
def test_ajoute_position():
    """ Tests pour la fonction ajoute_position """
    print("Tests de ajoute_position passés avec succès.")
    return True

# from tp import ajoute_position
# test_ajoute_position()
def test_supprime():
    """ Tests pour la fonction supprime """
    print("Tests de supprime passés avec succès.")
    return True

# from tp import supprime
# test_supprime()
