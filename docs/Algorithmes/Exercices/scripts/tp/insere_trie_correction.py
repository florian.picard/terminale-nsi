def insere_trie(l, e):
    """ Liste, int -> Liste
    l est triée par ordre croissant
    Insère l'élément e dans la liste l. La liste résultante est triée"""
    if est_vide(l):
        return ajoute(l, e)
    else:
        if e <= tete(l):
            return ajoute(l, e)
        else:
            return ajoute(insere_trie(queue(l), e), tete(l))
