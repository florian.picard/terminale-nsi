---
title: Travail demandé
---

Vous travaillerez en groupe de trois personnes maximum. Vous sélectionnerez un projet présent sur <http://pisurquatre.xyz/nsi/projets> en rapport avec le thème demandé. Vous pouvez également choisir de traiter un autre sujet en rapport avec le thème demandé. Dans ce cas, une fois le projet trouvé, il vous faudra me le présenter afin que je le valide avant que vous commenciez à travailler dessus.  


## Rendu du code

Deux fichiers au moins sont attendus.

-   un fichier contenant le code principal du projet. Vous prendrez soin de documenter toutes les fonctions et méthodes, ainsi que de commenter votre code si cela est nécessaire. Les fonctions et méthodes devront respecter les docstring indiquées.
-   un fichier de test dans lequel vous testerez **toutes** les fonctionnalités du fichier de code. Pour chaque fonctionnalité, vous écrirez plusieurs tests afin de vous assurer que celle-ci est correctement implémentée.
-   tout autre fichier qui vous semble nécessaire.


## Rapport et présentation orale

À l'issue du projet, vous effectuerez une présentation orale avec votre groupe. Celle-ci durera 5 minutes au total. Vous veillerez à ce que le temps de parole soit équitablement réparti entre les membres du groupe.

Vous fournirez un fichier de présentation `présentation.pdf` que vous utiliserez lors de cette présentation orale. Celle-ci doit être brève et compréhensible par des personnes qui n'ont pas connaissance du projet.

-   Vous introduirez l'objectif du projet succintement.
-   Vous présenterez ensuite l'architecture générale du projet (présentation des classes par exemple), et vous détaillerez les éléments essentiels du projet. Vous n'oublierez pas de présenter des exemples d'application.
-   Enfin, vous pourrez conclure en expliquant les difficultés rencontrées, ce que le projet vous a appris, ainsi que les pistes d'amélioration futures pour le projet (quelles fonctionnalités implémenter ?).


## Note finale

La note finale se décomposera de la manière suivante :

-   15 points pour le code et les tests ;
-   5 points pour la présentation orale et le fichier de présentation.

