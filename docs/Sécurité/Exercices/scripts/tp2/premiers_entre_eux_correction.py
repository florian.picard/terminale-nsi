def premiers_entre_eux(a, b):
    """ int, int -> bool
    Renvoie True si et seulement si a et b sont premiers entre eux """
    A, B = diviseurs_premiers(a), diviseurs_premiers(b)
    commun = A.intersection(B)
    return len(commun) == 0
