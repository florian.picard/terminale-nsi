def taille(a):
    """ Arbre -> int
    Renvoie le nombre de nœuds de a """
    if est_vide(a):
        return 0
    return 1 + taille(gauche(a)) + taille(droit(a))
