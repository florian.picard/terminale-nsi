BLANC, GRIS, NOIR = 0, 1, 2

def parcours_cycle(G, depart, couleurs):
    """ Graphe, Sommet, {Sommet:Couleur} -> bool
    Renvoie True ssi il existe un cycle dans le graphe G dans la composante connexe de depart. 
    On suppose que depart est en BLANC. """
    couleurs[depart] = GRIS
    for destination in G.voisins(depart):
        if couleurs[destination] == BLANC:
            return parcours_cycle(G, destination, couleurs)
        elif couleurs[destination] == GRIS:
            return True
    couleurs[depart] = NOIR
    return False
