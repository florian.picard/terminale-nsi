def nb_joueurs(a):
    """ Arbre -> int
    Renvoie le nombre de joueurs ayant participé à la compétition d'arbre a """
    if est_vide(a):
        return 0
    elif est_feuille(a):
        return 1
    else:
        return nb_joueurs(gauche(a)) + nb_joueurs(droit(a))
