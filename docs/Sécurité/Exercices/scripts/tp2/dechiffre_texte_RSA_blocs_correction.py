def dechiffre_texte_RSA_blocs(chiffre, sk):
    """ [int], (int, int) -> str
    Déchiffre le message chiffre à l'aide d'un chiffrement RSA par blocs """
    dechiffre = [dechiffre_RSA(n, sk) for n in chiffre]
    chaine = "".join([str(n).rjust(2, "0") for n in dechiffre])
    clair = ""
    for i in range(0, len(chaine), 3):
        n = int(chaine[i:i + 3])
        clair += chr(n)
    return clair
