def maximum(a):
    """ Arbre -> int
    Renvoie la plus grande étiquette de a """
    if est_feuille(a):
        return etiquette(a)
    elif est_vide(gauche(a)):
        return max(etiquette(a), maximum(droit(a)))
    elif est_vide(droit(a)):
        return max(etiquette(a), maximum(gauche(a)))
    else:
        return max(etiquette(a), maximum(gauche(a)), maximum(droit(a)))
