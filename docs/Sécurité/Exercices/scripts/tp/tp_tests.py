def test_code_ascii():
    """ Tests pour la fonction code_ascii """
    print("Tests de code_ascii passés avec succès.")
    return True

# from tp import code_ascii
# test_code_ascii()
def test_decode_ascii():
    """ Tests pour la fonction decode_ascii """
    print("Tests de decode_ascii passés avec succès.")
    return True

# from tp import decode_ascii
# test_decode_ascii()
def test_genere_alphabet_majuscule():
    """ Tests pour la fonction genere_alphabet_majuscule """
    print("Tests de genere_alphabet_majuscule passés avec succès.")
    return True

# from tp import genere_alphabet_majuscule
# test_genere_alphabet_majuscule()
def test_decale_car():
    """ Tests pour la fonction decale_car """
    print("Tests de decale_car passés avec succès.")
    return True

# from tp import decale_car
# test_decale_car()
def test_chiffre_cesar():
    """ Tests pour la fonction chiffre_cesar """
    print("Tests de chiffre_cesar passés avec succès.")
    return True

# from tp import chiffre_cesar
# test_chiffre_cesar()
def test_dechiffre_cesar():
    """ Tests pour la fonction dechiffre_cesar """
    print("Tests de dechiffre_cesar passés avec succès.")
    return True

# from tp import dechiffre_cesar
# test_dechiffre_cesar()
def test_chiffre_vigenere():
    """ Tests pour la fonction chiffre_vigenere """
    print("Tests de chiffre_vigenere passés avec succès.")
    return True

# from tp import chiffre_vigenere
# test_chiffre_vigenere()
