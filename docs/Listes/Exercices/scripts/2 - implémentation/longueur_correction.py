def longueur_r(m):
    """ Maillon -> int
    Compte le nombre de maillons présents dans la chaîne dont le premier maillon est m """
    # version récursive
    if est_vide(m):
        return 0
    else:
        return 1 + longueur_r(queue(m))

def longueur_i(m):
    """ Maillon -> int
    Détermine le nombre de maillons présents dans la chaine après le maillon m (inclu). """
    # version itérative
    courant = m
    count = 0
    while not est_vide(courant):
        count = count + 1
        courant = queue(courant)
    return count

longueur = longueur_i
