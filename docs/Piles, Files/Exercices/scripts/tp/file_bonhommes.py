class Bonhomme:
    def __init__(self, t):
        self.taille = t

    def __repr__(self):
        return f"<Bonhomme {self.taille}>"

    def affiche(self):
        print(self)

    def content(self, c):
        return c.taille == self.taille

class File:
    def __init__(self):
        self.contenu = []

    def __repr__(self):
        return "(Début file) [" + ", ".join(b.__repr__() for b in self.contenu) + "]"

    def affiche(self):
        print(self)

    def est_vide(self):
        return self.contenu == []

    def enfiler(self, b):
        assert isinstance(b, Bonhomme)
        self.contenu.append(b)

    def defiler(self):
        if self.est_vide():
            raise IndexError("Impossible de défiler : la file est vide.")
        return self.contenu.pop(0)
