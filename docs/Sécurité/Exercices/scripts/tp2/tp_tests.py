def test_nombres_premiers():
    """ Tests pour la fonction nombres_premiers """
    print("Tests de nombres_premiers passés avec succès.")
    return True

# from tp import nombres_premiers
# test_nombres_premiers()
def test_diviseurs():
    """ Tests pour la fonction diviseurs """
    print("Tests de diviseurs passés avec succès.")
    return True

# from tp import diviseurs
# test_diviseurs()
def test_premiers_entre_eux():
    """ Tests pour la fonction premiers_entre_eux """
    print("Tests de premiers_entre_eux passés avec succès.")
    return True

# from tp import premiers_entre_eux
# test_premiers_entre_eux()
def test_inverse_modulo():
    """ Tests pour la fonction inverse_modulo """
    print("Tests de inverse_modulo passés avec succès.")
    return True

# from tp import inverse_modulo
# test_inverse_modulo()
def test_decompose():
    """ Tests pour la fonction decompose """
    print("Tests de decompose passés avec succès.")
    return True

# from tp import decompose
# test_decompose()
def test_genere_RSA():
    """ Tests pour la fonction genere_RSA """
    print("Tests de genere_RSA passés avec succès.")
    return True

# from tp import genere_RSA
# test_genere_RSA()
def test_chiffre_RSA():
    """ Tests pour la fonction chiffre_RSA """
    print("Tests de chiffre_RSA passés avec succès.")
    return True

# from tp import chiffre_RSA
# test_chiffre_RSA()
def test_dechiffre_RSA():
    """ Tests pour la fonction dechiffre_RSA """
    print("Tests de dechiffre_RSA passés avec succès.")
    return True

# from tp import dechiffre_RSA
# test_dechiffre_RSA()
def test_chiffre_texte_RSA():
    """ Tests pour la fonction chiffre_texte_RSA """
    print("Tests de chiffre_texte_RSA passés avec succès.")
    return True

# from tp import chiffre_texte_RSA
# test_chiffre_texte_RSA()
def test_dechiffre_texte_RSA():
    """ Tests pour la fonction dechiffre_texte_RSA """
    print("Tests de dechiffre_texte_RSA passés avec succès.")
    return True

# from tp import dechiffre_texte_RSA
# test_dechiffre_texte_RSA()
def test_chiffre_texte_RSA_blocs():
    """ Tests pour la fonction chiffre_texte_RSA_blocs """
    print("Tests de chiffre_texte_RSA_blocs passés avec succès.")
    return True

# from tp import chiffre_texte_RSA_blocs
# test_chiffre_texte_RSA_blocs()
def test_dechiffre_texte_RSA_blocs():
    """ Tests pour la fonction dechiffre_texte_RSA_blocs """
    print("Tests de dechiffre_texte_RSA_blocs passés avec succès.")
    return True

# from tp import dechiffre_texte_RSA_blocs
# test_dechiffre_texte_RSA_blocs()
