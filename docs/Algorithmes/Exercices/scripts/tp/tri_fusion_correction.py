def tri_fusion(l):
    """ Liste -> Liste
    Trie la liste l (tri fusion) """
    if est_vide(l):
        return l
    elif est_singleton(l):
        return l
    else:
        l1, l2 = diviser(l)
        l1_triée, l2_triée = tri_fusion(l1), tri_fusion(l2)
        return fusionner(l1_triée, l2_triée)
