---
title: Implémentation
---


## Pile

Dans cette partie, on implémente l'interface du type `Pile` à l'aide d'objets de type `Maillon`. Une pile `p` possède un unique attribut, `sommet` : 

-   celui-ci vaut `None` dans le cas où la pile est vide ;
-   celui-ci est le premier `Maillon` de la chaîne dans le cas où la pile n'est pas vide.

Les opérations d'empilement/dépilement reviennent donc à ajouter/supprimer le premier maillon de la chaîne.

![img](repr_pile.png)

{{ IDE('scripts/tp/Pile') }}


### Méthode `est_vide`

Écrire la méthode `est_vide` de la classe `Pile` qui renvoie `True` si et seulement si la pile `self` est vide. On rappelle qu'on a pris comme convention que l'attribut `sommet` d'un objet de type `Pile` vaut `None` dans le cas de la liste vide.

``` py linenums="1" 
def est_vide(self):
    """ Pile -> bool
    Détermine si la pile est vide """
    pass
```


### Méthode `empiler`

Écrire la méthode `empiler` de la classe `Pile` qui ajoute la valeur `v` au sommet de la pile `self`. Il suffit pour cela de changer le maillon de tête : si la pile correspond à la chaîne suivante :

![img](empiler.png)

alors après l'exécution de l'instruction `p.empiler(42)` elle doit correspondre à la chaîne :

![img](empiler2.png)

``` py linenums="1" 
def empiler(self, v):
    """ Pile, int -> Nonetype
    Empile la valeur v au sommet de la pile self """
    pass
```


### Méthode `depiler`

Écrire la méthode `depiler` de la classe `Pile` qui renvoie la valeur de l'élément présent au sommet de la pile, en le supprimant de celle-ci.

Par exemple, si initialement la pile `p` correspond à la chaîne :

![img](empiler2.png)

alors l'intruction `p.depiler()` renvoie `42`. De plus, à l'issue de l'exécution de cette instruction, la chaîne correspondant à `p` est :

![img](empiler.png)

**Attention.** Dans le cas où la pile `self` est vide, on *soulèvera une exception* à l'aide de l'instruction `raise IndexError("Impossible de dépiler : la pile est vide")`. Une exception est une instruction qui, si elle n'est pas traitée, arrête l'exécution du programme et affiche sur la sortie standard un message.

``` py linenums="1" 
def depiler(self):
    """ Pile -> int
    Renvoie l'élément présent au sommet de la pile self, en le supprimant de la pile """
    pass
```


### Affichage d'une pile

On souhaite écrire la méthode spéciale `__str__` afin que, lorsque l’on exécute l’instruction `print(p)` une représentation compréhensible de la pile soit affichée sur la sortie standard.

Pour cela on initialisera une chaîne de caractère `s` commençant par `"[Sommet]"` et on parcourra tous les maillons de la chaîne, en ajoutant successivement la chaîne de caractère représentant la valeur du maillon courant à `s` (on utilisera
des espaces pour séparer les valeurs entres elles).

**Attention.** Méthode `__str__` ne modifiera pas l’objet de type `Pile` auquel elle est appliquée.

``` py linenums="1" 
def __str__(self):
    """ Pile -> str
    Construit la chaîne de caractère représentant la pile self """
    pass
```


## File

Dans cette partie, on implémente l'interface du type `Pile` à l'aide d'objets de type `Maillon`. Une file `f` possède deux attributs : 

-   l'attribut `debut` qui fait référence au début de la file ;
-   l'attribut `fin` qui fait référence à la fin de la file.

![img](repr_pile.png)

Enfiler un élément revient donc à ajouter un maillon à la fin de la chaîne, défiler revient à supprimer le maillon au début de la chaîne, en en renvoyant sa valeur. La file vide est caractérisée par le fait qu'elle ne contient aucun maillon et donc que ses attributs `debut` et `fin` valent tous deux `None`. 

{{ IDE('scripts/tp/File') }}


### Méthode `est_vide`

Écrire la méthode `est_vide` de la classe `File` qui renvoie `True` si et seulement si la pile `self` est vide. On rappelle qu'une file `f` est vide si et seulement si ses attributs `debut` et `fin` sont valent `None`.

``` py linenums="1" 
def est_vide(self):
    """ File -> bool
    Détermine si la file self est vide """
    pass
```


### Méthode `enfiler`

Écrire la méthode `enfiler` de la classe `File` qui ajoute la valeur `v` à la fin de la file `self`. Deux cas sont possibles :

-   soit la file `self` est vide, dans ce cas on met à jour `self.debut` et `self.fin` afin qu'ils fassent référence au même maillon de valeur `v` ;
-   sinon, on change l'attribut `suivant` du dernier maillon de la chaîne pour qu'il fasse référence au maillon de valeur `v` et on met à jour l'attribut `self.fin` pour qu'il fasse référence au maillon de valeur `v`.

Si la file `f` correspond à la chaîne suivante : 

![img](enfiler.png)

Alors après exécution de `f.enfiler(42)` elle doit correspondre à la chaîne :

![img](enfiler2.png)

``` py linenums="1" 
def enfiler(self, v):
    """ File, int -> Nonetype
    Ajoute l'élément v à la file self """
    pass
```


### Méthode `defiler`

Écrire la méthode `defiler` de la classe `File` qui renvoie la valeur du premier élément de la file, en le supprimant de celle-ci.

Si la file `f` correspond à la chaîne : 

![img](enfiler2.png)

Alors l'instruction `f.defiler()` doit renvoyer `12`. De plus, à l'issue de l'exécution de cette instruction, la chaîne correspondant à `f` est :

![img](enfiler.png)

**Attention.** Si la file est initialement vide, on soulèvera l'exception correspondante. Si la file est vide *après défilement* on s'assurera de modifier l'attribut `fin` de la file à `None` (car une file vide n'est constituée d'aucun maillon).

``` py linenums="1" 
def defiler(self):
    """ File -> int
    Renvoie le premier élément de la file en le supprimant de celle-ci """
    pass
```


### Affichage d'une file

On souhaite écrire la méthode spéciale `__str__` afin que, lorsque l’on exécute l’instruction `print(f)` une représentation compréhensible de la file soit affichée sur la sortie standard.

Pour cela on initialisera une chaîne de caractère `s` commençant par `"[Début]"` et on parcourra tous les maillons de la chaîne, en ajoutant successivement la chaîne de caractère représentant la valeur du maillon courant à `s` (on utilisera des espaces pour séparer les valeurs entres elles). On ajoutera `"[Fin]"` à `s` avant de la renvoyer.

**Attention.** Méthode `__str__` ne modifiera pas l’objet de type `File` auquel elle est appliquée.

``` py linenums="1" 
def __str__(self):
    """ self -> str
    Construit la chaîne de caractères représentant la file self """
    pass
```


## Piles et files bornées

On souhaite implémenter la structure de donnée "pile bornée" : il s'agit d'une structure de donnée qui possède la même interface que celle d'une pile, mais est de plus dotée d'une contenance maximale. Ainsi, il doit être impossible d'empiler un élément si la pile est remplie.

L'interface d'une pile bornée est la suivante : 

<table border="2" cellspacing="0" cellpadding="6" rules="groups" frame="hsides">


<colgroup>
<col  class="org-left" />

<col  class="org-left" />

<col  class="org-left" />
</colgroup>
<thead>
<tr>
<th scope="col" class="org-left">Fonction</th>
<th scope="col" class="org-left">Signature</th>
<th scope="col" class="org-left">Description</th>
</tr>
</thead>

<tbody>
<tr>
<td class="org-left"><code>Pile(n)</code></td>
<td class="org-left"><code>int -&gt; Pile</code></td>
<td class="org-left">Renvoie une pile bornée vide de capacité maximale <code>n</code></td>
</tr>


<tr>
<td class="org-left"><code>p.est_vide()</code></td>
<td class="org-left"><code>() -&gt; Bool</code></td>
<td class="org-left">Détermine si la pile est vide.</td>
</tr>


<tr>
<td class="org-left"><code>p.est_pleine()</code></td>
<td class="org-left"><code>() -&gt; Bool</code></td>
<td class="org-left">Détermine si la pile a atteint sa capacité maximale.</td>
</tr>


<tr>
<td class="org-left"><code>p.empiler(e)</code></td>
<td class="org-left"><code>int -&gt; Nonetype</code></td>
<td class="org-left">Empile l'élément <code>e</code> au sommet de la pile <code>p</code>.</td>
</tr>


<tr>
<td class="org-left">&#xa0;</td>
<td class="org-left">&#xa0;</td>
<td class="org-left">Si cela n'est pas possible, soulève une erreur.</td>
</tr>


<tr>
<td class="org-left"><code>p.depiler()</code></td>
<td class="org-left"><code>() -&gt; int</code></td>
<td class="org-left">Renvoie la valeur de l'élément au sommet de la pile, en le supprimant.</td>
</tr>


<tr>
<td class="org-left">&#xa0;</td>
<td class="org-left">&#xa0;</td>
<td class="org-left">Si cela n'est pas possible, soulève une erreur.</td>
</tr>
</tbody>
</table>

<div class="question" markdown="1">
Vous écrirez et **testerez** le code d’une classe `PileBornee` implémentant ces fonctionnalités.
On pourra penser à utiliser trois attributs : le premier étant une pile (classique), le second
la capacité maximale de la pile bornée, et le troisième le nombre d’éléments contenus dans la
pile. On écrira les méthodes de la classe `PileBornee` en faisant appel à celle de la classe Pile
et en mettant faisant les vérifications nécessaires et en mettant correctement à jour le nombre
d’éléments contenus dans la pile.
</div>

<div class="question" markdown="1">
Même question pour la structure de données "file bornée".
</div>




## Documents

-   [Implémentation (sujet)](tp.pdf)
-   Fichiers python :
    -   [templates](scripts/tp/tp.py)

