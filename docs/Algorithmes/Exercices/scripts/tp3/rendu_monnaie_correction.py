def rendu_monnaie(systeme, montant):
    """ [int], int -> [int]
    Trouve le meilleur rendu de monnaie dans systeme """
    T = [([1]*i) for i in range(montant + 1)] 
    for i in range(montant + 1):
        for p in systeme:
            if i + p <= montant and len(T[i]) + 1 < len(T[i + p]):
                    T[i + p] = T[i] + [p]
    return T[montant]
