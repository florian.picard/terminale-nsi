% Created 2023-08-02 mer. 16:24
% Intended LaTeX compiler: pdflatex
\documentclass{cours}
\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\usepackage{graphicx}
\usepackage{longtable}
\usepackage{wrapfig}
\usepackage{rotating}
\usepackage[normalem]{ulem}
\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{capt-of}
\usepackage{hyperref}
\renewcommand{\destinataire}{Terminales NSI}
\renewcommand{\numberdoc}{6}
\usepackage{minted}
\author{Florian Picard}
\date{\today}
\title{Techniques de programmation}
\hypersetup{
 pdfauthor={Florian Picard},
 pdftitle={Techniques de programmation},
 pdfkeywords={},
 pdfsubject={},
 pdfcreator={Emacs 28.2 (Org mode 9.5.5)}, 
 pdflang={English}}
\begin{document}

\maketitle
\vspace{-1cm}


\section{Styles de programmation}
\label{sec:orgf54f85b}

\subsection{Vocabulaire}
\label{sec:org22819d3}

\begin{description}
\item[{Effet de bord}] Une fonction est à effet de bord lorsqu'elle modifie l'état de la mémoire de l'ordinateur.
\item[{Objet non mutable}] Lorsque l'on ne peut pas modifier l'état d'un objet après sa création, on dit que celui-ci est non mutable. Par exemple \texttt{True} en Python est un objet non mutable. La plupart des objets sont mutables en Python.
\end{description}

\begin{itemize}
\item En python, le type \texttt{tuple} est \textbf{non-mutable}, tandis que le type \texttt{list} est mutable. 

\begin{minted}[frame=single,linenos=true,breaklines,framesep=2mm,label=Code python]{python}
  ma_liste = [1, 2, 3]
  mon_tuple = (1, 2, 3) # Ne soulève pas d'erreurs
  mon_tuple[0] += 1     # Soulève une erreur lors de son exécution
\end{minted}

\begin{verbatim}
---------------------------------------------------------------------------
TypeError                                 Traceback (most recent call last)
Input In [276], in <cell line: 3>()
      1 ma_liste = [1, 2, 3]
      2 mon_tuple = (1, 2, 3) # Ne soulève pas d'erreurs
----> 3 mon_tuple[0] += 1

TypeError: 'tuple' object does not support item assignment
\end{verbatim}
\end{itemize}


\begin{multicols}{2}
\begin{itemize}
\item Une fonction ayant un effet de bord peut avoir un comportement différent alors qu'elle est appelée avec les mêmes arguments. 
\begin{minted}[frame=single,linenos=true,breaklines,framesep=2mm,label=Code python]{python}
  ma_liste = [1, 2, 3]
  def f(x):
      """ int -> [int] """
      for i in range(len(ma_liste)):
          ma_liste[i] += x
      return ma_liste
  print(f(1))
  print(f(1))
\end{minted}

\begin{verbatim}
[2, 3, 4]
[3, 4, 5]
\end{verbatim}
\end{itemize}
\columnbreak
\begin{itemize}
\item Au contraire, une fonction n'ayant pas d'effet de bord a nécessairement le même comportement lors d'appels successifs. 
\begin{minted}[frame=single,linenos=true,breaklines,framesep=2mm,label=Code python]{python}
  ma_liste = (1, 2, 3)
  def g(x):
      """ int -> [int] """
      return [e + x for e in ma_liste]
  print(g(1))
  print(g(1))
\end{minted}

\begin{verbatim}
[2, 3, 4]
[2, 3, 4]
\end{verbatim}
\end{itemize}
\end{multicols}

\subsection{Programmation fonctionnelle}
\label{sec:orgaaea713}

Le style de programmation fonctionnel est proche de l'écriture de preuves mathématiques : on décrit un programme à l'aide d'appel de fonctions et on les compose entre elles. On utilise des appels récursifs pour obtenir un comportement similaire à une boucle. Il est caractérisé par :
\begin{itemize}
\item l'absence d'effets de bord
\item les objets sont non mutables
\item les fonctions réalisent des calculs qui renvoient toujours le même résultat
\end{itemize}
On parle de \textbf{paradigme fonctionnel}. 

La fonction suivante ajoute \texttt{x} à tous les éléments de la liste \texttt{l}.

La liste \texttt{l} \textbf{n'est pas} modifiée.
\begin{minted}[frame=single,linenos=true,breaklines,framesep=2mm,label=Code python]{python}
def f_fonc(l, x):
    """ [int], int -> [int] """
    if l == []:
        return []
    else:
        return [l[0] + x] + f_fonc(l[1:], x)
l = [1, 2, 3]
print(f_fonc(l, 1))
print(l)
\end{minted}

\begin{verbatim}
[2, 3, 4]
[1, 2, 3]
\end{verbatim}

\subsection{Programmation impérative}
\label{sec:orgbe5e76b}

Le style de programmation impérative est très proche de la manière de fonctionner d’un
ordinateur\textasciitilde{}: dans ce style de programmation, on exécute des instructions qui modifient l’état de
l’ordinateur. On utilise des boucles pour répéter des instructions. Il est caractérisé par :
\begin{itemize}
\item la présence d’effets de bord
\item les objets sont mutables
\item l’ordinateur exécute des instructions. Le résultat dépend de l'état actuel de l'ordinateur.
\end{itemize}
On parle de \textbf{paradigme impératif}.

La fonction suivante ajoute \texttt{x} à tous les éléments de la liste \texttt{l}.

La liste \texttt{l} \textbf{est} modifiée.
\begin{minted}[frame=single,linenos=true,breaklines,framesep=2mm,label=Code python]{python}
def f_imp(l, x):
    """ [int], int -> [int] """
    for i, e in enumerate(l):
        l[i] += x
    return l
l = [1, 2, 3]
print(f_imp(l, 1))
print(l)
\end{minted}

\begin{verbatim}
[2, 3, 4]
[2, 3, 4]
\end{verbatim}

\section{Complexité d'un algorithme}
\label{sec:org24a8394}
Un \textbf{algorithme} est un procédé automatique permettant de résoudre un problème en un nombre fini d'étapes. La \textbf{complexité en temps} d'un algorithme est le nombre d'opérations effectuées en fonction de \(n\), la taille des données du problème. 



\item \relax
\vspace{-1.5em}
\begin{minipage}{0.35\textwidth}
On s'intéresse au nombre d'opérations effectuées plutôt qu'au temps, car tous les ordinateurs n'effectuent pas le même nombre d'opérations par seconde.

\end{minipage}
\hspace{1em}
\begin{minipage}{0.7\textwidth}

\begin{center}
\begin{tabular}{llllll}
\hline
Taille & \(\log_{2}(n)\) & \(n\) & \(n\log_{2}(n)\) & \(n^2\) & \(2^n\)\\
\hline
10 & 0.003 ms & 0.01ms & 0.03 ms & 0.1 ms & 1 ms\\
100 & 0.006 ms & 0.1 ms & 0.6 ms & 10 ms & 10\textsuperscript{14} siècles\\
1000 & 0.01 ms & 1ms & 10 ms & 1s & \\
10\textsuperscript{4} & 0.013 ms & 10 ms & 0.1 s & 100 s & \\
10\textsuperscript{5} & 0.016 ms & 100 ms & 1.6 s & 3 h & \\
10\textsuperscript{6} & 0.02 ms & 1s & 20 s & 10 jours & \\
\hline
\end{tabular}
\end{center}

\end{minipage}
\vspace{0.5em}

On considère que la plupart des opérations élémentaires s'effectuent en temps constant, à l'exception notable de l'exponentiation qui s'effectuent en un temps proportionnel à la taille de l'exposant. 

\begin{center}
\includegraphics[width=.9\linewidth]{./opérations.png}
\end{center}

On rappelle l'implémentation python de l'algorithme du tri par sélection. 

\begin{multicols}{2}
\begin{minted}[frame=single,linenos=true,breaklines,framesep=2mm,label=Code python]{python}
def insere_trie(l, x):
    """ Liste, int -> Liste """
    if est_vide(l):
        return ajoute(l, x)
    else:
        if x < tete(l):
            return ajoute(l, x)
        else:
            rst = queue(l)
            pre  = tete(l)
            rst_x = insere_trie(rst, x)
            lst = ajoute(rst_x, pre)
            return lst
\end{minted}

\columnbreak

\begin{minted}[frame=single,linenos=true,breaklines,framesep=2mm,label=Code python]{python}
def tri_sel(l):
    """ Liste -> Liste """
    if est_vide(l):
        return l
    else:
        pre = tete(l)
        rst = queue(l)
        rst_t = tri_sel(rst)
        lst_t = insere_trie(rst_t, pre)
        return lst_t
\end{minted}

La complexité de la fonction \texttt{tri\_sel} est, dans le pire des cas, quadratique en le nombre \(n\) d'éléments qui composent \texttt{l}. 

\end{multicols}

\textbf{Démonstration.}
\reponse{20}

Lol

\begin{tikzpicture}[node distance = 3cm]
    \begin{scope}[every node/.style={circle,thick,draw}]
        \node (P){$P$} ;
        \node[ right of = P] (B) {B} ; 
        \node[ above of = B] (A) {A} ; 
        \node[ below of = B] (C) {C} ; 
        \node[ right of = A] (D) {D} ; 
        \node[ right of = C] (E) {E} ; 
        \node[ right of = D] (F) {F} ; 
        \node[ below of = F] (G) {G} ; 
    \end{scope}

    \begin{scope}[>={Stealth[black]},
        every node/.style={fill=white,circle},
        every edge/.style={draw=black,very thick}]
        \path (P) edge node [] {$9 $} (A); 
        \path (P) edge node [] {$8 $} (B); 
        \path (P) edge node [] {$4 $} (C); 
        \path (A) edge node [] {$5 $} (D); 
        \path (A) edge node [] {$6 $} (E); 
        \path (B) edge node [] {$6$} (D); 
        \path (B) edge node [] {$5 $} (E); 
        \path (C) edge node [] {$9 $} (E); 
        \path (D) edge node [] {$4 $} (E); 
        \path (D) edge node [] {$5 $} (F); 
        \path (F) edge node [] {$7 $} (G); 
        \path (D) edge node [] {$8 $} (G); 
        \path (E) edge node [] {$10 $} (G); 
    \end{scope}
\end{tikzpicture}

\begin{center}
\includegraphics[width=.9\linewidth]{/home/marzikill/Projets/Terminales NSI/docs/Techniques de programmation/Graphe.png}
\end{center}



\section{Programmation dynamique}
\label{sec:org5b26c14}

\begin{itemize}
\item à partir de l'arbre d'appel de fibonnacci
\item nombre de chemins
\item rendu de monnaie (tp)
\item problème du sac à dos
\end{itemize}

\section{Notions de calculabilité}
\label{sec:org8abb7a4}
\end{document}