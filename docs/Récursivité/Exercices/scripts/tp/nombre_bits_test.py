import math
def test_nombre_bits():
    """ Tests pour la fonction nombre_bits """
    for i in range(1, 1024):
        expected = int(math.log2(i)) + 1
        assert nombre_bits(i) == expected, message(i, est_pair(bits), expected)
    print("Tests de nombre_bits passés avec succès")
    return True

# from tp import nombre_bits
# test_nombre_bits()
