import random

def nombres_premiers(n):
    """ int -> [int]
    Renvoie la liste des nombres premiers inférieurs ou égaux à n """
    if n <= 1:
        return []
    table = [True for _ in range(n + 1)]
    table[0], table[1] = False, False
    for i in range(2, n + 1):
        if not table[i]:
            continue
        for j in range(2*i, n + 1, i):
            table[j] = False
    return [i for i in range(n + 1) if table[i] == True]

def diviseurs_premiers(n):
    """ int -> {int}
    Renvoie l'ensemble des diviseurs de n """
    return {p for p in nombres_premiers(n) if n%p == 0}

def premiers_entre_eux(a, b):
    """ int, int -> bool
    Renvoie True si et seulement si a et b sont premiers entre eux """
    A, B = diviseurs_premiers(a), diviseurs_premiers(b)
    commun = A.intersection(B)
    return len(commun) == 0

def inverse_modulo(n, a):
    """ int, int -> int
    Renvoie (si possible) l'inverse de a modulo n """
    for b in range(n):
        if a*b % n == 1:
            return b
    return None

def decompose(n):
    """ int -> {int:int}
    Décompose n en produit de facteurs premiers """
    divs = diviseurs_premiers(n)
    decomp = dict()
    for p in divs:
        e = 1
        while n%p**e == 0:
            e += 1
        decomp[p] = e - 1   
    return decomp

import random

def genere_RSA(p, q):
    """ int, int -> (int, int), (int, int)
    Génère un couple de clé privée/clé publique pour RSA """
    n = p*q
    phi = (p - 1)*(q - 1)
    e = random.choice([i for i in range(phi) if premiers_entre_eux(i, phi)])
    d = inverse_modulo(phi, e)
    return (n, e), (n, d)

def chiffre_RSA(M, pk):
    """ int, (int, int) -> int
    Chiffre le message M à l'aide de la clé publique """
    n, e = pk
    return (M ** e) % n

def dechiffre_RSA(C, sk):
    """ int, (int, int)
    Déchiffre le message chiffré C à l'aide de la clé secrète """
    n, d = sk
    return (C ** d) % n

def chiffre_texte_RSA(clair, pk):
    """ str, (int, int) -> [int]
    Chiffre le texte à l'aide du chiffrement RSA et de la clé publique pk """
    nombres = [ord(c) for c in clair]
    chiffres = [chiffre_RSA(n, pk) for n in nombres]
    return chiffres

def dechiffre_texte_RSA(chiffre, sk):
    """ [int], (int, int) -> str
    Déchiffre le message chiffre """
    caracteres = [dechiffre_RSA(n, sk) for n in chiffre]
    return "".join([chr(n) for n in caracteres])

def chiffre_texte_RSA_blocs(clair, pk):
    """ str, (int, int) -> [int]
    Chiffre le texte clair à l'aide d'un chiffrement RSA par bloc """
    nombres = [ord(c) for c in clair]
    chaine = "".join([str(n).rjust(3, "0") for n in nombres])
    chiffres = []
    for i in range(0, len(chaine), 2):
        n = int(chaine[i:i + 2])
        chiffres.append(chiffre_RSA(n, pk))
    return chiffres

def dechiffre_texte_RSA_blocs(chiffre, sk):
    """ [int], (int, int) -> str
    Déchiffre le message chiffre à l'aide d'un chiffrement RSA par blocs """
    dechiffre = [dechiffre_RSA(n, sk) for n in chiffre]
    chaine = "".join([str(n).rjust(2, "0") for n in dechiffre])
    clair = ""
    for i in range(0, len(chaine), 3):
        n = int(chaine[i:i + 3])
        clair += chr(n)
    return clair

