def longueur(l):
    """ Liste -> int
    Renvoie la longueur de la liste l """
    if est_vide(l):
        return 0
    else:
        return 1 + longueur(queue(l))
