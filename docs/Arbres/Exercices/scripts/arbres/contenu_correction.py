def contenu(a, d):
    """ Arbre, dict -> Nonetype
    Ajoute à d le contenu de a """
    if est_vide(a):
        pass
    else:
        contenu(gauche(a), d)
        if not etiquette(a) in d:
            d[etiquette(a)] = 1
        else:
            d[etiquette(a)] += 1
        contenu(droit(a), d)
