def diviser(l):
    """ Liste -> Liste, Liste
    Divise la liste en deux listes """
    if est_vide(l):
        return creer_vide(), creer_vide()
    elif est_singleton(l):
        return l, creer_vide()
    else:
        l1, l2 = diviser(queue(queue(l)))
        return ajoute(l1, tete(l)), ajoute(l2, tete(queue(l)))
