def test_existe_chemin():
    """ Tests pour la fonction existe_chemin """
    print("Tests de existe_chemin passés avec succès.")
    return True

# from tp import existe_chemin
# test_existe_chemin()
def test_composantes_connexes():
    """ Tests pour la fonction composantes_connexes """
    print("Tests de composantes_connexes passés avec succès.")
    return True

# from tp import composantes_connexes
# test_composantes_connexes()
def test_parcours_cycle():
    """ Tests pour la fonction parcours_cycle """
    print("Tests de parcours_cycle passés avec succès.")
    return True

# from tp import parcours_cycle
# test_parcours_cycle()
def test_contient_cycle():
    """ Tests pour la fonction contient_cycle """
    print("Tests de contient_cycle passés avec succès.")
    return True

# from tp import contient_cycle
# test_contient_cycle()
def test_distance():
    """ Tests pour la fonction distance """
    print("Tests de distance passés avec succès.")
    return True

# from tp import distance
# test_distance()
def test_diametre():
    """ Tests pour la fonction diametre """
    print("Tests de diametre passés avec succès.")
    return True

# from tp import diametre
# test_diametre()
