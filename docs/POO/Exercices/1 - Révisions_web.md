---
title: Révisions
---

## Écriture binaire

Dans la mémoire de l'ordinateur, toutes les données (nombres, chaînes de caractères, mais aussi images, son&#x2026;) sont stockées en utilisant uniquement des $0$ et des $1$. Pour représenter un nombre dans la mémoire de l'ordinateur, on utilise donc son écriture dans la base 2.

On rappelle que si un nombre $n$ s'écrit $\overline{10110001}^2$ en base 2, alors cela veut dire que $n = 177$. 
On peut utiliser le tableau ci-dessous pour présenter le calcul.

<table border="2" cellspacing="0" cellpadding="6" rules="groups" frame="hsides">


<colgroup>
<col  class="org-left" />

<col  class="org-right" />

<col  class="org-right" />

<col  class="org-right" />

<col  class="org-right" />

<col  class="org-right" />

<col  class="org-right" />

<col  class="org-right" />

<col  class="org-right" />

<col  class="org-left" />
</colgroup>
<thead>
<tr>
<th scope="col" class="org-left">Écriture en base 2</th>
<th scope="col" class="org-right">1</th>
<th scope="col" class="org-right">0</th>
<th scope="col" class="org-right">1</th>
<th scope="col" class="org-right">1</th>
<th scope="col" class="org-right">0</th>
<th scope="col" class="org-right">0</th>
<th scope="col" class="org-right">0</th>
<th scope="col" class="org-right">1</th>
<th scope="col" class="org-left">&#xa0;</th>
</tr>
</thead>

<tbody>
<tr>
<td class="org-left">Puissances de 2</td>
<td class="org-right">2<sup>7</sup></td>
<td class="org-right">2<sup>6</sup></td>
<td class="org-right">2<sup>5</sup></td>
<td class="org-right">2<sup>4</sup></td>
<td class="org-right">2<sup>3</sup></td>
<td class="org-right">2<sup>2</sup></td>
<td class="org-right">2<sup>1</sup></td>
<td class="org-right">2<sup>0</sup></td>
<td class="org-left">Total</td>
</tr>
</tbody>

<tbody>
<tr>
<td class="org-left">Termes à ajouter</td>
<td class="org-right">128</td>
<td class="org-right">0</td>
<td class="org-right">32</td>
<td class="org-right">16</td>
<td class="org-right">0</td>
<td class="org-right">0</td>
<td class="org-right">0</td>
<td class="org-right">1</td>
<td class="org-left">177</td>
</tr>
</tbody>
</table>

On remarquera que dans cette convention d'écriture, le bit de poids fort (associé à $2^7$) est le bit le plus à gauche dans l'écriture du nombre et que le bit de poids faible (aussi appelé bit de parité) est le bit le plus à droite dans l'écriture du nombre. On utilisera cette convention dans tous les exercices. 

<div class="exo" id="orge3baac2">
<p>
Déterminer l'écriture en base 2 des entiers compris entre 0 (inclus) et 17 (exclu).
</p>

</div>


### Parité d'un nombre écrit en base 2

Écrire une fonction `est_pair` qui étant donné un tableau `bits` non vide dont les éléments appartiennent à $\{0 ; 1\}$ détermine si le nombre dont l'écriture en base 2 est donnée par le tableau `bits` est un nombre pair. La fonction `est_pair` renverra `True` si c'est le cas, et `False` sinon. 

{{ IDE('scripts/1 - Révisions/est_pair') }}


### Entier maximal à nombre de bits fixés

Écrire une fonction `est_plus_grand_kbits` qui prend en entrée un tableau `bits` non vide de taille `k` dont les éléments appartiennent à $\{0 ; 1\}$ et qui détermine si le tableau correspond au plus grand nombre entier que l'on puisse écrire en base 2 sur `k` bits. 

{{ IDE('scripts/1 - Révisions/est_plus_grand_kbits') }}


## Algorithmes classiques


### Recherche d'occurrences


#### Appartient

Écrire une fonction `appartient` qui étant donné un tableau (éventuellement vide) `tab` d'entiers et entier `e`, détermine si l'élément `e` est présent dans le tableau `tab`.

{{ IDE('scripts/1 - Révisions/appartient') }}


#### Nombre d'occurrences

Écrire une fonction `nombre_occurrences` qui étant donné un tableau (éventuellement vide) `tab` d'entiers et un entier `e`, détermine le nombre de fois où l'élément `e` est apparait dans le tableau `tab` (on appelle ce nombre le nombre d'*occurrences* de l'élément `e`).

{{ IDE('scripts/1 - Révisions/nombre_occurrences') }}


#### Indice des occurrences

Écrire une fonction `indices_occurrences` qui étant donné un tableau (éventuellement vide) `tab` d'entiers de type quelconque et un entier `e`, détermine la liste des indices (rangés par ordre croissant) des éléments de `tab` qui sont égaux à `e`.

**Rappel.** On pourra soit faire appel à la fonction `nombre_occurrences` pour initialiser le tableau d'indice des éléments égaux à `e` à la bonne taille, ou bien initialiser un tableau vide et ajouter les indices des éléments égaux à `e` au fur et à mesure à l'aide de l'instruction `t.append(elem)`.

{{ IDE('scripts/1 - Révisions/indices_occurrences') }}


### Recherche de maximum

Écrire une fonction `maximum` qui étant donné un tableau non vide `tab` d'entiers détermine la valeur du plus grand des éléments de ce tableau, ainsi que le premier indice pour lequel ce maximum est atteint.  

{{ IDE('scripts/1 - Révisions/maximum') }}


### Tri par sélection

Écrire une fonction `tri_selection` qui prend en paramètre un tableau non vide `tab` de nombres entiers et qui trie ce tableau en place (c'est-à-dire que le tableau est modifié) par ordre croissant des valeurs. On utilisera l'algorithme suivant :

-   On parcourt le tableau de gauche à droite :
    -   on recherche le minimum du tableau entre cette position courante et la fin du tableau ;
    -   on échange alors les 2 valeurs

{{ IDE('scripts/1 - Révisions/tri_selection') }}


## Documents

-   [Mémento python](Mémento_python/mementopython3.pdf)
-   [Révisions (sujet)](1 - Révisions_sujet.pdf)

-   Fichiers python :
    -   [templates](scripts/1 - Révisions/tp.py)
    -   [tests](scripts/1 - Révisions/tp_tests.py)

