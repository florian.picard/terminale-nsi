def concatene(l1, l2):
    """ Liste, Liste -> Liste
    Concatène les deux listes """
    if est_vide(l1):
        return l2
    else:
        queue_et_l2 = concatene(queue(l1), l2)
        return ajoute(queue_et_l2, tete(l1))
