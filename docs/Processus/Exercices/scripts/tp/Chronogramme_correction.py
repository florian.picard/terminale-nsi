class Chronogramme:
    """Représente un chronogramme"""
    def __init__(self):
        """Chronogramme -> Nonetype"""
        self.table = []
        # self.t = 0

    def temps_ecoule(self):
        """ Chronogramme -> int
        Renvoie le nombre de cycles écoulés depuis l'instant initial """
        return len(self.table)
    
    def enregistrer(self, p, q):
        """ int, int -> Nonetype
        Enregistre l'information : le processus p est exécuté pendant q cycles """
        for _ in range(q):
            self.table.append(p)
    
    def __repr__(self):
        """ self -> str
        Renvoie une chaine de caractère représentant le chronogramme self """
        return " | ".join(str(p) for p in self.table)
