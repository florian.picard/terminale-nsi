class Maillon:
    def __init__(self, v, n):
        self.valeur = v
        self.suivant = n

class Pile:
    """Une pile d'entiers"""
    def __init__(self):
        """ Pile -> Nonetype """
        self.sommet = None

    def est_vide(self):
        """ Pile -> bool
        Détermine si la pile est vide """
        return self.sommet is None
    
    def empiler(self, v):
        """ Pile, int -> Nonetype
        Empile la valeur v au sommet de la pile self """
        self.sommet = Maillon(v, self.sommet)
    
    def depiler(self):
        """ Pile -> int
        Renvoie l'élément présent au sommet de la pile self,
        en le supprimant de la pile """
        if self.est_vide():
            raise IndexError("Impossible de dépiler : la pile est vide")
        val = self.sommet.valeur
        self.sommet = self.sommet.suivant
        return val
    
    def __str__(self):
        """ Pile -> str
        Construit la chaîne de caractère représentant la pile self """
        s = "[Sommet]"
        mc = self.sommet
        while mc is not None:
            s += f" {str(mc.valeur)}"
            mc = mc.suivant
        return s
