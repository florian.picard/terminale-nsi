---
title: Exercices en ligne
---


## Le site de l'AEIF

On trouvera les exercices à l'adresse <https://e-nsi.forge.aeif.fr/exercices_bdd/>

Les exercices sont prévus pour des élèves de Terminale NSI, il est obligatoire de traiter toutes les questions.


## SQL ZOO

Une très bonne ressource en anglais : <https://sqlzoo.net>

Vous êtes guidés pas à pas des requêtes les plus simples aux requêtes les plus compliquées. Il est obligatoire de faire les questions des pages :

-   [SELECT basics](https://sqlzoo.net/wiki/SELECT_basics). "Some simple queries to get you started"
-   [SELECT name](https://sqlzoo.net/wiki/SELECT_names). "Some pattern matching queries".
-   [JOIN](https://sqlzoo.net/wiki/The_JOIN_operation). "In which we join two tables; game and goals."


## SQL Island

Une ressource en anglais, sous forme de jeu. Vous incarnez une personne naufragée sur une île à la suite d'un crash d'avion, et vous devez trouver un moyen de vous échapper en écrivant des requêtes SQL. Il s'agit d'un très bon tutoriel interactif, dans lequel vous êtes guidés à chaque étape dans la requête à écrire. 

<http://wwwlgis.informatik.uni-kl.de/extra/game/?lang=en>

Une note bonus (même coefficient qu'une interrogation) sera attribuée aux élèves écrivant un document présentant leur solution du jeu pas à pas.


## SQL Murder Mystery

Une ressource en anglais. Vous devez résoudre un meurtre en écrivant des requêtes SQL. 

<https://mystery.knightlab.com/>

Sans aucune indication, cela peut-être un exercice un peu difficile, aussi une aide pour débuter se trouve a l'adresse <https://mystery.knightlab.com/walkthrough.html>

Une note bonus (même coefficient qu'une interrogation) sera attribuée aux élèves écrivant un document présentant leur solution du jeu pas à pas.


## Et plein d'autres&#x2026;

Citons par exemple :

-   <https://selectstarsql.com/>
-   <https://lost-at-sql.therobinlord.com/>
-   <https://schemaverse.com/>

L'élève motivé trouvera sur ces sites des tutoriels allant bien au delà du programme de terminale.

