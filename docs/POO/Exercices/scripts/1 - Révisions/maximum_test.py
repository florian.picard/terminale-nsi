import random
def test_maximum():
    # Tests pour la fonction maximum
    assert maximum([1]) == (1, 0)
    assert maximum([1, 2, -1]) == (2, 1)
    assert maximum([1, 2, -1, 2]) == (2, 1)
    for _ in range(1000):
        tab = [random.randint(-10, 10) for _ in range(random.randint(1, 10))]
        expected = max(tab), tab.index(max(tab))
        assert maximum(tab) == expected, f"\ Le test suivant n'a pas été validé.\nEntrée : {tab }\nSortie : {maximum(tab)}\nAttendu : {expected}"
    return True

# from tp import maximum
test_maximum()
