def somme(l):
    """ Liste -> int
    Calcule la somme des éléments de la liste l """
    if est_singleton(l):
        return tete(l)
    else:
        return tete(l) + somme(queue(l))
