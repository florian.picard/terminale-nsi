def test_ajoute_debut():
    """ Tests pour la fonction ajoute_debut """
    assert ajoute_debut(None, 3).valeur == 3
    assert ajoute_debut(None, 3).suivant == None
    print("Tests de ajoute_debut passés avec succès.")
    return True

# from tp import ajoute_debut
test_ajoute_debut()
