def existe_chemin(G, sommet, destination):
    """ Graphe, Sommet, Sommet -> bool 
    sommet != destination
    Renvoie True ssi il existe un chemin entre source et destination dans G. """
    atteignables = parcours_profondeur(G, sommet)
    return destination in atteignables

def composantes_connexes(G):
    """ Graphe -> {Sommet:int}
    Détermine les composantes connexes du graphe G sous la forme d'un dictionnaire """
    composantes = dict()
    composante_num = 0
    for sommet in G.sommets():
        if sommet in composantes:
            continue
        composante_num += 1
        for atteignable in parcours_profondeur(G, sommet):
            composantes[atteignable] = composante_num
    return composantes

BLANC, GRIS, NOIR = 0, 1, 2

def parcours_cycle(G, depart, couleurs):
    """ Graphe, Sommet, {Sommet:Couleur} -> bool
    Renvoie True ssi il existe un cycle dans le graphe G dans la composante connexe de depart. 
    On suppose que depart est en BLANC. """
    couleurs[depart] = GRIS
    for destination in G.voisins(depart):
        if couleurs[destination] == BLANC:
            return parcours_cycle(G, destination, couleurs)
        elif couleurs[destination] == GRIS:
            return True
    couleurs[depart] = NOIR
    return False

def contient_cycle(G):
    """ Graphe -> bool
    Renvoie True ssi le graphe orienté G contient un cycle """
    couleurs = {s:BLANC for s in G.sommets()}
    for s in G.sommets():
        if couleurs[s] == NOIR:
            continue
        trouve_cycle_depuis_s = parcours_cycle(G, s, couleurs)
        if trouve_cycle_depuis_s:
            return True
    return False

def distance(G, depart, arrivee):
    """ Graphe, Sommet, Sommet -> int 
    Renvoie la longueur du plus petit chemin entre depart et arrivee dans G """
    vus = Ensemble()
    à_explorer = File()
    à_explorer.enfiler((depart, 0))
    while not à_explorer.est_vide():
        # définition du sommet courant 
        sommet, distance = à_explorer.defiler()
        if sommet in vus:
            continue
        vus.ajouter(sommet)
        # opération de traitement
        if sommet == arrivee:
            return distance
        # ajout des voisins non visités
        for destination in G.voisins(sommet):
            if not destination in vus:
                à_explorer.enfiler((destination, distance + 1))
    return -1

def diametre(G):
    """ Graphe -> int
    Renvoie la distance maximale entre deux sommets quelconques du graphe """
    d_max = -float("inf")
    sommets_max = None
    for source in G.sommets():
        for destination in G.sommets():
            d = distance(G, source, destination)
            if d > d_max:
                d_max = d
                sommets_max = (source, destination)
    return d_max, sommets_max

