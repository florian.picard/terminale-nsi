def niveau(a, i):
    """ Arbre, int -> [str]
    Renvoie la liste des étiquettes des nœuds de profondeur i de l'arbre a """
    if est_vide(a):
        return []
    elif i == 0:
        return [etiquette(a)]
    else:
        return niveau(gauche(a), i - 1) + niveau(droit(a), i - 1)
