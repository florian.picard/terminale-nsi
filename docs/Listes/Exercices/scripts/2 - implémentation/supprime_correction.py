def supprime(m, i):
    """ Maillon, int -> NoneType
    0 <= i < longueur(m)
    Supprime la valeur i de la chaine dont le premier maillon est m """
    # la liste est un singleton
    if est_vide(m.suivant):
        return None
    # la liste n'est pas un singleton, mais on supprime l'élément de tête :
    # on décale toutes les valeurs de la chaîne et on supprime le dernier
    # maillon. 
    if i == 0:
        courant = m
        while not est_vide(courant.suivant.suivant):
            courant.valeur = courant.suivant.valeur
            courant = courant.suivant
        courant.suivant = None
        return
    # cas général
    # on parcourt toute la liste, et on s'arrête au maillon juste
    # avant celui à supprimer : deux cas sont possibles :
    # soit on supprime le dernier soit un maillon interne.
    courant = m
    count = 0
    while not est_vide(courant.suivant) and count < i - 1:
        count = count + 1
        courant = courant.suivant
    # le prochain maillon à supprimer est courant.suivant
    # on supprime le dernier maillon
    if est_vide(courant.suivant):
        courant.suivant = None
    # on supprime un maillon interne de la liste chaînée
    else:
        courant.suivant = courant.suivant.suivant
