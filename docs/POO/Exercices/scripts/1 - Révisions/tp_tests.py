def test_est_pair():
    # Tests pour la fonction est_pair
    for i in range(128):
        bits = list(map(int, list(bin(i)[2:])))
        expected = (i%2) == 0
        assert est_pair(bits) == expected, f"\ Le test suivant n'a pas été validé.\nEntrée : {bits}\nSortie : {est_pair(bits)}\nAttendu : {expected}"
    return True

# from tp import est_pair
# test_est_pair()
def test_est_plus_grand_kbits():
    # Tests pour la fonction est_plus_grand_kbits
    for i in range(1024):
        bits = list(map(int, list(bin(i)[2:])))
        reponse = est_plus_grand_kbits(bits) 
        expected = all(bits)
        assert reponse == expected, f"\ Le test suivant n'a pas été validé.\nEntrée : {bits}\nSortie : {reponse}\nAttendu : {expected}"
    return True

# test_est_plus_grand_kbits()
import random
def test_appartient():
    # Tests pour la fonction appartient 
    assert appartient([], 0) == False
    assert appartient([0, 1, 2, 3], 0) == True
    assert appartient([0, 1, 2, 3], 3) == True
    assert appartient([0, 1, 2, 3], 4) == False
    return True

# from tp import appartient
# test_appartient()
def test_nombre_occurrences():
    # Tests pour la fonction nombre_occurrences
    assert nombre_occurrences([], 0) == 0
    assert nombre_occurrences([0, 1, 2, 3], 0) == 1
    assert nombre_occurrences([0, 1, 2, 0], 0) == 2
    assert nombre_occurrences([0, 1, 2, 3], 2) == 1
    assert nombre_occurrences([0, 1, 2, 3], 4) == 0
    return True

# from tp import nombre_occurrences
# test_nombre_occurrences()
def test_indices_occurrences():
    # Tests pour la fonction indices_occurrences
    assert indices_occurrences([], 0) == []
    assert indices_occurrences([0, 1, 2, 3], 0) == [0]
    assert indices_occurrences([0, 1, 2, 0], 0) == [0, 3]
    assert indices_occurrences([0, 1, 2, 3], 2) == [2]
    assert indices_occurrences([0, 1, 2, 3], 4) == []
    return True

# from tp import indices_occurrences
# test_indices_occurrences()
import random
def test_maximum():
    # Tests pour la fonction maximum
    assert maximum([1]) == (1, 0)
    assert maximum([1, 2, -1]) == (2, 1)
    assert maximum([1, 2, -1, 2]) == (2, 1)
    for _ in range(1000):
        tab = [random.randint(-10, 10) for _ in range(random.randint(1, 10))]
        expected = max(tab), tab.index(max(tab))
        assert maximum(tab) == expected, f"\ Le test suivant n'a pas été validé.\nEntrée : {tab }\nSortie : {maximum(tab)}\nAttendu : {expected}"
    return True

# from tp import maximum
# test_maximum()
def test_tri_selection():
    """ Tests pour la fonction tri_selection """
    data = [
        [[0], [0]],
        [[9,4,6,1], [1,4,6,9]],
        [[1, 52, 6, -9, 12], [-9, 1, 6, 12, 52]],
        [[1, 2], [1, 2]]
    ]
    for tab, tab_tri in data:
        out = tri_selection(tab)
        assert out == tab_tri, f"\ Le test suivant n'a pas été validé.\nEntrée : {tab}\nSortie : {out}\nAttendu : {tab_tri}"
    print("Tests de tri_selection passés avec succès.")
    return True

# from tp import tri_selection
# test_tri_selection()
