#--- HDR ---#
class Chapeau:
    def __init__(self, t):
        self.taille = t

    def __repr__(self):
        return f"Chap. {self.taille}"

    def affiche(self):
        print(self)

class Pile:
    def __init__(self, max_elt = float('inf')):
        self.contenu = []
        self.max_elt = max_elt

    def __repr__(self):
        return "[" + ", ".join(b.__repr__() for b in self.contenu) + "] (Sommet pile)" 

    def __len__(self):
        return len(self.contenu)

    def affiche(self):
        print(self)

    def est_vide(self):
        return self.contenu == []

    def est_pleine(self):
        return self.max_elt == len(self)

    def empiler(self, c):
        # assert isinstance(c, Chapeau)
        if self.est_pleine():
            raise IndexError("Capacité maximale de la pile atteinte")
        self.contenu.append(c)

    def depiler(self):
        if self.est_vide():
            raise IndexError("Impossible de dépiler : la pile est vide.")
        return self.contenu.pop()
#--- HDR ---#
# Exécuter ce bloc AVANT de traiter les exercices.
# Charge les classes Chapeau et Pile.

c = Chapeau(1)
p = Pile()
p.empiler(c)
p.affiche()
print(p.depiler())
