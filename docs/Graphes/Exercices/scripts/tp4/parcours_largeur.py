def parcours_largeur(G, depart):
    """ Graphe, Sommet -> [Sommet]
    Parcours le graphe G depuis le sommet depart en largeur
    (sachant que la liste des sommets présents dans vus ont déjà été visités). """
    vus = []
    à_explorer = ...
    ...
    while ...
        sommet = ...
        if sommet in vus:
            continue
        # opération de traitement
        vus.append(sommet)
        # ajout des voisins non visités
        for destination in G.voisins(sommet):
            if ...:
                ...
    return vus
