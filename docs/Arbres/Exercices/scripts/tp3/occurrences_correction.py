def occurrences(a, nom):
    """ Arbre, str -> int
    Renvoie le nombre d'occurrences de nom dans a """
    if est_vide(a):
        return 0
    else:
        if etiquette(a) == nom:
            return 1 + occurrences(gauche(a), nom) + occurrences(droit(a), nom)
        else:
            return occurrences(gauche(a), nom) + occurrences(droit(a), nom)
