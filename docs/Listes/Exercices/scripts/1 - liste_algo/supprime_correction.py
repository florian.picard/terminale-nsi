def supprime(l, e):
    """ Liste, int -> Liste
    Supprime la première occurrence de e de la liste l """
    if est_vide(l):
        return creer_vide()
    else:
        if tete(l) == e:
            return queue(l)
        else:
            queue_sans_e = supprime(queue(l), e)
            return ajoute(queue_sans_e, tete(l))
