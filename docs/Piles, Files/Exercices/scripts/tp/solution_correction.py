def solution():
    """ () -> [(int, int)]
    Renvoie la liste des associations pile i <-> file j """
    piles = init_piles()
    files = init_files()
    sol = []
    for i in range(len(piles)):
        for j in range(len(files)):
            if compatibles(piles[i], files[j]):
                sol.append((i + 1, j + 1))
    return sol
