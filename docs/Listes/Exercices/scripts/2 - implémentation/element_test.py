def test_element():
    """ Tests pour la fonction element """
    inputs = tab2maillon([i for i in range(10)])
    outputs_i = [element_i(inputs, i) for i in range(10)]
    outputs_r = [element_r(inputs, i) for i in range(10)]
    expecteds = [i for i in range(10)]
    for i in range(10):
        assert outputs_i[i] == expecteds[i], message(inputs, outputs[i], expecteds[i])
        assert outputs_r[i] == expecteds[i], message(inputs, outputs[i], expecteds[i])
    print("Tests de element passés avec succès.")
    return True
# from tp import element
test_element()
