def diametre(G):
    """ Graphe -> int
    Renvoie la distance maximale entre deux sommets quelconques du graphe """
    d_max = -float("inf")
    sommets_max = None
    for source in G.sommets():
        for destination in G.sommets():
            d = distance(G, source, destination)
            if d > d_max:
                d_max = d
                sommets_max = (source, destination)
    return d_max, sommets_max
