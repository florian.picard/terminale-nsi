def nombre_occurrences(l, e):
    """ Liste, int -> int
    Compte le nombre d'occurrences de e dans l """
    if est_vide(l):
        return 0
    else:
        if e == tete(l):
            return 1 + nombre_occurrences(queue(l), e)
        else:
            return nombre_occurrences(queue(l), e)
