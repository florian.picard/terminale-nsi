---
title: Les tours de Hanoï
---

<div class="fancyquotes" markdown="1">
Claus de Siam a vu, dans ses voyages pour la publication des écrits de l'illustre Fer-Fer-Tam-Tam, dans le grand temple de Bénarès, au-dessous du dôme qui marque le centre du monde, trois aiguilles de diamant, plantées dans une dalle d'airain, hautes d'une coudée et grosses comme le corps d'une abeille. Sur une de ces aiguilles, Dieu enfila au commencement des siècles, 64 disques d'or pur, le plus large reposant sur l'airain, et les autres, de plus en plus étroits, superposés jusqu'au sommet. C'est la tour sacrée du Brahmâ. Nuit et jour, les prêtres se succèdent sur les marches de l'autel, occupés à transporter la tour de la première aiguille sur la troisième, sans s'écarter des règles fixes que nous venons d'indiquer, et qui ont été imposées par Brahma. Quand tout sera fini, la tour et les brahmes tomberont, et ce sera la fin des mondes !
</div>


## Présentation du problème


### Règle du jeu

Le problème des tours de Hanoï est un problème récréatif inventé par Édouard Lucas en 1883. Le jeu est constitué de trois piliers, sur lesquels s’empilent un nombre $n$ de disques (la "difficulté" du problème). Initialement, tous les disques sont placés sur le premier pilier.

![img](hanoi4_init.png)

Le but du jeu est de déplacer la pyramide de disques du pilier $A$ au pilier $C$ en respectant les règles suivantes :

-   on ne peut déplacer qu'un seul disque à la fois ;
-   lorsque l'on déplace un disque, on ne peut déplacer que le disque présent au sommet d'une pile ;
-   lorsque l'on déplace un disque, on ne peut empiler un disque sur un disque de diamètre inférieur.


### Notation

Pour noter les déplacements au cours d'une partie, on utilise la convention suivante : lorsque $X$ et $Y$ sont deux piliers différents parmi les piliers $A$, $B$ et $C$, $X \rightarrow Y$ indique que l'on a déplacé le disque au sommet du pilier $A$ vers le pilier $B$.

On considère la situation initiale suivante :

![img](hanoi4_notation.png)

<div class="exo" markdown="1">
Indiquer la situation finale après les coups $B \rightarrow A$, $C \rightarrow B$, et $C \rightarrow A$.
</div>


## Résolution du problème


### Une méthode naïve

<div class="exo" markdown="1">
La situation initiale est celle où les quatre disques sont empilés sur la tour $A$. Parmi les listes de coups suivants, lesquels ne sont pas des coups autorisés ? Expliquer pourquoi.

-   $A \rightarrow B$, $A \rightarrow C$, $A \rightarrow B$ ;
-   $A \rightarrow C$, $B \rightarrow A$, $C \rightarrow A$ ;
-   $A \rightarrow B$, $B \rightarrow C$, $A \rightarrow B$, $B \rightarrow B$, $C \rightarrow B$.

**Appeler le professeur avant de passer à la suite.**
</div>

<div class="exo" markdown="1">
Résoudre le problème des tours de Hanoï, successivement pour les difficultés 1, 2, 3, 4, et 5. **Noter le nombre de coups réalisés pour résoudre le problème.**

À partir de combien de disques la résolution du problème devient-elle très difficile ?
</div>


### Une méthode magique

<div class="exemple" markdown="1">
1.  Compléter le tableau ci-dessous avec la liste des coups permettant de déplacer **3** disques du pilier $X$ vers le pilier $Y$, où $X$ et $Y$ appartiennent à  $\{A, B, C\}$.
    
    <table border="2" cellspacing="0" cellpadding="6" rules="groups" frame="hsides">
    
    
    <colgroup>
    <col  class="org-left" />
    
    <col  class="org-left" />
    
    <col  class="org-left" />
    
    <col  class="org-left" />
    
    <col  class="org-left" />
    
    <col  class="org-left" />
    </colgroup>
    <thead>
    <tr>
    <th scope="col" class="org-left">De A vers B</th>
    <th scope="col" class="org-left">De A vers C</th>
    <th scope="col" class="org-left">De B vers A</th>
    <th scope="col" class="org-left">De B vers C</th>
    <th scope="col" class="org-left">De C vers A</th>
    <th scope="col" class="org-left">De C vers B</th>
    </tr>
    </thead>
    
    <tbody>
    <tr>
    <td class="org-left">&#xa0;</td>
    <td class="org-left">&#xa0;</td>
    <td class="org-left">&#xa0;</td>
    <td class="org-left">&#xa0;</td>
    <td class="org-left">&#xa0;</td>
    <td class="org-left">&#xa0;</td>
    </tr>
    
    
    <tr>
    <td class="org-left">&#xa0;</td>
    <td class="org-left">&#xa0;</td>
    <td class="org-left">&#xa0;</td>
    <td class="org-left">&#xa0;</td>
    <td class="org-left">&#xa0;</td>
    <td class="org-left">&#xa0;</td>
    </tr>
    
    
    <tr>
    <td class="org-left">&#xa0;</td>
    <td class="org-left">&#xa0;</td>
    <td class="org-left">&#xa0;</td>
    <td class="org-left">&#xa0;</td>
    <td class="org-left">&#xa0;</td>
    <td class="org-left">&#xa0;</td>
    </tr>
    
    
    <tr>
    <td class="org-left">&#xa0;</td>
    <td class="org-left">&#xa0;</td>
    <td class="org-left">&#xa0;</td>
    <td class="org-left">&#xa0;</td>
    <td class="org-left">&#xa0;</td>
    <td class="org-left">&#xa0;</td>
    </tr>
    </tbody>
    </table>

2.  **En déduire (sans aucun "calcul" supplémentaire mais en réutilisant les cases du tableau précédent)** la liste des coups permettant de déplacer **4** disques du pilier $A$ vers le pilier $C$.
</div>

<div class="exo" markdown="1">
En vous inspirant de la méthode de la question précédente, expliquer en français comment vous *feriez* pour résoudre le problème Hanoï de difficulté 5. 

Comment feriez-vous pour résoudre le problème de Hanoï de difficulté 6 ?

**Appeler le professeur avant de passer à la suite.**
</div>

<div class="exo" markdown="1">
Compléter la phrase suivante : « Soit $n \in \mathbb{N}^{*}$. Pour résoudre le problème de Hanoï de difficulté $n$, il suffit de savoir résoudre [le problème de Hanoï de difficulté $n - 1$](\dotfill) »
</div>

<div class="exo" markdown="1">
`départ` et `arrivée` sont deux caractères parmi `'A'` `'B'` et `'C'`. On suppose écrite une fonction `hanoi3` qui étant donné deux arguments `départ` et `arrivée` (supposés distincts), affiche la liste des déplacement permettant de déplacer **3** disques depuis le pilier `départ` vers le pilier `arrivée`.

1.  Écrire une fonction `hanoi4` qui étant donné deux arguments `départ` et `arrivée` (supposés distincts), affiche la liste des déplacement permettant de déplacer **4** disques depuis le pilier `départ` vers le pilier `arrivée`.
2.  Pouvez-vous écrire une fonction `hanoi5` ?
</div>

<div class="exo" markdown="1">
Écrire une fonction `hanoi` qui prend en argument un entier `n` (le nombre de disques), et deux caractères `départ` et `arrivée` (distincts, égaux soit à `'a'`, `'b'` ou `'c'`), affiche la liste des déplacements à effectuer pour résoudre déplacer `n` disques depuis le pilier `départ` vers le pilier `arrivée`.
</div>

