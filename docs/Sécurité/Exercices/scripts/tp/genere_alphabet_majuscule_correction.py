def genere_alphabet_majuscule():
    """ () -> [str]
    Renvoie la liste des 26 lettres de l'alphabet latin en majuscule """
    return [chr(i) for i in range(ord('A'), ord('Z') + 1)]
