def test_queue():
    """ Tests pour la fonction queue """
    m0 = Maillon(1, None)
    m1 = Maillon(2, m0)
    m2 = Maillon(3, m1)
    assert queue(m1) is m0
    assert queue(m2) is m1
    print("Tests de queue passés avec succès.")
    return True

# from tp import queue
test_queue()
