def vaincus_gagnant(a):
    """ Arbre -> [str]
    Renvoie la liste des joueurs ayant joué un match contre le gagnant de la compétition d'arbre a """
    if est_feuille(a):
        return []
    elif etiquette(gauche(a)) == etiquette(a):
        return vaincus_gagnant(gauche(a)) + [etiquette(droit(a))]
    elif etiquette(droit(a)) == etiquette(a):
        return [etiquette(gauche(a))] + vaincus_gagnant(droit(a))
