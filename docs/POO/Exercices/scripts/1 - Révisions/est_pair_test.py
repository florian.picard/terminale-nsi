def test_est_pair():
    # Tests pour la fonction est_pair
    for i in range(128):
        bits = list(map(int, list(bin(i)[2:])))
        expected = (i%2) == 0
        assert est_pair(bits) == expected, f"\ Le test suivant n'a pas été validé.\nEntrée : {bits}\nSortie : {est_pair(bits)}\nAttendu : {expected}"
    return True

# from tp import est_pair
test_est_pair()
