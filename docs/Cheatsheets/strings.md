---
title: Chaînes de caractères
---


## Chaînes de caractères

Une chaîne de caractères est une suite ordonnée de caractères : on accède à un caractère particulier à l'aide de son indice dans la chaîne.

``` py linenums="1" 
texte = "Hello World!"    
print(texte[0])
print(texte[1])
print(texte[-1])
```

```
H
e
!
```


## Caractère d'échappement `\`

Il est parfois nécessaire **d'échapper** certains caractères : on le fait en plaçant un `\` devant le caractère correspondant.

``` py linenums="1" 
citation = "Shakespeare dit \"To be or not to be\""
print(citation)
phrase = 'L\'eleve se dit "facile" mais l\'est-ce vraiment ?'
print(phrase)
```

```
Shakespeare dit "To be or not to be"
L'eleve se dit "facile" mais l'est-ce vraiment ?
```

<table border="2" cellspacing="0" cellpadding="6" rules="groups" frame="hsides">


<colgroup>
<col  class="org-left" />

<col  class="org-left" />
</colgroup>
<thead>
<tr>
<th scope="col" class="org-left">Caractère</th>
<th scope="col" class="org-left">Affichage</th>
</tr>
</thead>

<tbody>
<tr>
<td class="org-left"><code>\'</code></td>
<td class="org-left">Apostrophe</td>
</tr>


<tr>
<td class="org-left"><code>\"</code></td>
<td class="org-left">Guillemets</td>
</tr>


<tr>
<td class="org-left"><code>\\</code></td>
<td class="org-left">Backslash</td>
</tr>


<tr>
<td class="org-left"><code>\n</code></td>
<td class="org-left">Retour à la ligne</td>
</tr>


<tr>
<td class="org-left"><code>\t</code></td>
<td class="org-left">Tabulation</td>
</tr>
</tbody>
</table>

``` py linenums="1" 
print("Il est vrai que : \n\t\"/!\\ Tel est pris qui croyait prendre /!\\\"")
```

```
Il est vrai que : 
	"/!\ Tel est pris qui croyait prendre /!\"
```


## Formatage de chaînes avec les f-strings

L'utilisation des f-string est recommandée pour formater facilement des chaînes de caractères. 

``` py linenums="1" 
nom, age = "Louise", 17
print(f"{nom}, elle a {age} ans.")
```

```
Louise, elle a 17 ans.
```

Attention, il devient alors impossible d'utiliser normalement les caractères `{` et `}` le caractère d'échappement. 

``` py linenums="1" 
a, b, c = 1, 2, 3
print(f"L'ensemble vide est noté {}. l'ensemble E = {a, b, c}")
```

```
  Input In [1868]
    print(f"L'ensemble vide est noté {}. l'ensemble E = {a, b, c}")
                                                                  ^
SyntaxError: f-string: empty expression not allowed
```

Pour afficher les caractères `{` et `}` dans un f-string, on les double.


### L'astuce du `=`

``` py linenums="1" 
ma_variable = 42
print(f"On a : {ma_variable=}")
```

```
On a : ma_variable=42
```


## Méthodes du type `str`


### Méthodes `join` et `split`


#### Construire une chaîne avec `join`

La méthode `join` appliquée à la chaîne de caractères `sep` avec pour argument `liste` intercale `sep` entre tous les éléments de `liste`.

``` py linenums="1" 
sep = " | "    
liste = ["a", "b", "c"]
print(sep.join(liste))
```

```
a | b | c
```

Attention, tous les éléments de `liste` doivent être des chaînes de caractères. 

``` py linenums="1" 
print(",".join([1, 2, 3]))
```

```
---------------------------------------------------------------------------
TypeError                                 Traceback (most recent call last)
Input In [1871], in <cell line: 1>()
----> 1 print(",".join([1, 2, 3]))

TypeError: sequence item 0: expected str instance, int found
```

On peut les convertir avec la fonction `str` si besoin. 

``` py linenums="1" 
liste = [1, 2, 3]
liste_convertie = [str(e) for e in liste]
print(liste_convertie)
print(",".join(liste_convertie))
```

```
['1', '2', '3']
1,2,3
```


#### Découper une chaîne avec `split`

Il est possible de découper un texte le long d'un séparateur donné à l'aide de la méthode `split`. 

``` py linenums="1" 
texte = "0,1,2,3"    
print(texte.split(","))
```

```
['0', '1', '2', '3']
```

En découpant le texte selon le caractère `\n`, on récupère les lignes qui le constituent.

``` py linenums="1" 
texte = """
Celui qui croyait au ciel
Celui qui n'y croyait pas
Tous deux adoraient la belle
Prisonnière des soldats
Lequel montait à l'échelle
Et lequel guettait en bas 
"""    
print(texte.split("\n"))
```

```
['', 'Celui qui croyait au ciel', "Celui qui n'y croyait pas", 'Tous deux adoraient la belle', 'Prisonnière des soldats', "Lequel montait à l'échelle", 'Et lequel guettait en bas ', '']
```

La méthode `split` peut par exemple être particulièrement utile lorsque l'on souhaite lire des données au format `csv`. 

``` py linenums="1" 
texte = """nom,prenom,date_naissance
Durand,Jean-Pierre,23/05/1985
Dupont,Christophe,15/12/1967
Terta,Henry,12/06/1978"""

def read_csv(texte, sep = ','):
    lignes = texte.split("\n")
    header = lignes[0].split(sep)
    data = [lignes[i].split(sep) for i in range(1, len(lignes))]
    return header, data

print(read_csv(texte))    
```

```
(['nom', 'prenom', 'date_naissance'], [['Durand', 'Jean-Pierre', '23/05/1985'], ['Dupont', 'Christophe', '15/12/1967'], ['Terta', 'Henry', '12/06/1978']])
```


### Méthodes `ljust`, `center`, `rjust`

`texte.center(taille, car)` créé une chaîne de caractère de longueur totale `taille`, dans laquelle `texte` est centré. Pour ce faire, les caractères `car` sont ajoutés afin d'obtenir la taille souhaitée. Si `car` n'est pas précisé, le caractère espace est utilisé par défaut.

``` py linenums="1" 
print("".center(30, "#")) 
print(" Exercice ".center(30, "#")) 
print("".center(30, "#")) 
```

```
##############################
########## Exercice ##########
##############################
```

La méthode `ljust` justifie le texte à gauche.

``` py linenums="1" 
print(" Exercice ".ljust(30, '-')) 
```

```
 Exercice --------------------
```

La méthode `ljust` justifie le texte à droite.

``` py linenums="1" 
print(" Exercice ".rjust(30)) 
```

```
                     Exercice 
```


### Méthodes `lstrip`, `strip`, `rstrip`

``` py linenums="1" 
spam = '    Hello World    '
print(f"|{spam}|")
print(f"|{spam.lstrip()}|")
print(f"|{spam.strip()}|")
print(f"|{spam.rstrip()}|")
```

```
|    Hello World    |
|Hello World    |
|Hello World|
|    Hello World|
```


### Méthode `replace`

``` py linenums="1" 
print("chap 1 tp 1 récursivité.py".replace(" ", "_"))    
```

```
chap_1_tp_1_récursivité.py
```

