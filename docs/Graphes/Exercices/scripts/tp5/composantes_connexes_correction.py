def composantes_connexes(G):
    """ Graphe -> {Sommet:int}
    Détermine les composantes connexes du graphe G sous la forme d'un dictionnaire """
    composantes = dict()
    composante_num = 0
    for sommet in G.sommets():
        if sommet in composantes:
            continue
        composante_num += 1
        for atteignable in parcours_profondeur(G, sommet):
            composantes[atteignable] = composante_num
    return composantes
