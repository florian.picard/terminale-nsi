---
title: Implémentation par des listes chaînées
---


## Listes chaînées : description


### Introduction

On rappelle que l'interface du type `Liste` est la suivante : 

<table border="2" cellspacing="0" cellpadding="6" rules="groups" frame="hsides">


<colgroup>
<col  class="org-left" />

<col  class="org-left" />
</colgroup>
<thead>
<tr>
<th scope="col" class="org-left">Fonction</th>
<th scope="col" class="org-left">Description</th>
</tr>
</thead>

<tbody>
<tr>
<td class="org-left"><code>creer_vide()</code></td>
<td class="org-left">Renvoie une liste vide</td>
</tr>


<tr>
<td class="org-left"><code>est_vide(l)</code></td>
<td class="org-left">Renvoie <code>True</code> si et seulement si la liste est vide</td>
</tr>


<tr>
<td class="org-left"><code>tete(l)</code></td>
<td class="org-left">Renvoie le premier élément de la liste \(l\) (l'élément dit <b>de tête</b>)</td>
</tr>


<tr>
<td class="org-left"><code>queue(l)</code></td>
<td class="org-left">Renvoie la liste constituée de tous les éléments de \(l\) à l'exception du premier</td>
</tr>


<tr>
<td class="org-left"><code>ajoute(l, e)</code></td>
<td class="org-left">Renvoie la liste constituée de l'élément \(e\), suivi des éléments de \(l\)</td>
</tr>


<tr>
<td class="org-left"><code>affiche(l)</code></td>
<td class="org-left">Affiche la liste des éléments de \(l\) sur la sortie standard, séparés par le caractère <code>-</code>.</td>
</tr>


<tr>
<td class="org-left">&#xa0;</td>
<td class="org-left">La liste vide est représentée par <code>•</code>.</td>
</tr>
</tbody>
</table>

On se propose de donner une implémentation de cette interface via un objet nommé *liste chaînée*. En python, on peut représenter les listes de la manière suivante : chaque élément de la liste est associé à un **objet**, que nous appelerons un "maillon" (faisant partie d'une "chaîne" de maillons) qui comporte deux attributs :

-   un attribut `valeur` qui stocke la valeur de l'élément considéré ;
-   un attribut `suivant` qui permet de déterminer quel est le prochain maillon dans la chaîne.

On représente **un maillon vide** par la valeur spéciale `None`. Ainsi, on pourra instancier un objet `m` de type `Maillon` de trois façons distinctes :

-   `m = None` : le maillon est vide ;
-   `m = Maillon(v, None)` : le maillon est le seul dans sa chaîne ;
-   `m = Maillon(v, m2)` : le prochain maillon de la chaîne est le maillon `m2` (supposé ici non vide).

Une chaîne de maillons ainsi constituée représente bien une liste d'éléments. La classe `Liste` (donnée pour le moment à titre indicatif) possède donc un seul attribut : `head` une référence vers le premier maillon de la chaîne. 

{{ IDE('scripts/2 - implémentation/Maillon') }}

À l'aide de l'objet `Maillon`, on peut représenter la liste $l = (3, 8, 42, 1)$. Pour chaque élément $e$ de la liste, on créé un maillon qui contient la valeur de l'élément et le lien vers le maillon suivant. 

``` py linenums="1" 
maillon4 = Maillon(1, None)
maillon3 = Maillon(42, maillon4)
maillon2 = Maillon(8, maillon3)
maillon1 = Maillon(3, maillon2)

print(maillon2.valeur)
print(maillon2.suivant)
print(maillon2.suivant.valeur)
```

Créer des listes de cette façon est très fastidieux, en raison de l'utilisation de multiples variables. On peut condenser le code de la manière à n'utiliser qu'une seule variable :

``` py linenums="1" 
m = Maillon(1, None)
m = Maillon(42, m)
m = Maillon(8, m)
m = Maillon(3, m)
# encore plus court :
# m = Maillon(3, Maillon(8, Maillon(42, Maillon(1, None))))
    
print(m.valeur)
print(m.suivant)
print(m.suivant.valeur)
```

```
3
<__main__.Maillon object at 0x7f5c9e8bc460>
8
```


### Premier exemple

Écrire les instructions python permettant de stocker à l'aide d'objets de type `Maillon`, ou de la valeur spéciale `None` les listes $l_1 = ()$, $l_2 = (-1)$, $l_3 = (5, 6, 9, -1)$, $l_4 = (-5, 9, 13)$.

{{ IDE('scripts/2 - implémentation/exemple') }}


## Implémentation de l'interface de base des listes


### Créer une liste vide

Écrire une fonction `creer_vide` qui renvoie, **avec les conventions de l'énoncé**, un maillon vide. 

{{ IDE('scripts/2 - implémentation/creer_vide') }}


### Tester si un maillon est vide

Écrire une fonction `est_vide` étant donné un maillon `m` renvoie `True` si et seulement si celui-ci est vide. 

{{ IDE('scripts/2 - implémentation/est_vide') }}


### Élément de tête

Écrire une fonction `tete` qui étant donné un maillon non-vide `m`, renvoie la valeur stockée dans ce maillon.

{{ IDE('scripts/2 - implémentation/tete') }}


### Élément de queue

Écrire une fonction `queue` qui étant donné un maillon non-vide `m`, renvoie le maillon suivant de la chaîne de maillons.

{{ IDE('scripts/2 - implémentation/queue') }}


### Ajout d'un maillon

Écrire une fonction `ajoute_debut` qui étant donné un maillon `m` et un élément `e` renvoie le maillon `m'` dont l'attribut valeur est `e` et dont l'attribut `suivant` est `m`.

{{ IDE('scripts/2 - implémentation/ajoute_debut') }}


## Parcours d'une liste chaînée


### Affichage d'une liste chaînée

Pour afficher une liste chaînée, il faut parcourir tous les maillons la composant. Pour cela, deux approches sont possibles : une méthode récursive et méthode itérative.  


#### Méthode récursive

Si le maillon est vide alors on affiche un symbole indiquant qu'il s'agit du maillon vide. Sinon, on affiche la valeur contenue dans le maillon, puis on affiche les maillons suivants de la chaîne.

{{ IDE('scripts/2 - implémentation/affiche') }}

<div class="question" id="org1e29341">
<ol class="org-ol">
<li>Que se passe-t-il si on échange les lignes <code>7</code> et <code>8</code> du code de la fonction <code>affiche_rec</code> ?</li>
<li><p>
À quoi sert l'argument nommé <code>end</code> de la fonction <code>print</code> ? Modifier le code de la fonction précédente afin que l'affichage effectué par l'instruction <code>affiche_rec(m3)</code> soit : 
</p>

<pre class="example">
5 | 6 | 9 | -1 | x
</pre></li>
</ol>

</div>


#### Méthode itérative

On initialise une variable `maillon_courant` qui stocke le maillon courant du parcours de la liste. Tant que le maillon courant n'est pas vide (ce qui veut dire que l'on n'est pas encore au bout de la chaîne), on affiche la valeur du maillon courant puis le maillon courant devient le maillon suivant dans la chaîne. Lorsque l'on atteint la fin de la chaîne, on affiche un symbole indiquant qu'il s'agit du maillon vide. 

{{ IDE('scripts/2 - implémentation/affiche') }}

On utilisera dans la suite simplement l'instruction `affiche` pour afficher une chaîne de maillons. 


### Longueur d'une chaîne

On appelle longueur d'une chaîne de maillons le nombres de maillons la composant. Écrire une fonction `longueur` qui étant donné un maillon `m`, détermine la longueur de la chaîne de maillons dont le premier maillon est `m`.

On écrira une version récursive et une version itérative de cette fonction.

{{ IDE('scripts/2 - implémentation/longueur') }}


### Élément d'indice i

Écrire le code d'une fonction `element` qui étant donné un maillon `m`, et un entier `i`, supposé compatible avec la longueur de la chaîne, renvoie la valeur stockée dans le maillon d'indice $i$ de la chaine dont le premier maillon est `m`.

On écrira une version récursive et une version itérative de cette fonction.

{{ IDE('scripts/2 - implémentation/element') }}


### Remplacer l'élément d'indice i

Écrire le code d'une fonction `remplace` qui étant donné un maillon `m` non vide, un entier `i`, supposé compatible avec la longueur de la chaîne, et un élément `e`, renvoie la chaîne où la valeur du maillon d'indice `i` a été remplacée par `e`. Ainsi, après l'exécution de `remplace(m)` : 

-   le premier maillon de la chaîne est toujours le maillon `m` ;
-   l'attribut `valeur` du maillon d'indice `i` de la chaine est `e`.
-   les autres maillons de la chaîne sont inchangés.

On pourra utiliser au choix un algorithme de parcours récursif ou itératif.

{{ IDE('scripts/2 - implémentation/remplace') }}


### Ajouter un élément à l'indice i

Écrire une fonction `ajoute_position` qui étant donné un maillon `m`, un entier `i`, supposé compatible avec la longueur de la chaîne, et un élément `e`, ajoute un maillon à la chaine de telle sorte que :

-   le premier maillon de la chaîne soit toujours le maillon `m` ;
-   le maillon d'indice `i` de la chaîne ait pour valeur `e` ;
-   l'ordre d'apparition des valeurs des autres maillons de la chaîne est inchangé.

On pourra utiliser au choix un algorithme de parcours récursif ou itératif.

{{ IDE('scripts/2 - implémentation/ajoute_position') }}


### Suppression d'un élément

Écrire une fonction `supprime` qui étant donné un maillon `m`, un entier `i`, supposé compatible avec la longueur de la chaîne, supprime la valeur `i` de la chaine dont le premier maillon est `m` de telle sorte que le premier maillon de la chaîne soit toujours le maillon `m`.

On pourra utiliser au choix un algorithme de parcours récursif ou itératif. On traitera le cas où la liste est constitué d'un seul élément et le cas `i = 0` à part.

{{ IDE('scripts/2 - implémentation/supprime') }}


## Documents

-   [Implémentation par des listes chaînées (sujet)](2 - implémentation_sujet.pdf)

-   Fichiers python :
    -   [templates](scripts/2 - implémentation/tp.py)
    -   [tests](scripts/2 - implémentation/tp_tests.py)

