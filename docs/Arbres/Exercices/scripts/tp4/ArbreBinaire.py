class Noeud:
    def __init__(self, v, g, d):
        self.etiquette = v
        self.gauche = g
        self.droit = d

class ArbreBinaire:
    """Représente un arbre binaire"""
    def __init__(self, n = None):
        """ArbreBinaire, Noeud -> NoneType"""
        self.racine = n

    def est_vide(self):
        """ ArbreBinaire -> bool """
        return ...
    
    def etiquette(self):
        """ ArbreBinaire -> int """
        return ...
    
    def gauche(self):
        """ ArbreBinaire -> ArbreBinaire """
        return ...
    
    def droit(self):
        """ ArbreBinaire -> ArbreBinaire """
        return ...
    
    def est_feuille(self):
        """ ArbreBinaire -> bool """
        return ...
    
    def __str__(self):
        """ ArbreBinaire -> str """
        pass
    
    def ajouter_depuis_tableau(self, tab):
        """ ArbreBinaire, [int] -> Nonetype
        Ajoute à l'arbre binaire self (supposé initiallement vide) les éléments de tab (correspondant à un parcours en largeur de l'arbre) """
        pass
    
    def taille(self):
        """ ArbreBinaire -> int
        Renvoie le nombre de nœuds dont est composé self """
        pass
    
    def appartient(self, e):
        """ ArbreBinaire, int -> bool
        Détermine si l'entier e est une étiquette de l'arbre self """
        pass
    
    def inserer(self, e):
        """ ArbreBinaire, int
        Insère l'entier e dans l'arbre binaire self """
        pass
