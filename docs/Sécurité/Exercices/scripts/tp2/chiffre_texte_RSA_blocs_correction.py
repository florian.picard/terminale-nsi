def chiffre_texte_RSA_blocs(clair, pk):
    """ str, (int, int) -> [int]
    Chiffre le texte clair à l'aide d'un chiffrement RSA par bloc """
    nombres = [ord(c) for c in clair]
    chaine = "".join([str(n).rjust(3, "0") for n in nombres])
    chiffres = []
    for i in range(0, len(chaine), 2):
        n = int(chaine[i:i + 2])
        chiffres.append(chiffre_RSA(n, pk))
    return chiffres
