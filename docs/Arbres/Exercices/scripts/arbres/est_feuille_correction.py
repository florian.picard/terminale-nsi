def est_feuille(a):
    """ Arbre -> bool
    Détermine si a est constitué d'un seul élément """
    return est_vide(gauche(a)) and est_vide(droit(a))
