def renvoie_et_supprime_premier(p_list):
    """ [Processus] -> Processus
    Renvoie et supprime de p_list le processus dont l'attribut arrivee est le plus petit """
    instant = p_list[0].arrivee
    indice = 0
    for i in range(len(p_list)):
        if p_list[i].arrivee < instant:
            instant = p_list[i].arrivee
            indice = i
    return p_list.pop(indice)
