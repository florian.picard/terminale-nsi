def encadre(k):
    """ int -> int
    Détermine le plus grand entier que l'on puisse écrire sur k bits """
    if k == 1:
        return 1
    return 2*encadre(k - 1) + 1
