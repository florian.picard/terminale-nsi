class Processus:
    """Représente un processus"""
    def __init__(self, pid, arrivee, duree):
        """Processus, int, int, int -> Nonetype"""
        pass

    def __repr__(self):
        """ Processus -> str
        Renvoie une chaine de caractère représentant le processus self """
        pass
    
    def avancer(self, d, t):
        """ Processus, int, int -> Nonetype
        Le processus (re)prend son exécution à la date t pour une durée d """
        pass
    
    def est_termine(self):
        """ Processus -> bool
        Détermine si le processus p a terminé son exécution """
        pass
    
    def sejour(self):
        """ Renvoie le temps de séjour de self """
        pass
    
    def execution(self):
        """ Renvoie le temps d'exécution de self """
        pass
    
    def attente(self):
        """ Renvoie le temps d'attente de self """
        pass
    
    def latence(self):
        """ Renvoie le temps de latence de self """
        pass
