def dechiffre_cesar(chiffre, cle):
    """ str, int -> str
    Renvoie le texte clair vérifiant chiffre = chiffre_cesar(clair, cle) """
    clair = ""
    for c in chiffre:
        clair += decale_car(c, -cle)
    return clair
