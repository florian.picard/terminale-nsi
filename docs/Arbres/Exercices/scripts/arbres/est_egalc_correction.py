def est_egalc(a1, a2):
    """ Arbre, Arbre -> bool
    Renvoie True ssi les arbres a1 et a2 ont le même contenu """
    d1 = dict()
    d2 = dict()
    contenu(a1, d1)
    contenu(a2, d2)
    return est_dico_egal(d1, d2)
