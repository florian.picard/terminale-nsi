def test_liste_joueurs():
    """ Tests pour la fonction liste_joueurs """
    for j in ["Kamel", "Carine", "Joris", "Abdou"]:
        assert j in liste_joueurs(a)
    assert not "ada" in liste_joueurs(a)
    print("Tests de liste_joueurs passés avec succès.")
    return True

# from tp import liste_joueurs
test_liste_joueurs()
