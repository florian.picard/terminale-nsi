def init_files():
    f1 = File()
    f1.enfiler(Bonhomme(1))
    f1.enfiler(Bonhomme(2))
    f1.enfiler(Bonhomme(3))
    f1.enfiler(Bonhomme(4))
    f1.enfiler(Bonhomme(5))

    f2 = File()
    f2.enfiler(Bonhomme(5))
    f2.enfiler(Bonhomme(4))
    f2.enfiler(Bonhomme(3))
    f2.enfiler(Bonhomme(2))
    f2.enfiler(Bonhomme(1))

    f3 = File()
    f3.enfiler(Bonhomme(4))
    f3.enfiler(Bonhomme(3))
    f3.enfiler(Bonhomme(5))
    f3.enfiler(Bonhomme(1))
    f3.enfiler(Bonhomme(2))

    f4 = File()
    f4.enfiler(Bonhomme(3))
    f4.enfiler(Bonhomme(2))
    f4.enfiler(Bonhomme(1))
    f4.enfiler(Bonhomme(5))
    f4.enfiler(Bonhomme(4))
    return f1, f2, f3, f4
