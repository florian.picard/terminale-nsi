def remplace_r(m, i, e):
    """ Maillon, int, int -> NoneType
    m est non vide, 0 <= i < longueur(m)
    Remplace par e la valeur du i-ème maillon de la chaine commençant par m """
    # version récursive
    pass

def remplace_i(m, i, e):
    """ Maillon, int, int -> NoneType
    m est non vide, 0 <= i < longueur(m)
    Remplace par e la valeur du i-ème maillon de la chaine commençant par m """
    # version itérative
