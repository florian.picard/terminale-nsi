---
title: Fonctions récursives
---


## Suites définies par une relation de récurrence


### Suites géométriques

On dit qu'une suite $(u_n)$ est géométrique lorsqu'il existe un nombre
réel $a$ et un nombre réel $q$ tels que $u_0 = a$ et que pour tout $n
\geq 1$ on ait $u_{n} = q\times u_{n - 1}$. On écrit souvent ces
informations de la manière suivante :

$$ u_n = \begin{cases} a \quad \textrm{si } n = 0 \\ q\times u_{n - 1}
\quad \textrm{sinon.}  \end{cases} $$

Écrire le code d'une fonction récursive `géométrique` qui prend en
argument un entier `n`, deux nombres flottants `a` et `q`, et qui renvoie
$u_n$, quand $(u_n)$ est une suite géométrique de raison $q$ et de
premier terme $a$.

{{ IDE('scripts/tp/geometrique') }}


### Suite arithmétiques

On dit qu'une suite $(u_n)$ est arithmétique lorsqu'il existe un nombre réel $a$ et un nombre réel $q$ tels que $u_0 = a$ et que pour tout $n \geq 1$ on ait $u_{n} = q + u_{n - 1}$. On écrit souvent ces informations de la manière suivante :

$$
u_n = \begin{cases}
a \quad \textrm{si } n = 0 \\
q + u_{n - 1} \quad \textrm{sinon.}
\end{cases}
$$

Écrire le code d'une fonction récursive `arithmétique` qui prend en argument un entier `n`, deux nombres flottants `a` et `q`, et qui renvoie $u_n$, quand $(u_n)$ est une suite arithmétique de raison $q$ et de premier terme $a$.

{{ IDE('scripts/tp/arithmetique') }}


### Fonction factorielle

Pour tout entier positif ou nul on définit la fonction factorielle de $n$ de la manière suivante :

-   Si $n = 0$, alors la factorielle de $n$ vaut 1 ;
-   Sinon, la factorielle de $n$ vaut le produit des $n$ premiers entiers strictement positifs : $1 \times 2\times 3 \times \ldots \times n$.

On note $n!$ le nombre factorielle de $n$

Écrire une fonction python récursive `factorielle` qui prend en argument un entier positif `n` et qui renvoie le nombre $n!$. 

{{ IDE('scripts/tp/factorielle') }}


### Suite de Fibonnacci

<div class="fancyquotes" id="org7fbac08">
<p>
La suite doit son nom à Leonardo Fibonacci qui, dans un problème récréatif posé dans l'ouvrage Liber abaci publié en 1202, décrit la croissance d'une population de lapins :
</p>

<p>
« Quelqu’un a déposé un couple de lapins dans un certain lieu, clos de toutes parts, pour savoir combien de couples seraient issus de cette paire en une année, car il est dans leur nature de générer un autre couple en un seul mois, et qu’ils enfantent dans le second mois après leur naissance. »
</p>

</div>

On note $F_n$ le nombre de lapins présents au début du $n$-ième mois. Jusqu'à la fin du deuxième mois,  les lapins n'ont pas commencé à se reproduire, il y a donc $F_1 = F_2 = 1$ couple.
Lorsque $n \geq 3$, on admet que le nombre $F_n$ de couples lors du $n$-ième mois est donné par la formule suivante :
    $$
    F_n = F_{n-2} + F_{n - 1}
    $$

<div class="exo" markdown="1">
1.  Calculer les 10 premiers nombres de fibonacci.
2.  Écrire une fonction récursive `fibonacci` qui prend en argument un entier positif `n` et qui calcule le nombre $F_n$.
</div>

{{ IDE('scripts/tp/fibonacci') }}


## Fonctions récursives


### Encadre

<div class="exo" id="org45ca0ef">
<p>
L'objectif de cet exercice est d'écrire une version récursive de la fonction <code>encadre</code> qui étant donné un nombre entier <code>k</code> détermine la valeur du plus grand entier \(n\) que l'on puisse écrire en base 2 avec <code>k</code> bits.
</p>

<ol class="org-ol">
<li>Pour quelles valeurs de \(k\) est-il facile de calculer <code>encadre(k)</code> ?</li>
<li>On suppose dans cette question qu'un nombre entier \(n\) s'écrit en base 2 avec \(k\) bits.
<ol class="org-ol">
<li><p>
Quel est le plus grand entier \(n\) que l'on puisse écrire avec \(k - 1\) bits ?
</p>

<p>
Exprimer \(n\) en fonction de \(k\).
</p></li>
<li><p>
Quel est le plus grand entier \(n\) que l'on puisse écrire avec \(k\) bits ?
</p>

<p>
Exprimer \(n\) en fonction de \(k\).
</p></li>
<li>En déduire l'expression de <code>encadre(k)</code> en fonction de <code>encadre(k-1)</code>.</li>
</ol></li>
<li>En déduire le code d'une fonction récursive <code>encadre</code> qui réponde au problème de l'énoncé.</li>
</ol>

</div>

{{ IDE('scripts/tp/encadre') }}


### Nombres de bits

<div class="exo" id="org661fe65">
<p>
L'objectif de cet exercice est d'écrire une version récursive de la fonction <code>nombre_bits</code> qui étant donné un entier <code>n</code> détermine le nombre <code>k</code> de bits présents dans l'écriture en base 2 du nombre <code>n</code>. 
</p>

<ol class="org-ol">
<li>Pour quelle(s) valeur(s) de \(n\) est-il facile de calculer <code>nombre_bits(n)</code> ?</li>
<li>On suppose dans cette question qu'un nombre entier \(n\) s'écrit en base 2 avec \(k\) bits.
<ol class="org-ol">
<li><p>
Avec combien de bits s'écrit en base 2 le nombre <code>n//2</code> ?
</p>

<p>
Exprimer la réponse en fonction de \(k\). 
</p></li>
<li>En déduire l'expression de <code>nombre_bits(n)</code> en fonction de <code>nombre_bits(n//2)</code>.</li>
</ol></li>
<li>En déduire le code d'une fonction récursive <code>nombre_bits</code> qui réponde au problème de l'énoncé.</li>
</ol>

</div>

{{ IDE('scripts/tp/nombre_bits') }}


### Appartient

L'objectif de cet exercice est d'écrire une version récursive de la fonction `appartient` qui étant donné un tableau (éventuellement vide) `tab` d'entiers et entier `e`, détermine si l'élément `e` est présent dans le tableau `tab`.
On commence pour cela par écrire une fonction `appartient_aux` qui étant donné un tableau (éventuellement vide) `tab` d'entiers, un entier `e` et un entier `i` détermine si l'élément `e` est présent parmi les `i` premiers éléments du tableau `tab`. Si $n$ est la taille du tableau `tab`, on supposera que $0 \leq i \leq n$.

1.  Soient `tab` un tableau et `e` un élément.
    
    Que doit renvoyer l'instruction `appartient(tab, e, 0)`  ?
2.  On suppose dans cette questions que $1 \leq i \leq n$.
    1.  Quel est l'indice du $i$-ème élément du tableau `tab` ?
        
        Exprimer votre réponse en fonction de $i$.
    2.  Avec quels arguments faut-il appeler la fonction `appartient_aux` pour déterminer si l'élément `e` est présent parmi les $i - 1$ premiers éléments du tableau ?
3.  Décommenter et compléter le code ci-dessous.

``` py linenums="1" 
def appartient_aux(tab, e, i):
    """ [int], int, int -> bool 
    0 <= i <= len(tab)
    Détermine si l'élément e est présent parmi les i premiers éléments de tab. """
    # if i == 0:
        # return ...............................
    # else:
        # On teste si le i-ème élément du tableau est l'élément recherché.
        # if tab[............] == .............:
        #     return ...........................
        # Sinon on recherche l'élément e parmi les i-1 premiers éléments. 
        # else:
        #     return ...........................

def appartient(tab, e):
    """ [int], int -> bool 
    Détermine si l'élément e est présent dans le tableau tab. """
    # return .....................................
```


### Nombre d'occurrences

En vous inspirant de l'algorithme de l'exercice précédent, écrire une fonction `nombre_occurrences_aux` qui étant donné un tableau (éventuellement vide) `tab` d'entiers, un entier `e`, et un entier `i` détermine le nombre de fois où l'élément `e` est apparait parmi les `i` premiers éléments du tableau `tab`. Si $n$ est la taille du tableau `tab`, on supposera que $0 \leq i \leq n$.

``` py linenums="1" 
def nombre_occurrences_aux(tab, e, i):
    """ [int], int, int -> int
    0 <= i <= len(tab)
    Renvoie le nombre d'occurences de e parmi les i premiers éléments de tab """
    pass
```

