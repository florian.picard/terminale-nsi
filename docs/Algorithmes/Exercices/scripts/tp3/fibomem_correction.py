def fibomem(n, memo):
    """ int -> int
    Renvoie le terme d'indice n de la suite de Fibonacci """
    if n in memo: 
        return memo[n]
    if n <= 1:
        memo[n] = n
        return memo[n]
    else:
        inter1 = fibomem(n - 1, memo)
        inter2 = fibomem(n - 2, memo)
        memo[n] = inter1 + inter2
        return memo[n]
