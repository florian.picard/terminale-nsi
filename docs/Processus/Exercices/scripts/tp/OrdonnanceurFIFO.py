class OrdonnanceurFIFO:
    """Représente un ordonnanceur"""
    def __init__(self, q=1):
        """OrdonnanceurFIFO, int -> Nonetype"""
        pass

    def ajoute_processus(self, p_list):
        """ OrdonnanceurFIFO, [Processus] -> Nonetype
        Ajoute à self.file_processus les processus de p_list par ordre d'arrivée """
        pass
    
    def exec_quantum(self):
        """ OrdonnanceurFIFO -> Nonetype
        Simule l'exécution d'un quantum """
        pass
    
    def exec(self):
        """ OrdonnanceurFIFO -> Chronogramme
        Simule l'exécution des processus de la file et en renvoie le chronogramme """
        pass
