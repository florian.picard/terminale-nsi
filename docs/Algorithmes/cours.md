---
title: Techniques de programmation
---


## Styles de programmation


### Vocabulaire

-   **Effet de bord:** Une fonction est à effet de bord lorsqu'elle modifie l'état de la mémoire de l'ordinateur.
-   **Objet non mutable:** Lorsque l'on ne peut pas modifier l'état d'un objet après sa création, on dit que celui-ci est non mutable. Par exemple `True` en Python est un objet non mutable. La plupart des objets sont mutables en Python.

<div class="exemple" markdown="1">
-   En python, le type `tuple` est **non-mutable**, tandis que le type `list` est mutable. 
    
    ``` py linenums="1" 
      ma_liste = [1, 2, 3]
      mon_tuple = (1, 2, 3) # Ne soulève pas d'erreurs
      mon_tuple[0] += 1     # Soulève une erreur lors de son exécution
    ```
    
    ```
    ---------------------------------------------------------------------------
    TypeError                                 Traceback (most recent call last)
    Input In [66], in <cell line: 3>()
          1 ma_liste = [1, 2, 3]
          2 mon_tuple = (1, 2, 3) # Ne soulève pas d'erreurs
    ----> 3 mon_tuple[0] += 1
    
    TypeError: 'tuple' object does not support item assignment
    ```

-   Une fonction ayant un effet de bord peut avoir un comportement différent alors qu'elle est appelée avec les mêmes arguments. 
    
    ``` py linenums="1" 
      ma_liste = [1, 2, 3]
      def f(x):
          """ int -> [int] """
          for i in range(len(ma_liste)):
              ma_liste[i] += x
          return ma_liste
      print(f(1))
      print(f(1))
    ```
    
    ```
    [2, 3, 4]
    [3, 4, 5]
    ```

-   Au contraire, une fonction n'ayant pas d'effet de bord a nécessairement le même comportement lors d'appels successifs. 
    
    ``` py linenums="1" 
      ma_liste = (1, 2, 3)
      def g(x):
          """ int -> [int] """
          return [e + x for e in ma_liste]
      print(g(1))
      print(g(1))
    ```
    
    ```
    [2, 3, 4]
    [2, 3, 4]
    ```
</div>

Dans certains langages de programmation, on dit qu'une fonction est **pure** lorsqu'elle ne possède pas d'effet de bord, et on dit qu'elle est **impure** lorsqu'elle possède des effets de bord.


### Programmation fonctionnelle

Le style de programmation fonctionnel est proche de l'écriture de preuves mathématiques : on décrit un programme à l'aide d'appel de fonctions et on les compose entre elles. On utilise des appels récursifs pour obtenir un comportement similaire à une boucle. Il est caractérisé par :

-   l'absence d'effets de bord
-   les objets sont non mutables
-   les fonctions réalisent des calculs qui renvoient toujours le même résultat

On parle de **paradigme fonctionnel**. 

<div class="exemple" markdown="1">
La fonction suivante ajoute `x` à tous les éléments de la liste `l`.

La liste `l` **n'est pas** modifiée.

``` py linenums="1" 
def f_fonc(l, x):
    """ [int], int -> [int] """
    if l == []:
        return []
    else:
        return [l[0] + x] + f_fonc(l[1:], x)
l = [1, 2, 3]
print(f_fonc(l, 1))
print(l)
```

```
[2, 3, 4]
[1, 2, 3]
```
</div>


### Programmation impérative

Le style de programmation impérative est très proche de la manière de fonctionner d’un
ordinateur : dans ce style de programmation, on exécute des instructions qui modifient l’état de
l’ordinateur. On utilise des boucles pour répéter des instructions. Il est caractérisé par :

-   la présence d’effets de bord
-   les objets sont mutables
-   l’ordinateur exécute des instructions. Le résultat dépend de l'état actuel de l'ordinateur.

On parle de **paradigme impératif**.

<div class="exemple" markdown="1">
La fonction suivante ajoute `x` à tous les éléments de la liste `l`.

La liste `l` **est** modifiée.

``` py linenums="1" 
def f_imp(l, x):
    """ [int], int -> [int] """
    for i, e in enumerate(l):
        l[i] += x
    return l
l = [1, 2, 3]
print(f_imp(l, 1))
print(l)
```

```
[2, 3, 4]
[2, 3, 4]
```
</div>


## Complexité d'un algorithme

<div class="definition" markdown="1">
Un **algorithme** est un procédé automatique permettant de résoudre un problème en un nombre fini d'étapes. La **complexité en temps** d'un algorithme est le nombre d'opérations effectuées en fonction de $n$, la taille des données du problème. 
</div>

<div class="exemple" markdown="1">
On s'intéresse au nombre d'opérations effectuées plutôt qu'au temps, car tous les ordinateurs n'effectuent pas le même nombre d'opérations par seconde.

<table border="2" cellspacing="0" cellpadding="6" rules="groups" frame="hsides">


<colgroup>
<col  class="org-left" />

<col  class="org-left" />

<col  class="org-left" />

<col  class="org-left" />

<col  class="org-left" />

<col  class="org-left" />
</colgroup>
<thead>
<tr>
<th scope="col" class="org-left">Taille</th>
<th scope="col" class="org-left">\(\log_{2}(n)\)</th>
<th scope="col" class="org-left">\(n\)</th>
<th scope="col" class="org-left">\(n\log_{2}(n)\)</th>
<th scope="col" class="org-left">\(n^2\)</th>
<th scope="col" class="org-left">\(2^n\)</th>
</tr>
</thead>

<tbody>
<tr>
<td class="org-left">10</td>
<td class="org-left">0.003 ms</td>
<td class="org-left">0.01ms</td>
<td class="org-left">0.03 ms</td>
<td class="org-left">0.1 ms</td>
<td class="org-left">1 ms</td>
</tr>


<tr>
<td class="org-left">100</td>
<td class="org-left">0.006 ms</td>
<td class="org-left">0.1 ms</td>
<td class="org-left">0.6 ms</td>
<td class="org-left">10 ms</td>
<td class="org-left">10<sup>14</sup> siècles</td>
</tr>


<tr>
<td class="org-left">1000</td>
<td class="org-left">0.01 ms</td>
<td class="org-left">1ms</td>
<td class="org-left">10 ms</td>
<td class="org-left">1s</td>
<td class="org-left">&#xa0;</td>
</tr>


<tr>
<td class="org-left">10<sup>4</sup></td>
<td class="org-left">0.013 ms</td>
<td class="org-left">10 ms</td>
<td class="org-left">0.1 s</td>
<td class="org-left">100 s</td>
<td class="org-left">&#xa0;</td>
</tr>


<tr>
<td class="org-left">10<sup>5</sup></td>
<td class="org-left">0.016 ms</td>
<td class="org-left">100 ms</td>
<td class="org-left">1.6 s</td>
<td class="org-left">3 h</td>
<td class="org-left">&#xa0;</td>
</tr>


<tr>
<td class="org-left">10<sup>6</sup></td>
<td class="org-left">0.02 ms</td>
<td class="org-left">1s</td>
<td class="org-left">20 s</td>
<td class="org-left">10 jours</td>
<td class="org-left">&#xa0;</td>
</tr>
</tbody>
</table>

On considère que la plupart des opérations élémentaires s'effectuent en temps constant, à l'exception notable de l'exponentiation qui s'effectuent en un temps proportionnel à la taille de l'exposant. 

![img](./opérations.png)
</div>

<div class="theoreme" markdown="1">
On rappelle l'implémentation python de l'algorithme du tri par sélection. 

``` py linenums="1" 
def ins_trie(l, e):
  """ Liste, int -> Liste
  l est triée par ordre croissant """
  if est_vide(l):
    return ajoute(l, e)
  else:
    if e < tete(l):
      return ajoute(l, e)
    else:
      a = ins_trie(queue(l),e)
      return ajoute(a, tete(l))
```

``` py linenums="1" 
def tri_insertion(l):
  """ Liste -> Liste
  Trie la liste l à l'aide de l'algorithme du tri par insertion """
  if est_vide(l):
    return l
  else:
    rst = tri_insertion(queue(l))
    return ins_trie(rst, tete(l))
```

On suppose que les fonctions `est_vide`, `ajoute`, `tete` et `queue` s'exécutent en temps constant. La complexité de la fonction `ins_trie` est alors dans le pire des cas linéaire en la taille de la liste. La complexité de la fonction `tri_insertion` est dans le pire des cas quadratique en la taille de la liste. 
</div>

<div class="theoreme" markdown="1">
On rappelle l'implémentation python du tri fusion. 

``` py linenums="1" 
def tri_fusion(l):
    """ Liste -> Liste
    Trie la liste l (tri fusion) """
    if est_vide(l) or est_singleton(l):
        return l
    else:
        l1, l2 = diviser(l)
        l1 = tri_fusion(l1)
        l2 = tri_fusion(l2)
        return fusionner(l1, l2)
```

On suppose que les opérations `est_vide` et `est_singleton` s'effectuent en temps constant, que les opérations `fusionner` et `diviser` ont une complexité linéaire. Pour une liste `l` de taille $n$, on note $c_n$ est le nombre d'opérations élémentaires effectuées par `tri_fusion(l)`. On a :

$$\begin{cases}
c_0 = c_1 = 0 \\
c_n = 2c_{n/2} + 2n
\end{cases} 
$$

Lorsque $n = 2^k$ on a : $c_{n} = 2 n\log_2(n)$.

On dit que la complexité de la fonction `tri_fusion` est **quasilinéaire**.
</div>

<div class="exemple" markdown="1">
On fait la moyenne du temps d'execution des deux algorithmes. `n` est la taille de la liste à trier.

![img](tri_insertion2.png)

![img](tri_fusion2.png)
</div>

<div class="exemple" markdown="1">
Dresser l'[arbre d'appel](https://www.recursionvisualizer.com/?function_definition=def%20diviser%28tab%29%3A%0A%20%20%20%20%22%22%22%20Liste%20-%3E%20Liste%2C%20Liste%0A%20%20%20%20Divise%20la%20liste%20en%20deux%20listes%20%22%22%22%0A%20%20%20%20if%20len%28tab%29%20%3D%3D%200%3A%0A%20%20%20%20%20%20%20%20return%20%5B%5D%2C%20%5B%5D%0A%20%20%20%20elif%20len%28tab%29%20%3D%3D%201%3A%0A%20%20%20%20%20%20%20%20return%20tab%2C%20%5B%5D%20%0A%20%20%20%20else%3A%0A%20%20%20%20%20%20%20%20l1%2C%20l2%20%3D%20diviser%28tab%5B2%3A%5D%29%0A%20%20%20%20%20%20%20%20x%2C%20y%20%3D%20tab%5B0%3A2%5D%20%0A%20%20%20%20%20%20%20%20return%20%5Bx%5D%20%2B%20l1%2C%20%5By%5D%20%2B%20l2%0A%0Adef%20fusionner%28l1%2C%20l2%29%3A%0A%20%20%20%20%22%22%22%20Liste%2C%20Liste%20-%3E%20Liste%0A%20%20%20%20Renvoie%20la%20liste%20des%20%C3%A9l%C3%A9ments%20de%20l1%20et%20l2%20tri%C3%A9s%20%22%22%22%0A%20%20%20%20if%20len%28l1%29%20%3D%3D%200%20and%20len%28l2%29%20%3D%3D%200%3A%0A%20%20%20%20%20%20%20%20return%20%5B%5D%0A%20%20%20%20elif%20len%28l1%29%20%3D%3D%200%3A%0A%20%20%20%20%20%20%20%20return%20l2%0A%20%20%20%20elif%20len%28l2%29%20%3D%3D%200%3A%0A%20%20%20%20%20%20%20%20return%20l1%0A%20%20%20%20else%3A%0A%20%20%20%20%20%20%20%20x1%2C%20x2%20%3D%20l1%5B0%5D%2C%20l2%5B0%5D%0A%20%20%20%20%20%20%20%20if%20x1%20%3C%20x2%3A%0A%20%20%20%20%20%20%20%20%20%20%20%20reste%20%3D%20fusionner%28l1%5B1%3A%5D%2C%20l2%29%0A%20%20%20%20%20%20%20%20%20%20%20%20reste%20%3D%20%5Bx1%5D%20%2B%20reste%0A%20%20%20%20%20%20%20%20else%3A%0A%20%20%20%20%20%20%20%20%20%20%20%20reste%20%3D%20fusionner%28l1%2C%20l2%5B1%3A%5D%29%0A%20%20%20%20%20%20%20%20%20%20%20%20reste%20%3D%20%5Bx2%5D%20%2B%20reste%20%20%20%20%20%20%20%20%20%0A%20%20%20%20%20%20%20%20return%20reste%0A%0Adef%20tri_fusion%28l%29%3A%0A%20%20%20%20%22%22%22%20Liste%20-%3E%20Liste%0A%20%20%20%20Trie%20la%20liste%20l%20%28tri%20fusion%29%20%22%22%22%0A%20%20%20%20if%20len%28l%29%20%3C%3D%201%3A%0A%20%20%20%20%20%20%20%20return%20l%0A%20%20%20%20else%3A%0A%20%20%20%20%20%20%20%20l1%2C%20l2%20%3D%20diviser%28l%29%0A%20%20%20%20%20%20%20%20l1%20%3D%20tri_fusion%28l1%29%0A%20%20%20%20%20%20%20%20l2%20%3D%20tri_fusion%28l2%29%0A%20%20%20%20%20%20%20%20return%20fusionner%28l1%2C%20l2%29%0A&function_call=tri_fusion%28%5B3%2C%206%2C%201%2C%205%5D%29) de l'instruction `tri_fusion(l)` lorsque `l` est la liste $(8, 1, 7, 3, 1, 6, 4, 5)$.
</div>

