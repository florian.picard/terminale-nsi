def init_piles():
    p1 = Pile()
    p1.empiler(Chapeau(1))
    p1.empiler(Chapeau(2))
    p1.empiler(Chapeau(3))
    p1.empiler(Chapeau(4))
    p1.empiler(Chapeau(5))

    p2 = Pile()
    p2.empiler(Chapeau(2))
    p2.empiler(Chapeau(1))
    p2.empiler(Chapeau(5))
    p2.empiler(Chapeau(3))
    p2.empiler(Chapeau(4))

    p3 = Pile()
    p3.empiler(Chapeau(4))
    p3.empiler(Chapeau(5))
    p3.empiler(Chapeau(1))
    p3.empiler(Chapeau(2))
    p3.empiler(Chapeau(3))

    p4 = Pile()
    p4.empiler(Chapeau(5))
    p4.empiler(Chapeau(4))
    p4.empiler(Chapeau(3))
    p4.empiler(Chapeau(2))
    p4.empiler(Chapeau(1))
    return p1, p2, p3, p4
