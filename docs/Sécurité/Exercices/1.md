---
title: (Dé)codage, (dé)chiffrement
---


## Codage, décodage : exemple du code ASCII

<div class="fancyquotes" markdown="1">
L'American Standard Code for Information Interchange (Code américain normalisé pour l'échange d'information), plus connu sous l'acronyme ASCII, est une norme informatique de codage de caractères apparue dans les années 1960. C'est la norme de codage de caractères la plus influente à ce jour. ASCII définit 128 codes à 7 bits, comprenant 95 caractères imprimables : les chiffres arabes de 0 à 9, les 26 lettres de l'alphabet latin en minuscules et en capitales, et des symboles mathématiques et de ponctuation.
</div>

Le **code** ASCII est donc une **référence commune**. En python, il existe deux fonctions permettant de manipuler les caractères :

-   `ord` qui étant donné un caractère (une chaîne de caractères de taille 1) renvoie son code ASCII (notation décimale).
-   `chr` qui étant donné un entier renvoie le caractère de la table ASCII correspondant.

Ainsi, **tout le monde** peut encoder une chaîne de caractères suivant le **code** ASCII, et décoder une chaîne de caractères suivant le code ASCII. 


### Codage d'un texte

Écrire une fonction `code_ascii` qui étant donné une chaîne de caractères, renvoie une liste d'entiers correspondant aux codes ASCII de ces caractères.  

{{ IDE('scripts/tp/code_ascii') }}


### Décodage d'un texte

Écrire une fonction `décode_ascii` qui étant donné une liste d'entiers correspondant à des codes ASCII, renvoie la chaîne de caractères correspondante.

{{ IDE('scripts/tp/decode_ascii') }}


### Génération de l'alphabet

Écrire une fonction `genere_alphabet_majuscule` qui renvoie la liste des 26 lettres de l'alphabet latin en majuscule.

{{ IDE('scripts/tp/genere_alphabet_majuscule') }}


## Chiffrement, déchiffrement

<div class="fancyquotes" markdown="1">
Il y deux types de cryptographie dans ce monde : la cryptographie qui empêche votre petite sœur de lire vos fichiers, et la cryptographie qui empêche le gouvernement de lire vos fichiers.
</div>


### La méthode de chiffrement de César

L'une des méthodes les plus anciennes pour chiffrer un texte est attribuée à Jules César. Elle consiste à remplacer chaque lettre du message par la lettre située trois places plus loin dans l'alphabet. Par analogie, on continue d'appeler cette méthode un chiffrement de César si le décalage vaut autre chose que 3. On appelle le premier message le **clair** et celui obtenu après décalage le **chiffré**.

On considère dans cette partie que l'on ne chiffre que des lettres majuscules non accentuées, les autres caractères (ponctuation, espaces, chiffres) restent inchangés.

<div class="question" markdown="1">
1.  1.  En considérant un décalage de 3, chiffrer le texte `TG8 WINZALL`.
    2.  De combien de manières possibles peut-on chiffrer un texte avec la méthode de chiffrement de César ?
2.  De quelle(s) information(s) a-t-on besoin pour déchiffrer un message ? Déchiffrer le mot `LQIRUPDWLTXH<3`.
</div>


#### Décalage de caractère

Écrire une fonction `decale_car` qui étant donné un caractère `car` et une valeur de décalage `cle` renvoie le caractère décalé correspondant si `car` est un caractère majuscule. Si `car` n'est pas un caractère majuscule, alors la fonction renverra `car`.

Ainsi, avec une valeur de décalage de 3, `'A'` sera chiffré en `'D'`, `'B'` sera chiffré en `'E'`, etc. 

**Indication.**. Afin de déterminer la relation entre `car`, `cle` et `decale_car(car, cle)`, on pourra utiliser le tableau ci-dessus, en explicitant quelle fonction permet de passer d'une ligne à la suivante. 

![img](cesar.png)

Par exemple, on passe de la ligne 1 du tableau à la ligne 2 du tableau en appliquant `ord` au caractère `car`. On rappelle également un exemple d'utilisation de la fonction modulo `%` :

``` py linenums="1" 
for i in range(10):
    print(i%4, end = '')
```

```
0123012301
```

{{ IDE('scripts/tp/decale_car') }}


#### Chiffrement de César

Écrire une fonction `chiffre_cesar` qui étant donné un texte en `clair` et une `cle` applique la méthode de chiffrement de César au `clair` avec un décalage égal à la `cle`.

{{ IDE('scripts/tp/chiffre_cesar') }}


#### Déchiffrement de César

Écrire une fonction `dechiffe_cesar` qui étant donné un message `chiffre` avec une `cle` renvoie le texte en clair correspondant. 

{{ IDE('scripts/tp/dechiffre_cesar') }}


#### Une petite énigme

Entre deux toiles d'araignées, vous trouvez dans le grenier de votre grand-mère, agent secrète pour le compte des britanniques lors de la seconde guerre mondiale, une malle antique remplie de documents. Intrigué·e vous en parcourez du regard quelques uns. 

    LE 26 AVRIL 1942, A WASHINGTON D.C.
    
    A QUI DE DROIT,
    J'AI FAIT DES DECOUVERTES IMPORTANTES SUR LA CRYPTANALYSE DE LA MACHINE ENIGMA. J'AI UTILISE MES CONNAISSANCES ET CRYPTOGRAPHIE ANTIQUE POUR PROTEGER MES TRAVAUX DES PLUS CURIEUX, MAIS JE N'AI AUCUN DOUTE QU'UN EXPERT EN CRYPTOGRAPHIE SAURA Y ACCÉDER.
    
    ELIZABETH SMITH FRIEDMAN.
    POST-SCRIPTUM : PORTEZ CE VIEUX WISKY AU JUGE BLOND QUI FUME.

Vous finissez par tomber sur ce document, qui vous laisse perplexe. 

```
OH 28 DYULO 1942, D ZDVKLQJWRQ G.F.

D TXL GH GURLW.
PD GHFRXYHUWH SRUWH VXU OD VWUXFWXUH GH OD PDFKLQH HQLJPD. HOOH SHUPHW GH IDLUH GHV VXFFHVVLRQV GH VXEVWLWXWLRQV HW GH SHUPXWDWLRQV. M'DL DXVVL O'LPSUHVVLRQ TXH OD VWUXFWXUH GHV PHVVDJHV HFKDQJHV HVW VRXYHQW OD PHPH, FH TXH QRXV DOORQV WHQWHU G'HASORLWHU.

HOLCDEHWK VPLWK IULHGPDQ.
SRVW-VFULSWXP : SRUWHC FH YLHXA ZLVNB DX MXJH EORQG TXL IXPH. 
```

<div class="question" markdown="1">
1.  Quel(s) point(s) commun(s) pouvez-vous imaginer entre le message chiffré et le message en clair connu ?
2.  À l'aide des fonctions écrites précédemment, déchiffrer le texte chiffré. De quelle information avez-vous eu besoin ?
</div>


### La méthode de chiffrement de Vigenère

Le chiffrement de César n'est pas un chiffrement très sécurisé :

-   il n'y a que 26 clé possibles, ce qui le rend vulnérable aux [attaques par force brute](https://fr.wikipedia.org/wiki/Attaque_par_force_brute) ;
-   une fois que la clé a été choisie, chaque lettre de l'alphabet est toujours chiffrée de la même manière : on parle de chiffrement **mono-alphabetique**. Ceci le rend vulnérable à une [attaque par analyse des fréquences](https://fr.wikipedia.org/wiki/Analyse_fr%C3%A9quentielle).

Une de ses variantes est le chiffrement de Vigenère. Cette méthode consiste à exploiter l'ensemble des 26 chiffrements de César possibles, que l'on stocke dans une table appelée *carré de Vigenère* :

![img](output.png)

La clé n'est alors plus un seul entier, mais une liste d'entiers `i` avec $0 \leq i < 26$ qui indique quel alphabet de chiffrement utiliser pour chaque lettre du message clair. On dira que **la taille** de la clé est le nombre d'entiers qui la composent. Chaque caractère du texte en clair est alors chiffré à l'aide du chiffrement de César avec le morceau de la clé correspondant.

Par exemple, si le message est `'LA RELEVE ARRIVE'` et que la clé est `[19, 20, 17, 8, 13, 6]`, on chiffre le premier caractère `'L'` avec un chiffrement de César et un décalage de 19 : on obtient la lettre `'E'`. On chiffre le second caractère `'A'` avec un chiffrement de César et un décalage de 20, etc. Comme la clé est en générale plus courte que le message à chiffrer, on répète la clé autant de fois que nécessaire.

![img](vigenere_ex.png)

**Remarque.** Afin de mémoriser cette suite d'entiers, on peut lui associer un mot-clé. Par exemple, le moyen mnémotechnique pour mémoriser la clé `[19, 20, 17, 8, 13, 6]` est d'utiliser le mot `'TURING'` : `'T'` est la 19ième lettre de l'alphabet, `U` la 20ième, etc.

``` py linenums="1" 
def indice_lettre(c):
    """ str -> int
    Renvoie l'indice dans l'alphabet de la lettre capitale c. """
    return ord(c) - ord('A')

cle = [indice_lettre(c) for c in "TURING"]
print(cle)
```

```
[19, 20, 17, 8, 13, 6]
```

<div class="question" markdown="1">
Chiffrer le message `TENEZ BON !` à l'aide du chiffrement de Vigenère en utilisant la clé `NSI`.
</div>


#### Fonction de chiffrement

Écrire une fonction `chiffre_vigenere` qui étant donné un texte `clair` et une `cle` renvoie le texte chiffré que l'on obtient à l'aide de la méthode de chiffrement de Vigenère.  

**Indication.** Vous utiliserez pour cela la fonction `chiffre_cesar`. Afin de "mettre bout à bout suffisamment de fois" la clé, on pourra utiliser au choix l'une des deux informations suivantes : 

-   `[i%4 for i in range(10)]` s'évalue en `[0,1,2,3,0,1,2,3,0,1]` ;
-   `[1, 2, 3]*4` s'évalue en `[1, 2, 3, 1, 2, 3, 1, 2, 3, 1, 2, 3]`.

{{ IDE('scripts/tp/chiffre_vigenere') }}

