def test_nombre_matchs():
    """ Tests pour la fonction nombre_matchs """
    assert nombre_matchs(a, "Kamel") == 2
    assert nombre_matchs(a, "Carine") == 2
    assert nombre_matchs(a, "Joris") == 1
    assert nombre_matchs(a, "Abdou") == 1
    assert nombre_matchs(a, "Ada") == 0
    print("Tests de nombre_matchs passés avec succès.")
    return True

# from tp import nombre_matchs
test_nombre_matchs()
