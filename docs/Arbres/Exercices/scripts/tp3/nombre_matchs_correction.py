def nombre_matchs(a, nom):
    """ Arbre, str -> int
    Renvoie le nombre de matchs joués par le joueur nom dans la compétition d'arbre a """
    if etiquette(a) == nom:
        return occurrences(a, nom) - 1
    else:
        return occurrences(a, nom)
