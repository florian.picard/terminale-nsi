def dechiffre_texte_RSA(chiffre, sk):
    """ [int], (int, int) -> str
    Déchiffre le message chiffre """
    caracteres = [dechiffre_RSA(n, sk) for n in chiffre]
    return "".join([chr(n) for n in caracteres])
