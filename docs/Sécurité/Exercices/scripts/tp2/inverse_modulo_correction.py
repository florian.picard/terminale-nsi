def inverse_modulo(n, a):
    """ int, int -> int
    Renvoie (si possible) l'inverse de a modulo n """
    for b in range(n):
        if a*b % n == 1:
            return b
    return None
