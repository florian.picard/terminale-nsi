def nombre_chemins(n, m):
    """ int, int -> int
    Renvoie le nombre de chemins de (haut, gauche) à (bas, droite) dans une grille n×m """
    if n == 1 or m == 1:
        return 1
    else:
        return nombre_chemins(n - 1, m) + nombre_chemins(n, m - 1)
