def liste_sous_ensembles(E):
    """ set -> set
    Renvoie la liste de tous les sous-ensembles de E """
    if len(E) == 0:
        return [[]]
    else:
        e = E.pop()
        ssens_sans_e = liste_sous_ensembles(E)
        ssens_avec_e = []
        for sse in ssens_sans_e:
            ajoute_e = [e for e in sse] + [e]
            ssens_avec_e.append(ajoute_e)
        return ssens_sans_e + ssens_avec_e
