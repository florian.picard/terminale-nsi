def distance(G, depart, arrivee):
    """ Graphe, Sommet, Sommet -> int 
    Renvoie la longueur du plus petit chemin entre depart et arrivee dans G """
    vus = Ensemble()
    à_explorer = File()
    à_explorer.enfiler((depart, 0))
    while not à_explorer.est_vide():
        # définition du sommet courant 
        sommet, distance = à_explorer.defiler()
        if sommet in vus:
            continue
        vus.ajouter(sommet)
        # opération de traitement
        if sommet == arrivee:
            return distance
        # ajout des voisins non visités
        for destination in G.voisins(sommet):
            if not destination in vus:
                à_explorer.enfiler((destination, distance + 1))
    return -1
