import random
def test_appartient():
    # Tests pour la fonction appartient 
    assert appartient([], 0) == False
    assert appartient([0, 1, 2, 3], 0) == True
    assert appartient([0, 1, 2, 3], 3) == True
    assert appartient([0, 1, 2, 3], 4) == False
    return True

# from tp import appartient
test_appartient()
