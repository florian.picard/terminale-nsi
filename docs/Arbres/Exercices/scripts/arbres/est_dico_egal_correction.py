def est_dico_egal(d1, d2):
    """ dict, dict -> bool
    Détermine si les dictionnaires d1 et d2 sont égaux """
    for k in d1:
        if not k in d2:
            return False
    for k in d2:
        if not k in d1:
            return False
    for k in d1:
        if not d1[k] == d2[k]:
            return False
    return True
