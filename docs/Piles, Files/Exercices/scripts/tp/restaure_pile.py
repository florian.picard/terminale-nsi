def restaure_pile(p, p_aux):
    """ Pile, Pile -> Nonetype
    Restaure p à son état initial lorsque les dépilements successifs
    on été empilés dans p_aux. """
    while not p.est_vide():
        c = p.depiler()
        p_aux.empiler(c)
    while not p_aux.est_vide():
        # À compléter

def restaure_file(f, f_aux):
    """ File, File -> Nonetype
    Restaure f à son état initial lorsque les défilement successifs
    on été enfilés dans f_aux. """
    pass

def compatibles(p, f):
    """ Pile, File -> Bool
    Détermine si les chapeaux sont associés aux bons bonshommes
    L'état de la pile p et la file f sera le même avant et après exécution """
    pass
