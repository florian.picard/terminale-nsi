def compatibles(p, f):
    """ Pile, File -> Bool
    Détermine si les chapeaux sont associés aux bons bonshommes """
    while not p.est_vide() and not f.est_vide():
        c, b = p.depiler(), f.defiler()
        if not b.content(c):
            return False
    if not (p.est_vide() and f.est_vide()):
        return False
    return True
