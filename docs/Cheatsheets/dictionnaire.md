---
title: Dictionnaires et ensembles
---


## Dictionnaire, clé, valeurs

On définit un dictionnaire en utilisant des crochets. Un dictionnaire (aussi appelé un tableau associatif ou table d'association) est une structure de données associant à un ensemble de clefs, un ensemble correspondant de valeurs. Les clés d'un dictionnaire sont donc uniques. On associe à chaque clé une unique valeur : une association clé/valeur est appelée un enregistrement. Il est possible que plusieurs clés distinctes soient associées à la même valeur. 

La **taille** d'un dictionnaire est le nombre de clés qui le composent. Un dictionnaire est une structure **mutable** : on peut ajouter, modifier des éléments qui le composent.

On peut définir un dictionnaire vide à l'aide de l'instruction `dict()`

``` py linenums="1" 
dico = {'a':1, 'b':2, 'c':1}
print(len(dico))
dico2 = dict()
dico2['Zoé'] = "0684956523"
dico2['Xavier'] = "0612756534"
print(dico2)
```

```
3
{'Zoé': '0684956523', 'Xavier': '0612756534'}
```

Les objets utilisés comme clés d'un dictionnaire ne doivent pas être mutables.

``` py linenums="1" 
dico = {(1, 2):'a', (2, 1):'b'} # les clés sont des tuples (non mutables)
print(dico)
dico = {[1, 2]:'a', [2, 1]:'b'} # les clés sont des listes (mutables)
```

```
{(1, 2): 'a', (2, 1): 'b'}
```

```
---------------------------------------------------------------------------
TypeError                                 Traceback (most recent call last)
Input In [1504], in <cell line: 3>()
      1 dico = {(1, 2):'a', (2, 1):'b'} # les clés sont des tuples (non mutables)
      2 print(dico)
----> 3 dico = {[1, 2]:'a', [2, 1]:'b'}

TypeError: unhashable type: 'list'
```


### Appartenance d'un élément à l'ensemble des clés

Il est possible de tester l'appartenance d'un élément à l'ensemble des clés d'un dictionnaire **en temps constant** à l'aide du mot-clé `in`.

``` py linenums="1" 
dico = {'a':1, 'b':2, 'c':1}
print('a' in dico)
print(1 in dico)
```

```
True
False
```


### Recherche dans un dictionnaire

On accède à la valeur associé à une clé à l'aide de crochets. Si la clé n'est pas présente dans le dictionnaire, l'erreur `KeyError` est soulevée.

``` py linenums="1" 
dico = {'a':1, 'b':2, 'c':1}
print(dico['a'])
print(dico['b'])
print(dico['c'])
print(dico[1]) # 1 n'est pas une clé du dictionnaire
```

```
1
2
1
```

```
---------------------------------------------------------------------------
KeyError                                  Traceback (most recent call last)
Input In [1506], in <cell line: 5>()
      3 print(dico['b'])
      4 print(dico['c'])
----> 5 print(dico[1])

KeyError: 1
```

Il est possible de **modifier** la valeur associée à une clé d'un dictionnaire, et d'ajouter de nouveaux enregistrements.

``` py linenums="1" 
dico = {'a':1, 'b':2, 'c':1}
dico['d'] = 5
dico['a'], dico['b'] = dico['b'], dico['a']
print(dico)
```

```
{'a': 2, 'b': 1, 'c': 1, 'd': 5}
```


### Parcours d'un dictionnaire

On peut parcourir l'ensemble des clés d'un dictionnaire. Attention, **l'ordre** dans lequel les éléments sont parcourus n'est pas toujours garanti.

``` py linenums="1" 
dico = {'a':1, 'b':2, 'c':1}
for k in dico:
    print(f"La clé {k} est associée à la valeur {dico[k]}")
```

```
La clé a est associée à la valeur 1
La clé b est associée à la valeur 2
La clé c est associée à la valeur 1
```


### Méthodes du type `dict`


#### Méthode `get`

La méthode `get` renvoie la valeur associée à la clé passée en argument, `None` si la clé n'est pas présente dans le dictionnaire. Combinée avec l'opérateur de Walrus `:=`, cela permet d'écrire certaines opérations de manière concise.

``` py linenums="1" 
dico = {'Alfred':'0695746325', 'Bobby':'0632569410', 'Charlotte':'0682314685'}
def trouve_numéro(dico, nom):
    if tel := dico.get(nom): 
        return tel
    return "Numéro inconnu"

print(trouve_numéro(dico, "Alfred"))
print(trouve_numéro(dico, "Danielle"))
```

```
0695746325
Numéro inconnu
```


#### Méthode `pop`

Il est possible de supprimer un enregistrement correspondant à une clé donnée à l'aide de la méthode `pop`. Celle-ci renvoie alors la valeur associé à la clé.

``` py linenums="1" 
dico = {'a':1, 'b':2, 'c':1}    
valeur = dico.pop('b')
print(dico)
print(valeur)
```

```
{'a': 1, 'c': 1}
2
```


#### Méthode `setdefault`

La méthode `setdefault` permet de préciser une valeur par défaut pour les enregistrement. Ainsi `dico.setdefault(k, 0)` :

-   ne fait rien lorsque la clé `k` est déja présente dans le dictionnaire
-   associe la valeur `0` à la clé `k` si celle-ci n'est pas présente dans le dictionnaire.

``` py linenums="1" 
texte = "Ceci est un long texte dont on va compter toutes les lettres"
dico = dict()
for c in texte:
    dico.setdefault(c, 0)
    dico[c] += 1
print(dico)
```

```
{'C': 1, 'e': 9, 'c': 2, 'i': 1, ' ': 11, 's': 4, 't': 9, 'u': 2, 'n': 4, 'l': 3, 'o': 5, 'g': 1, 'x': 1, 'd': 1, 'v': 1, 'a': 1, 'm': 1, 'p': 1, 'r': 2}
```


#### Méthodes `keys`, `values`, `items`

Ces méthodes renvoient respectivement la liste des clés, valeurs, et enregistrements du dictionnaire. 

``` py linenums="1" 
dico = {'a':1, 'b':2, 'c':1}    
print("Parcours des clés")
for k in dico.keys():
    print(k)
print("Parcours des valeurs")
for v in dico.values():
    print(v)
print("Parcours des enregistrements")
for k, v in dico.items():
    print(k, v)
```

    Parcours des clés
    a
    b
    c
    Parcours des valeurs
    1
    2
    1
    Parcours des enregistrements
    a 1
    b 2
    c 1


## Ensemble

Un ensemble est une collection de données **sans répétition**. Ainsi, un ensemble peut-être vu comme un dictionnaire dans lequel seules les clés importent. On peut définir un ensemble à l'aide du mot-clé `set()`. Attention, `{}` définit un **dictionnaire** vide. 

``` py linenums="1" 
ens = {'a', 'b', 'c', 'd', 'a'} # 'a' _n'est pas_ répété dans ens
print(ens)
print(type(ens))
aie = {}
print(type(aie))
```

```
{'c', 'd', 'b', 'a'}
<class 'set'>
<class 'dict'>
```

Comme pour les dictionnaires, on peut tester **en temps constant** l'appartenance d'un élément à un ensemble :

``` py linenums="1" 
lettres_minuscules = {chr(code) for code in range(ord('a'), ord('z') +1)}    
print(lettres_minuscules) # l'ordre n'est pas garanti
print('a' in lettres_minuscules)
print('A' in lettres_minuscules)
```

```
{'s', 'i', 'k', 'y', 'l', 'f', 'j', 'h', 't', 'u', 'w', 'v', 'o', 'b', 'q', 'm', 'e', 'z', 'd', 'c', 'a', 'p', 'g', 'n', 'x', 'r'}
True
False
```

On peut rapidement éliminer les doublons d'une liste à l'aide des fonctions `list` et `set`. Attention cette opération ne conserve pas l'ordre des éléments.

``` py linenums="1" 
liste = [0, -1, 1, 0, -2, 0, 1, 1, 2, -2]
print(list(set(liste)))
```

```
[0, 1, 2, -2, -1]
```


### Méthodes du type `set`


#### Méthodes `add` et `update`

Il est possible d'ajouter un élément à un ensemble à l'aide de la méthode `add`, d'ajouter un ensemble d'élément à un ensemble à l'aide de la méthode `update`.

``` py linenums="1" 
ens1 = {'a', 'b', 'c'}    
ens2 = {'e', 'f', 'g'}
ens1.add('d')
print(ens1)
ens1.update(ens2)
print(ens1) # ens1 est modifié (sans répétitions)
print(ens2) # ens2 n'est pas modifié
```

```
{'c', 'd', 'b', 'a'}
{'c', 'g', 'b', 'f', 'e', 'd', 'a'}
{'e', 'g', 'f'}
```


#### Méthodes `remove` et `discard`

Il est possible de supprimer un élément d'un ensemble  à l'aide de la méthode `remove`. Si celui-ci n'est pas présent dans l'ensemble, une erreur est soulevée. 

``` py linenums="1" 
ens1 = {'a', 'b', 'c'}    
ens1.remove('a')
print(ens1)
ens1.remove('a')
print(ens1)
```

```
{'c', 'b'}
```

```
---------------------------------------------------------------------------
KeyError                                  Traceback (most recent call last)
Input In [1517], in <cell line: 4>()
      2 ens1.remove('a')
      3 print(ens1)
----> 4 ens1.remove('a')
      5 print(ens1)

KeyError: 'a'
```

La méthode `discard` effectue la même opération, mais ne soulève pas d'erreur dans le cas où l'élément n'appartient pas à l'ensemble.

``` py linenums="1" 
ens1 = {'a', 'b', 'c'}    
ens1.discard('a')
print(ens1)
ens1.discard('a')
print(ens1)
```

```
{'c', 'b'}
{'c', 'b'}
```


#### Méthodes `union` et `intersection`

La méthode `union` (resp. `intersection`) renvoie l'union (resp. intersection) des ensembles correspondants. Ceux-ci **ne sont pas modifiés**.

``` py linenums="1" 
ens1 = {'a', 'b', 'c'}    
ens2 = {'e', 'f', 'g'}
print(ens1, ens2)
ens3 = ens1.union(ens2)
print(ens1, ens2, ens3)
```

```
{'c', 'b', 'a'} {'e', 'g', 'f'}
{'c', 'b', 'a'} {'e', 'g', 'f'} {'c', 'g', 'b', 'f', 'e', 'a'}
```

