---
title: Un coffre bien gardé
---

Ce projet sans difficulté particulière se donne pour but de modéliser un coffre fort, composé de plusieurs constituants : une serrure, une alarme, et enfin le coffre-fort lui-même. On implétemente au fur et à mesure les classes qui permettent de modéliser la situation décrite par l'énoncé.


## Une serrure très très sécur'

Une serrure fonctionne avec un **code secret** qui est un nombre à quatre chiffres. Une serrure peut-être dans l'**état** verrouillée ou déverrouillée (par défaut elle est verrouillée). Afin de déverrouiller une serrure, il faut entrer le bon code. Aucune condition particulière n'est requise pour verrouiller une serrure.

1.  1.  Quelles sont les données décrivant l'état d'un objet serrure ?
    2.  Pour chacunes des deux données précédentes, préciser quel type python permettrait de les représenter au mieux.
    3.  Déduire des questions précédentes le code python d'une classe `Serrure`. On n'indiquera pour le moment que la méthode `__init__` de cette classe.

2.  Le code de la classe `Serrure` sera à compléter avec les réponses aux questions suivantes. 
    1.  Quelles sont les actions que l'on veut pouvoir effectuer sur une serrure ?
    2.  1.  Écrire une méthode `vérifier`. Celle-ci prendra en argument un `code` et vérifie qu'il s'agit bien d'un code valide (un nombre à quatre chiffres). Si c'est le cas, elle vérifiera que celui-ci est bien égal au code secret de la serrure. Elle renverra `True` si c'est le cas, `False` sinon.
        
        2.  Comment utiliser la méthode `vérifier` dans la méthode `__init__` pour vérifier que la serrure a été correctement initialisée ?
    
    3.  Écrire une méthode `verrouiller` de la classe `Serrure`. Celle-ci ne prend aucun argument, et passe la serrure dans l'état verrouillé.
    
    4.  Écrire une méthode `déverrouiller` de la classe `Serrure`. Celle-ci prend en argument un mot de passe `code`, vérifie si le code est le même que le code secret de la serrure, et déverrouille la serrure si le code est correct. Si l'opération est un succès, la méthode renverra `True`, `False` sinon.
    
    5.  Écrire une méthode `changer_code_secret` qui prend en argument un `ancien_code` et un `nouveau_code`. Elle vérifie si l'`ancien_code` est bien égal au code secret de la serrure. Si c'est le cas, le code secret est remplacé par `nouveau_code`. Elle renverra `True` si la modification a été effectuée avec succès, `False` sinon.

3.  Commenter toutes les méthodes de la classe `Serrure` (signature et docstring).
4.  Écrire un code python permettant de tester les fonctionnalités de la classe `Serrure`.

5.  Un ingénieur en cybersécurité passant par là fait remarquer que cette serrure ne semble pas si sûre que ça. Expliquer pourquoi, et proposer une solution à ce problème.


## Une alarme bruyante mais pas sans charme

Une alarme est un système qui émet un **son** lorsqu'elle est **armée** et déclenchée. Par défaut l'alarme est armée mais on peut la désarmer. Dans ce cas elle n'émettra pas de son si on la déclenche.

1.  1.  Quelles sont les données décrivant l'état d'une alarme ?
    2.  Quelles sont les actions que l'on veut pouvoir effectuer sur une alarme ?
    3.  En déduire les attributs et les méthodes que doivent posséder les objets de type `Alarme`.
2.  Écrire le code python d'une classe `Alarme` qui permet de représenter le comportement d'une alarme tel que décrit dans l'énoncé.
3.  Écrire un code python permettant de commenter et tester les méthodes de la classe `Alarme`.


## Un coffre fort pour garder tout notre or

Un coffre fort est un objet qui contient un certain **montant** d'argent. Généralement, les coffres forts sont protégés par une **serrure**, ainsi qu'une **alarme**. Un coffre fort peut-être dans un **état** ouvert ou fermé. S'il est ouvert, on peut y déposer de l'argent et en retirer librement (si cela est possible). S'il est fermé, il est impossible d'effectuer des opérations de retrait et/ou de dépôt. Il est toujours possible de fermer un coffre fort ouvert, mais pour ouvrir un coffre-fort fermé, il faut entrer le bon code sur la serrure, sans quoi l'alarme se déclenche, afin de prévenir tout vol. 

Compléter, commenter, et tester le code ci-dessous.

``` py linenums="1" 
class Coffre :
    """ Représente un coffre fort protégé par une alarme et une serrure. """
    def __init__(self,  montant, code_secret) :
        """ Initialise un coffre fort.
        code_secret est le code de la serrure.
        L'alarme fait un son prédéfini par le constructeur. """
        # à compléter

    def fermer(self) :
        """ Ferme le coffre. """
        # à compléter
        
    def ouvrir(self, code) :
        """ Ouvre le coffre. """
        # à compléter
        
    def deposer(self, montant) :
        """ Dépose le montant dans le coffre. Renvoie le montant déposé. """
        # à compléter
        
    def retirer(self, montant) :
        """ Retire le montant du coffre. Renvoie le montant retiré. """
        # à compléter
```

