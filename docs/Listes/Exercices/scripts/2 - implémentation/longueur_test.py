def message(input, output, expected):
    return f"Erreur :\n Entrée : {input}\n Sortie : {output}\n Attendu : {expected}"

def tab2maillon(tab):
    maillon = None
    for i in range(len(tab) - 1, -1, -1):
        maillon = Maillon(tab[i], maillon)
    return maillon

def égaux(m1, m2):
    if est_vide(m1) and est_vide(m2):
        return True
    elif est_vide(m1) ^ est_vide(m2):
        return False
    return tete(m1) == tete(m2) and égaux(queue(m1), queue(m2))

def test_longueur():
    """ Tests pour la fonction longueur """
    inputs = [tab2maillon([i for i in range(n)]) for n in range(10)]
    outputs_i = [longueur_i(m) for m in inputs]
    outputs_r = [longueur_r(m) for m in inputs]
    expecteds = [n for n in range(10)]
    for i in range(10):
        assert outputs_i[i] == expecteds[i], message(inputs[i], outputs[i], expecteds[i])
        assert outputs_r[i] == expecteds[i], message(inputs[i], outputs[i], expecteds[i])
    print("Tests de longueur passés avec succès.")
    return True

# from tp import longueur
test_longueur()
