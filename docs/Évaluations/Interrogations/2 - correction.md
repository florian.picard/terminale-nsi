---
title: Rappels, POO
---

<div class="exo" markdown="1">
Compléter l'encadré avec le code python répondant aux questions de l'énoncé. On rappelle que la fonction `randint` du module `random` prend en entrée deux nombres `a` et `b` et renvoie un nombre aléatoire $N$ avec $a \leq  N \leq  b$.

1.  Définir une classe `Dé` possédant les attributs `nb_faces` et `dernier_lancer`.
    
    L'attribut `dernier_lancer` aura comme valeur par défaut `0`. 
    
    ``` py linenums="1" 
    from random import randint
    class Dé:
        def __init__(self, nb_faces):
            self.nb_faces = nb_faces
            self.dernier_lancer = 0
    ```

2.  Écrire une méthode `lancer` de la classe `Dé` qui simule un lancer de dé à `nb_faces` :
    -   elle génère un nombre aléatoire compris entre 1 et `nb_faces` ;
    
    -   modifie l'attribut `dernier_lancer` correspondant ;
    
    -   renvoie la valeur générée.
        
        ``` py linenums="1" 
        def lancer(self):
            """ Dé -> int """
            N = randint(1, self.nb_faces)
            self.dernier_lancer = N
            return N
        ```

3.  Écrire une méthode `__str__` de la classe `Dé` de telle sorte que lorsque `d` est une variable de type `Dé` l'instruction `print(d)` affiche l'attribut `dernier_lancer` :
    
    ``` py linenums="1" 
    def __str__(self):
        return f"Le résultat du dé est {self.dernier_lancer}."     
    ```

1.  Écrire un code python permettant de représenter un dé à 20 faces, de simuler un lancer, puis d'afficher le résultat du lancer.
    
    ``` py linenums="1" 
    d = Dé(6)
    d.lancer()
    print(d)         
    ```
    
    ```
    Le résultat du dé est 3.
    ```

2.  Un programmeur maladroit cherche à simuler le lancer de deux dés à 6 faces. Il écrit le code suivant :
    
    ``` py linenums="1" 
    d1 = Dé(6) 
    d2 = d1
    d1.lancer() # on suppose : renvoie 4
    d2.lancer() # on suppose : renvoie 2
    print(d1.dernier_lancer, d2.dernier_lancer)
    ```
    
    Pouvez-vous expliquer son erreur à l'aide d'un schéma ?
    
    Les deux variables `d1` et `d2` font référence au même objet. 
    
    ![img](mermaid-diagram-2023-09-27-173453.png)
</div>

