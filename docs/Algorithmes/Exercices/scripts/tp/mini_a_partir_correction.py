def mini_a_partir(tab, i):
    """ [int], int -> int
    0 <= i < len(tab)
    Renvoie l'indice du plus petit élément de tab à partir de l'indice i """
    indice_mini = i
    for k in range(i, len(tab)):
        if tab[indice_mini] > tab[k]:
            indice_mini = k
    return indice_mini
