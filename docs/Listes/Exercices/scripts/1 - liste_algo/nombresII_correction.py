def nombresII_aux(n, i):
    """ int, int -> liste
    Renvoie la liste de nombres (i, i + 1, ..., n-1, n) """
    if i >= n:
        return singleton(n)
    reste = nombresII_aux(n, i + 1)
    return ajoute(reste, i)

def nombresII(n):
    """ int, int -> liste
    Renvoie la liste de nombres (1, i + 1, ..., n-1, n) """
    return nombresII_aux(n, 1)
