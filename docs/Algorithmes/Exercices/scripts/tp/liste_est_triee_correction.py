def liste_est_triee(l):
    """ Liste -> bool
    Détermine si la liste l est triée """
    if est_vide(l) or est_singleton(l):
        return True
    else:
        x1, x2 = tete(l), tete(queue(l))
        reste_trie = liste_est_triee(queue(queue(l)))
        return x1 <= x2 and reste_trie
