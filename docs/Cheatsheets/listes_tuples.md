---
title: Tableaux et tuples
---


## Tableaux redimensionnables

Le type de base permettant de représenter les tableaux en python est le type `list`. En fait, cela correspond plus précisément à la notion de **tableau redimensionnable** (on peut modifier la taille du tableau dynamiquement après la création de celui-ci). Quand on parle de "liste en python" on fait référence au type `list`.

Les objets de type `list` sont des objets **mutables**.


### Définitions par extension et indices

On définit un tableau en utilisant des crochets (on appelle ce mode de définition "par extension" : on écrit tous les éléments les uns après les autres). La **taille** d'un tableau est le nombre d'éléments qui le composent. On dit que $i \in \mathbb{N}$ est un indice **compatible avec la taille du tableau** si $0 \leq i < n$, où $n$ est la taille du tableau.

``` py linenums="1" 
tab = ["banane", "chocolat", "ananas"]
print(len(tab))

print(tab[0])
print(tab[1])
print(tab[2])
print(tab[3])
```

```
3
banane
chocolat
ananas
```

```
---------------------------------------------------------------------------
IndexError                                Traceback (most recent call last)
Input In [97], in <cell line: 7>()
      5 print(tab[1])
      6 print(tab[2])
----> 7 print(tab[3])

IndexError: list index out of range
```

Si $i\in \mathbb{N}$ est négatif et vérifie $-n \leq  i < 0$, alors il correspond à l'élément d'indice $i + n$ du tableau (c'est à dire qu'on accède aux éléments à partir de la fin).

``` py linenums="1" 
print(tab[-1])
print(tab[-2])
print(tab[-3])
print(tab[-4])
```

```
ananas
chocolat
banane
```

```
---------------------------------------------------------------------------
IndexError                                Traceback (most recent call last)
Input In [98], in <cell line: 4>()
      2 print(tab[-2])
      3 print(tab[-3])
----> 4 print(tab[-4])

IndexError: list index out of range
```

Il est possible de **modifier** une ou plusieurs cases du tableau.

``` py linenums="1" 
tab = ["banane", "chocolat", "ananas"]
tab[0] = "pomme"
tab[1], tab[2] = tab[2], tab[1]
print(tab)
```

```
['pomme', 'ananas', 'chocolat']
```


### Définition par compréhension

On peut également définir un tableau en compréhension : on décrit comment obtenir les éléments qui le composent à l'aide d'une expression (qu'il faut comprendre, donc).

``` py linenums="1" 
tab = [0 for i in range(10)] # 0 répété 10 fois
print(tab)
tab = [i**2 for i in range(10)] # liste des carrés pour i allant de 0 à 9 (inclus)
print(tab)
```

```
[0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
[0, 1, 4, 9, 16, 25, 36, 49, 64, 81]
```

Il est possible de filtrer certains résultats dans une liste en compréhension à l'aide du mot-clé `if`, **après** le mot-clé `for`.

``` py linenums="1" 
# liste des carrés pour i _pair_ allant de 0 à 9 (inclus)
tab = [i**2 for i in range(10)
       if i%2 == 0] 
print(tab)
# liste des entiers i compris entre 0 et 9 (inclus) tels que 2*i + 1 < 10
tab = [i  for i in range(10)
       if 2*i + 1 < 10] 
print(tab)
```

```
[0, 4, 16, 36, 64]
[0, 1, 2, 3, 4]
```

Il est possible de faire des cabrioles pythonesques à l'aide de l'opérateur de Walrus `:=`.

``` py linenums="1" 
tab = [c for x in range(10)
       if (c := x**2)%2 == 1] # liste des carrés _impairs_ pour i allant de 0 à 9 (inclus)
print(tab)
```

```
[1, 9, 25, 49, 81]
```


### Parcours d'un tableau


#### Parcours par indice

On peut parcourir les indices du tableau à l'aide du mot-clé `range`.

``` py linenums="1" 
tab = ["banane", "chocolat", "ananas"]
for i in range(len(tab)):
    print(i, tab[i])
```

```
0 banane
1 chocolat
2 ananas
```


#### Parcours par valeur

Lorsque les indices ne sont pas pertinents, on peut parcourir le tableau par valeur.

``` py linenums="1" 
tab = ["banane", "chocolat", "ananas"]
for e in tab:
    print(e)
```

```
banane
chocolat
ananas
```


#### Parcours mixte

Il est possible de réaliser un parcours "mixte" à l'aide du mot-clé `enumerate`. À chaque itération, `e` est l'élément d'indice `i` de `tab`.

``` py linenums="1" 
tab = ["banane", "chocolat", "ananas"]
for i, e in enumerate(tab):
    print(i, e)
```

```
0 banane
1 chocolat
2 ananas
```


### Affectation depuis un tableau

On peut affecter des variables directement depuis un tableau. Cela nécessite d'affecter autant de variables que d'éléments dans le tableau.

``` py linenums="1" 
tab = ["banane", "chocolat", "ananas"]
a, b, c = tab    
print(a, b, c)
```

```
banane chocolat ananas
```


### Méthodes du type `list`


#### Méthode `append`

La méthode `append` ajoute l'élément `x` à la fin du tableau. Elle **modifie** le tableau. Elle **renvoie** `None`.

``` py linenums="1" 
tab = ["banane", "chocolat", "ananas"]
retour = tab.append("cerise")
print(tab)    
print(retour)
```

```
['banane', 'chocolat', 'ananas', 'cerise']
None
```


#### Méthode `insert`

La méthode `insert` ajoute l'élément `x` à l'indice `i` tableau. Elle **modifie** le tableau. Elle **renvoie** `None`.

``` py linenums="1" 
tab = ["banane", "chocolat", "ananas"]
retour = tab.insert(1, "cerise")
print(tab)    
print(retour)
```

```
['banane', 'cerise', 'chocolat', 'ananas']
None
```


#### Méthode `pop`

Par défaut, la méthode `pop` supprime et renvoie le dernier élément du tableau. Si un entier optionnel lui est passé en paramètre, elle supprime et renvoie l'élément d'indice `i` du tableau. Elle **modifie** le tableau. Elle **renvoie** `None`.

``` py linenums="1" 
tab = ["banane", "chocolat", "ananas"]
dernier = tab.pop()
premier = tab.pop(0)
print(premier, dernier)
print(tab)
```

```
banane ananas
['chocolat']
```


#### Méthode `remove`

La méthode `remove` supprime la première occurrence de l'élément `x` dans le tableau. Elle **modifie** le tableau. Elle **renvoie** `None`. Elle soulève une erreur si l'élément n'est pas présent dans le tableau

``` py linenums="1" 
tab = ["banane", "chocolat", "ananas", "banane"]
retour = tab.remove("banane")
print(tab)
print(retour)
tab.remove("fraise")
```

```
['chocolat', 'ananas', 'banane']
None
```

```
---------------------------------------------------------------------------
ValueError                                Traceback (most recent call last)
Input In [110], in <cell line: 5>()
      3 print(tab)
      4 print(retour)
----> 5 tab.remove("fraise")

ValueError: list.remove(x): x not in list
```


#### Méthode `index`

La méthode `index` renvoie l'indice de la première occurrence de l'élément `x` dans le tableau.  Elle soulève une erreur si l'élément n'est pas présent dans le tableau

``` py linenums="1" 
tab = ["banane", "chocolat", "ananas", "banane"]
i = tab.index("banane")
j = tab.index("ananas")
print(i, j)
tab.index("fraise")
```

```
0 2
```

```
---------------------------------------------------------------------------
ValueError                                Traceback (most recent call last)
Input In [111], in <cell line: 5>()
      3 j = tab.index("ananas")
      4 print(i, j)
----> 5 tab.index("fraise")

ValueError: 'fraise' is not in list
```


#### Méthode `copy`

La méthode `copy` renvoie une copie indépendante du tableau `tab`. Elle est utile pour éviter le phénomène d'aliassage.

``` py linenums="1" 
tab1 = ["banane", "chocolat", "ananas"]
tab2 = tab1
tab3 = tab1.copy()
tab1[0] = "kiwi"
print(tab1)
print(tab2)
print(tab3)
```

```
['kiwi', 'chocolat', 'ananas']
['kiwi', 'chocolat', 'ananas']
['banane', 'chocolat', 'ananas']
```

**Remarque.** On peut aussi utiliser l'instruction `list` pour réaliser une copie indépendante.

``` py linenums="1" 
tab1 = ["banane", "chocolat", "ananas"]
tab2 = tab1
tab3 = list(tab1)
tab1[0] = "kiwi"
print(tab1)
print(tab2)
print(tab3)
```

```
['kiwi', 'chocolat', 'ananas']
['kiwi', 'chocolat', 'ananas']
['banane', 'chocolat', 'ananas']
```


## Tuples

Les tuples sont très similaires aux tableaux, à la différence près qu'un objet de type `tuple` est **non mutable**.

``` py linenums="1" 
liste = ("fraise", "banane", "chocolat")    
print(liste[0])
liste[1] = "mangue"
```

```
fraise
```

```
---------------------------------------------------------------------------
TypeError                                 Traceback (most recent call last)
Input In [114], in <cell line: 3>()
      1 liste = ("fraise", "banane", "chocolat")    
      2 print(liste[0])
----> 3 liste[1] = "mangue"

TypeError: 'tuple' object does not support item assignment
```

On peut convertir une liste en tuple et un tuple en liste.

``` py linenums="1" 
a = ("fraise", "banane", "chocolat")    
b = list(a)
print(b, type(b))
c = tuple(b)
print(c, type(c))
```

```
['fraise', 'banane', 'chocolat'] <class 'list'>
('fraise', 'banane', 'chocolat') <class 'tuple'>
```

