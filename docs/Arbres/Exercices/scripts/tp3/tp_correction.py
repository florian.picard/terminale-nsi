def nb_joueurs(a):
    """ Arbre -> int
    Renvoie le nombre de joueurs ayant participé à la compétition d'arbre a """
    if est_vide(a):
        return 0
    elif est_feuille(a):
        return 1
    else:
        return nb_joueurs(gauche(a)) + nb_joueurs(droit(a))

def nb_rounds(a):
    """ Arbre -> int
    Renvoie le nombre de rounds de la compétition a """
    if est_vide(a) or est_feuille(a):
        return 0
    else:
        return 1 + max(nb_rounds(gauche(a)), nb_rounds(droit(a)))

def occurrences(a, nom):
    """ Arbre, str -> int
    Renvoie le nombre d'occurrences de nom dans a """
    if est_vide(a):
        return 0
    else:
        if etiquette(a) == nom:
            return 1 + occurrences(gauche(a), nom) + occurrences(droit(a), nom)
        else:
            return occurrences(gauche(a), nom) + occurrences(droit(a), nom)

def nombre_matchs(a, nom):
    """ Arbre, str -> int
    Renvoie le nombre de matchs joués par le joueur nom dans la compétition d'arbre a """
    if etiquette(a) == nom:
        return occurrences(a, nom) - 1
    else:
        return occurrences(a, nom)

def liste_joueurs(a):
    """ Arbre -> [str]
    Renvoie la liste des joueurs ayant participé à la compétition d'arbre a """
    if est_vide(a):
        return []
    elif est_feuille(a):
        return [etiquette(a)]
    else:
        return liste_joueurs(gauche(a)) + liste_joueurs(droit(a))

def niveau(a, i):
    """ Arbre, int -> [str]
    Renvoie la liste des étiquettes des nœuds de profondeur i de l'arbre a """
    if est_vide(a):
        return []
    elif i == 0:
        return [etiquette(a)]
    else:
        return niveau(gauche(a), i - 1) + niveau(droit(a), i - 1)

def vaincus_gagnant(a):
    """ Arbre -> [str]
    Renvoie la liste des joueurs ayant joué un match contre le gagnant de la compétition d'arbre a """
    if est_feuille(a):
        return []
    elif etiquette(gauche(a)) == etiquette(a):
        return vaincus_gagnant(gauche(a)) + [etiquette(droit(a))]
    elif etiquette(droit(a)) == etiquette(a):
        return [etiquette(gauche(a))] + vaincus_gagnant(droit(a))

def parcours_largeur(a):
    """ Arbre -> [str]
    Renvoie la liste des étiquettes des nœuds de l'arbre a, telle qu'obtenue à l'aide d'un parcours en largeur """
    file_noeud = [a]
    liste_etiquettes = []
    while not file_noeud == []:
        n = file_noeud[0]
        liste_etiquettes.append(etiquette(n))
        file_noeud.pop(0)
        if not est_vide(gauche(n)):
            file_noeud.append(gauche(n))
        if not est_vide(droit(n)):
            file_noeud.append(droit(n))
    return liste_etiquettes

def recherche_noeud(a, nom):
    """ Arbre, str -> Arbre
    Renvoie le sous-arbre de a dont la racine a pour étiquette nom et est de profondeur minimale dans a """
    file_noeud = [a]
    liste_etiquettes = []
    while not file_noeud == []:
        n = file_noeud[0]
        if etiquette(n) == nom:
            return n
        file_noeud.pop(0)
        if not est_vide(gauche(n)):
            file_noeud.append(gauche(n))
        if not est_vide(droit(n)):
            file_noeud.append(droit(n))
    return None

def joueurs_vaincus(a, nom):
    """ Arbre, str -> [str]
    Renvoie la liste des joueurs vaincus par nom dans la compétition d'arbre a """
    if etiquette(a) == nom:
        return vaincus_gagnant(a)
    else:
        n = recherche_noeud(a, nom)
        return vaincus_gagnant(n)

