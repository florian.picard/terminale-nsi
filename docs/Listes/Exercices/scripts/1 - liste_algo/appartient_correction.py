def appartient(l, e):
    """ Liste, int -> bool
    Détermine si l'élément e fait partie de la liste l """
    if est_vide(l):
        return False
    else:
        return (tete(l) == e) or appartient(queue(l), e)
