---
title: Introduction à la POO
---


## Objets : attributs et création

Le libraire du coin de votre rue décide de faire appel à vous pour informatiser son système de gestion de livres : l'objectif de ce tp est de comprendre comment on peut structurer les informations se rapportant à un livre, ainsi que les opérations que l'on peut effectuer sur les livres (en calculer le prix, par exemple) dans un ensemble cohérent. 

En python, le mot-clé `class` permet de définir un nouveau type : c'est une nouvelle structure de donnée permettant de représenter, structurer et manipuler des objets. 

{{ IDE('scripts/tp/intro_livre') }}


### Attributs d'un objet

Le code ci-dessous permet d'ajouter différents **attributs** au livre : son titre (*La peste*), son auteur (Albert Camus), et son nombre total de page (59).

{{ IDE('scripts/tp/la_peste') }}

1.  Utiliser Thonny pour inspecter l'objet `livre1` : dans le menu *Affichage*, faire afficher les panneaux *Variables* et *Inspecteur d'objets* en cliquant sur ces entrées. Une fois le panneau *Inspecteur d'objets* ouvert, cliquer sur le bouton *Attributs*. Commenter.

2.  Écrire le code python permettant de représenter le livre <span class="underline">Fondation</span> (256 pages), écrit par Isaac Asimov en 1951. 
    
    ``` py linenums="1" 
    livre2 = Livre()
    # À compléter
    livre2.titre = ...
    livre2.auteur = ...
    livre2.nb_pages = ...
    ```


### Création d'un nouveau livre

Écrire une fonction `créer_livre` qui prend en argument une chaine de caractère `t`, une chaîne de caractères `a` et un entier positif `n` et qui renvoie l'objet de type `Livre` de titre `t`, d'auteur `a` et constitué de `n` pages. 

Utiliser la fonction `créer_livre` pour initialiser une variable `livre3` représentant le livre de votre choix. 

{{ IDE('scripts/tp/créer_livre') }}


### Fonction `__init__`

Dans un nouveau fichier intitullé `introduction_init.py`, copier-coller le code suivant. Ouvrir ce fichier avec Thonny, puis l'exécuter. 

{{ IDE('scripts/tp/introduction_init') }}

1.  Dans la console de Thonny, exécuter l'instruction `Livre("Les misérables", "Hugo Victor", 2135")`. Observer l'effet sur l'état de la mémoire. Commenter.

2.  À quoi peut server la fonction `__init__` placée dans le corps de la classe `Livre` ?


## Premières fonctions


### Afficher un livre

Le libraire est un peu perturbé par l'effet de l'instruction `print(livre1)` : il préfèrerait un affichage plus lisible. La fonction `affiche` prend en argument un objet de type `Livre` et réalise un affichage partiel des informations du livre. 

Modifier la fonction `affiche` afin que l'appel `affiche(livre1)` indique également le nom de l'auteur du livre dans la description affichée. Tester votre modification sur les variables de type `Livre` déjà définies. 

{{ IDE('scripts/tp/affiche') }}


### Prix d'un livre


#### Prix de base

Ce libraire un peu simplet calcule le prix de tous ses livres de la même manière : tous ses livres coûtent 12 centimes la page. Ainsi un livre constitué de 100 pages coûte 1,2€.

Écrire une fonction `prix_base` qui étant donné un livre `lvr` et un flotant `prix_page` renvoie le prix du livre en euros. 

{{ IDE('scripts/tp/prix_base') }}


#### Prix réduit

Il arrive au libraire (qui est simplet, mais pas radin) de faire occasionnellement des réductions sur le prix des livres.

Écrire une fonction `prix_reduit` qui étant donné un livre `lvr`, un prix de vente à la page `prix_page` et un pourcentage de réduction `reduc` (un entier compris entre 0 et 100) calcule le prix du livre, après une réduction de `reduc` pourcent. 

{{ IDE('scripts/tp/prix_réduit') }}


### Méthodes d'un objet

Sur ce même modèle, on peut imaginer :

-   d'autres attributs pour la classe livre. Ils permettent d'ajouter de l'information à la description de l'objet.
-   d'autres fonctions opérant sur les objets de type livre. Elles permettent de manipuler un livre.

Afin de structurer efficacement notre code, nous allons va *encapsuler* toutes ces informations **dans** la classe qui représente notre objet. 

Toutes les réponses aux questions suivantes du tp devront être écrites dans un fichier intitulé `librairie.py`.

{{ IDE('scripts/tp/librairie', MAX = "+") }}


#### Méthodes d'un objet : utilisation

Exécuter dans la console les lignes du code suivant. Commenter la syntaxe utilisée.

{{ IDE('scripts/tp/livre_methodes') }}


#### Ajout d'un attribut

Ajouter un attribut `disponible` à la classe `Livre` : il s'agit d'un entier positif ou nul qui représente combien de livres de ce type sont disponibles. L'attribut `disponible` d'un livre aura pour valeur par défaut `1`.

{{ IDE('scripts/tp/livre.disponible') }}


#### Reception d'un livre

Écrire une méthode `recevoir` de la classe `Livre` : cette méthode prend comme argument un entier nombre `n` correspondant au nombre de copies supplémentaires du livre `self`. Celles-ci seront ajoutées aux copies disponibles du livre : on modifiera pour cela l'attribut `disponible`.

**Attention.** On rappelle que le code de cette fonction devra être écrit *dans* la classe `Livre`.

``` py linenums="1" 
def recevoir(self, n):
    """ Livre, int -> None
    n nouveaux exemplaires du livre sont disponibles """
    pass
```


#### Vente d'un livre

Écrire une méthode `vendre` de la classe `Livre` simule la vente du livre `self` :

-   si aucun livre de ce type n'est disponible, la méthode renverra `0`.
-   sinon : on calcule le prix du livre (avec le prix par page et la réduction passés en arguments) et on décrémente de 1 l'attribut `disponible` de `lvr`. La méthode renverra le prix du livre.

``` py linenums="1" 
def vendre(self, prix_page, réduc):
    """ Vend le livre, renvoie l'argent généré par la vente. """
    pass
```


## Gestion d'une librairie


### Introduction et motivation

Notre libraire cherche maintenant à gérer une collection de livre. Son stock de livre ne faisant qu'augmenter, il souhaite pouvoir répondre aux clients lui demandant s'il possède ou non un livre dont on lui donne le titre. Écrire une fonction `recherche` qui étant donné une liste de livres `collection` et un `titre`, renvoie le nombre d'exemplaires disponibles du livre intitulé `titre` que possède le libraire.

{{ IDE('scripts/tp/recherche') }}


### La classe `Librairie`

Afin de structurer efficacement notre code, on décide de regrouper toutes les fonctions opérant sur une collection de livres dans une classe intitulée `Librairie`. Ainsi, à l'aide de cette classe, le libraire pourra efficacement gérer l'ensemble des livres de sa librairie. Dans le code ci-dessous, l'attribut `collection` est un objet de type `list` constitué d'objets de type `Livre`. 

{{ IDE('scripts/tp/librairie_librairie') }}

1.  Avec quelle instruction peut-on représenter une librairie vide (ne contenant aucun livre) ?

2.  Initialiser une variable `lib` de type `Librairie` représentant une librairie contenant les livres suivants :
    -   8 exemplaires du livre <span class="underline">La peste</span>, d'Albert Camus (59 pages) ;
    -   2 exemplaires du livre <span class="underline">Fondation</span>, d'Isaac Asimov (256 pages) ;
    -   15 exemplairs du livre <span class="underline">Les misérables</span>, de Victor Hugo (2135 pages) ;
    -   5 exemplaires du livre <span class="underline">Harry Potter à l'école des sorciers</span>, de J. K. Rowling (132 pages)


#### Ajouter un livre dans une librairie

1.  Écrire une méthode `ajoute` de la classe `Librairie` qui prend en argument un objet `lvr` de type `Livre` et qui l'ajoute à la librairie représentée par `self`. 
    
    ``` py linenums="1" 
       def ajoute_livre(self, lvr):
           """ Librairie, Livre -> None
           Ajoute le livre lvr à la librairie self """
           pass
    ```

2.  Écrire le code python qui permette d'ajouter à la variable `lib` définie précédemment 11 exemplaires du livre <span class="underline">Le roi Babar</span> (15 pages), écrit par Jean de Brunhoff.
    
    {{ IDE('scripts/tp/ajoute_livre') }}


#### Afficher l'ensemble des livres de la librairie

Écrire une méthode `affiche` de la classe `Librairie` qui affiche les informations de tous les livres présents dans la librairie `self`. On pourra faire appel à la méthode `affiche` de la classe `Livre`.

``` py linenums="1" 
def affiche(self):
    """ Librairie -> None
    Affiche les livres de la librairie  """
    pass
```


#### Calculer le bénéfice total

Écrire une méthode `bénéfice` de la classe `Librairie` qui renvoie le bénéfice total généré par la vente de tous les exemplaires de tous les livres de la librairie `self`. Cette fonction prendra comme argument `prix_page`, un flottant représentant le prix par page fixé par le libraire. 

``` py linenums="1" 
def bénéfice(self, prix_page):
    """ Librairie, float -> None
    Calcule le bénéfice généré par la vente de tous les livres """
    pass
```


#### À vous de jouer

1.  Ajouter un nouvel attribut de votre choix à la classe `Livre`.
    
    Écrire une méthode de la classe `Livre` qui ajoute une fonctionnalité aux objets de type `Livre` (à vous de l'inventer !).

2.  Ajouter un nouvel attribut de votre choix à la classe `Librairie`.
    
    Écrire une méthode de la classe `Librairie` qui ajoute une fonctionnalité aux objets de type `Librairie` (à vous de l'inventer !).


## Documents

-   [Introduction à la POO (sujet)](2 - Introduction POO_sujet.pdf)

-   Fichiers python :
    -   [templates](scripts/tp/tp.py)
    -   [librairie.py](scripts/tp/librairie_all.py)

