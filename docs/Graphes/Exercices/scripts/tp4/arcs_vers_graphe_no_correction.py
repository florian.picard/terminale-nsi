def arcs_vers_graphe_no(aretes):
    """ [(Sommet, Sommet)] -> Graphe
    Sommet peut être de type int ou str
    Construit le graphe non orienté dont on donne la liste des arêtes. """
    g = Graphe()
    for (source, destination) in aretes:
        g.ajouter_sommet(source)
        g.ajouter_sommet(destination)
        g.ajouter_arc(source, destination)
        g.ajouter_arc(destination, source)
    return g
