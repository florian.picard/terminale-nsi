def nombre_bits(n):
    """ int -> int
    Renvoie le nombre de bits nécessaires pour écrire n en base 2 """
    if n <= 1:
        return 1
    return nombre_bits(n // 2) + 1
