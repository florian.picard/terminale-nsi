def tri_selection(tab):
    """ [int] -> [int]
    Trie en place le tableau tab, à l'aide de l'algorithme du tri par sélection """
    n = len(tab)
    for i in range(len(tab)):
        record = tab[i]
        indice_record = i
        for j in range(i + 1, len(tab)):
            if tab[j] < record:
                record, indice_record = tab[j], j
        tab[i], tab[indice_record] = tab[indice_record], tab[i]
    return tab
