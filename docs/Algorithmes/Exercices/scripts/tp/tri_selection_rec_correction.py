def minimum(l):
    """ Liste -> int
    l est non vide
    Renvoie le plus petit élément de la liste l """
    if est_singleton(l):
        return tete(l)
    else:
        return min(tete(l), minimum(queue(l)))

def supprime(l, e):
    """ Liste, int -> Liste
    l est non vide, e appartient à l
    Renvoie la liste l où on a supprimé une occurence de e """
    if est_singleton(l):
        return creer_vide()
    else:
        if tete(l) == e:
            return queue(l)
        else:
            return ajoute(supprime(queue(l), e), tete(l))

def tri_selection_rec(l):
    """ Liste -> Liste
    Trie la liste l à l'aide de l'algorithme du tri par selection """
    if est_vide(l):
        return l
    else:
        mini = minimum(l)
        reste = supprime(l, mini)
        reste_trié = tri_selection_rec(reste)
        return ajoute(reste_trié, mini)
