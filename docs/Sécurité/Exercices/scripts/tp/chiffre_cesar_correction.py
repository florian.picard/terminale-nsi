def chiffre_cesar(clair, cle):
    """ str, int -> str
    Chiffre texte avec le chiffrement de César (clé = décalage) """
    chiffre = ""
    for c in clair:
        chiffre += decale_car(c, cle)
    return chiffre
