class Chronogramme:
    """Représente un chronogramme"""
    def __init__(self):
        """Chronogramme -> Nonetype"""
        pass

    def temps_ecoule(self):
        """ Chronogramme -> int
        Renvoie le nombre de cycles écoulés depuis l'instant initial """
        pass
    
    def enregistrer(self, p, q):
        """ int, int -> Nonetype
        Enregistre l'information : le processus p est exécuté pendant q cycles """
        pass
    
    def __repr__(self):
        """ self -> str
        Renvoie une chaine de caractère représentant le chronogramme self """
        pass
