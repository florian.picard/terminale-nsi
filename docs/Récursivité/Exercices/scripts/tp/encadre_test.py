def test_encadre():
    """ Tests pour la fonction encadre """
    maxs = [1, 3, 7, 15, 31, 63, 127, 255, 511, 1023]
    for i in range(len(maxs)):
        expected = maxs[i]
        assert encadre(i + 1) == expected, message(i + 1, encadre(i + 1), expected)
    print("Tests de encadre passés avec succès")
    return True

# from tp import encadre
# test_encadre()
