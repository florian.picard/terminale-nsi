def test_est_vide():
    """ Tests pour la fonction est_vide """
    assert est_vide(creer_vide()) == True
    assert est_vide(Maillon(1, creer_vide())) == False
    print("Tests de est_vide passés avec succès.")
    return True

# from tp import est_vide
test_est_vide()
