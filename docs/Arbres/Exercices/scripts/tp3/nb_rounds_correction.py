def nb_rounds(a):
    """ Arbre -> int
    Renvoie le nombre de rounds de la compétition a """
    if est_vide(a) or est_feuille(a):
        return 0
    else:
        return 1 + max(nb_rounds(gauche(a)), nb_rounds(droit(a)))
