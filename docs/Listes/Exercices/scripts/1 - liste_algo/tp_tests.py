def test_premier_exemple():
    """ Tests pour la fonction premier_exemple """
    print("Tests de premier_exemple passés avec succès.")
    return True

# from tp import premier_exemple
# test_premier_exemple()
def test_est_singleton():
    """ Tests pour la fonction est_singleton """
    print("Tests de est_singleton passés avec succès.")
    assert est_singleton(creer_vide()) == False
    assert est_singleton(ajoute(creer_vide(), 3)) == True
    assert est_singleton(ajoute(ajoute(creer_vide(), 3), 2)) == False
    return True

# from tp import est_singleton
# test_est_singleton()
def test_singleton():
    """ Tests pour la fonction singleton """
    print("Tests de singleton passés avec succès.")
    return True

# from tp import singleton
# test_singleton()
def test_nombres():
    """ Tests pour la fonction nombres """
    print("Tests de nombres passés avec succès.")
    return True

# from tp import nombres
# test_nombres()
def test_nombresII():
    """ Tests pour la fonction nombresII """
    print("Tests de nombresII passés avec succès.")
    return True

# from tp import nombresII
# test_nombresII()
def test_longueur():
    """ Tests pour la fonction longueur """
    print("Tests de longueur passés avec succès.")
    return True

# from tp import longueur
# test_longueur()
def test_appartient():
    """ Tests pour la fonction appartient """
    print("Tests de appartient passés avec succès.")
    return True

# from tp import appartient
# test_appartient()
def test_nombre_occurrences():
    """ Tests pour la fonction nombre_occurrences """
    print("Tests de nombre_occurrences passés avec succès.")
    return True

# from tp import nombre_occurrences
# test_nombre_occurrences()
def test_somme():
    """ Tests pour la fonction somme """
    print("Tests de somme passés avec succès.")
    return True

# from tp import somme
# test_somme()
def test_maximum():
    """ Tests pour la fonction maximum """
    print("Tests de maximum passés avec succès.")
    return True

# from tp import maximum
# test_maximum()
def test_supprime():
    """ Tests pour la fonction supprime """
    print("Tests de supprime passés avec succès.")
    return True

# from tp import supprime
# test_supprime()
def test_insere():
    """ Tests pour la fonction insere """
    print("Tests de insere passés avec succès.")
    return True

# from tp import insere
# test_insere()
def test_concatene():
    """ Tests pour la fonction concatene """
    print("Tests de concatene passés avec succès.")
    return True

# from tp import concatene
# test_concatene()
def test_divise():
    """ Tests pour la fonction divise """
    print("Tests de divise passés avec succès.")
    return True

# from tp import divise
# test_divise()
def test_liste_sous_ensembles():
    """ Tests pour la fonction liste_sous_ensembles """
    print("Tests de liste_sous_ensembles passés avec succès.")
    return True

# from tp import liste_sous_ensembles
# test_liste_sous_ensembles()
