class ABR(ArbreBinaire):
    """ Représentation d'ABR (Arbre Binaire de Recherche) """

    def inserer(self, e):
        """ ABR, int -> NoneType
        Insère l'élément e dans l'ABR self """
        pass
    
    def appartient(self, e):
        """ ABR, int -> Nonetype
        Détermine si l'élément e est une des étiquettes de l'ABR self """
        pass
    
    def minimum(self):
        """ ABR -> int
        Renvoie la plus petite étiquette de self """
        pass
