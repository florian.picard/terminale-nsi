def rendu_monnaie_rec(systeme, montant):
    """ [int], int -> int
    Renvoie le nombre de pièces minimales à utiliser pour rendre
    le montant avec les pièces de systeme """
    if montant == 0:
        return 0
    else:
        rendu = [rendu_monnaie_rec(systeme, montant - p)
                 for p in systeme
                 if montant - p >= 0]
        return min(rendu) + 1
