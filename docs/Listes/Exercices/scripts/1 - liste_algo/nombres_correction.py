def nombres(n):
    """ int -> Liste
    Renvoie la liste (n, n-1, n-2, ..., 3, 2, 1) """
    if n <= 1:
        return singleton(1)
    else:
        apres = nombres(n - 1)
        return ajoute(apres, n)
