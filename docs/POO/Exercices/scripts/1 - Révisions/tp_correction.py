def est_pair(bits):
    """ [int] -> bool
    len(bits) > 0
    Détermine si le nombre dont l'écriture en base 2 est donnée par le tableau bits est pair.  """
    return bits[-1] == 0

def est_plus_grand_kbits(bits):
    """ [int] -> bool
    k = len(bits) > 0
    Détermine si le tableau bits correspond au plus  grand entier que l'on  puisse écrire sur k bits.  """
    # bits correspond au plus grand entier si il n'est constitué que de 1 :
    # On parcourt tout le tableau :
    # - si jamais un des éléments est 0 on s'arrête et on renvoie False.
    # - si on a parcouru tout le tableau sans s'arrêter on renvoie True.
    for b in bits:
        if b == 0:
            return False
    return True

def appartient(tab, e):
    """ [int], int -> bool
    Détermine si l'élément e est présent dans le tableau tab.  """
    for elem in tab:
        if elem == e:
            return True
    return False

def nombre_occurrences(tab, e):
    """ [int], int -> int
    Détermine le nombre d'éléments de tab qui sont égaux à e.  """
    compte = 0
    for elem in tab:
        if elem == e:
            compte += 1
    return compte

def indices_occurrences(tab, e):
    """ [int], int -> [int]
    Détermine les indices des occurrences de e dans le tableau tab. """
    indices = [0]*nombre_occurrences(tab, e)
    nb_occurrences = 0
    for i in range(len(tab)):
        if tab[i] == e:
            indices[nb_occurrences] = i
            nb_occurrences += 1
    return indices

def indices_occurrences(tab, e):
    """ [int], int -> [int]
    Détermine les indices des occurrences de e dans le tableau tab. """
    # Avec la méthode append
    indices = []
    for i in range(len(tab)):
        if tab[i] == e:
            indices.append(i)
    return indices

def maximum(tab):
    """ [int], int -> int, int
    len(tab) > 0
    Détermine le maximum des éléments de tab, ainsi que le premier indice pour lequel ce maximum est atteint.  """
    record = tab[0]
    indice = 0
    for i in range(len(tab)):
        if tab[i] > record:
            record = tab[i]
            indice = i
    return record, indice
