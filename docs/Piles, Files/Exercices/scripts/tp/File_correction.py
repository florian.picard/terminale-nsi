class File:
    """Une file d'entiers"""
    def __init__(self):
        """File -> Nonetype"""
        self.debut = None
        self.fin = None

    def est_vide(self):
        """ File -> bool
        Détermine si la file self est vide """
        return self.debut is None and self.fin is None
    
    def enfiler(self, v):
        """ File, int -> Nonetype
        Ajoute l'élément v à la file self """
        m = Maillon(v, None)
        if self.est_vide():
            self.debut = m
        else:
            self.fin.suivant = m
        self.fin = m
    
    def defiler(self):
        """ File -> int
        Renvoie le premier élément de la file en le supprimant de celle-ci """
        if self.est_vide():
            raise IndexError("Impossible de défiler la file vide")
        val = self.debut.valeur
        self.debut = self.debut.suivant
        if self.debut is None:
            self.fin = None
        return val
    
    def __str__(self):
        """ self -> str
        Construit la chaîne de caractères représentant la file self """
        s = "[Début]"
        mc = self.debut
        while mc is not None:
            s += f" {mc.valeur}"
            mc = mc.suivant
        s += " [Fin]"
        return s
