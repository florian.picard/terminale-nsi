def element_r(m, i):
    """ Maillon, int -> int
    m est non vide, 0 <= i < longueur(m)
    Renvoie l'élément d'indice i de la chaîne de maillons dont le premier est m """
    # version récursive
    if i == 0:
        return tete(m)
    else:
        return element_r(queue(m), i - 1)

def element_i(m, i):
    """ Maillon, int -> int
    m est non vide, 0 <= i < longueur(m)
    Renvoie l'élément d'indice i de la chaîne de maillons dont le premier est m """
    # version récursive
    courant = m
    count = 0
    while count < i:
        count = count + 1
        courant = queue(courant)
    return tete(courant)
