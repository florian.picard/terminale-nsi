def restaure_pile(p, p_aux):
    """ Pile, Pile -> Nonetype
    Restaure p à son état initial lorsque les dépilements successifs
    on été empilés dans p_aux. """
    while not p.est_vide():
        c = p.depiler()
        p_aux.empiler(c)
    while not p_aux.est_vide():
        c = p_aux.depiler()
        p.empiler(c)

def restaure_file(f, f_aux):
    """ File, File -> Nonetype
    Restaure f à son état initial lorsque les défilement successifs
    on été enfilés dans f_aux. """
    while not f.est_vide():
        b = f.defiler()
        f_aux.enfiler(b)
    while not f_aux.est_vide():
        b = f_aux.defiler()
        f.enfiler(b)

def compatibles(p, f):
    """ Pile, File -> Bool
    Détermine si les chapeaux sont associés aux bons bonshommes """
    p_aux = Pile()
    f_aux = File()
    while not p.est_vide() and not f.est_vide():
        c, b = p.depiler(), f.defiler()
        p_aux.empiler(c)
        f_aux.enfiler(b)
        if not b.content(c):
            restaure_pile(p, p_aux)
            restaure_file(f, f_aux)
            return False
    if not (p.est_vide() and f.est_vide()):
        restaure_pile(p, p_aux)
        restaure_file(f, f_aux)
        return False
    restaure_pile(p, p_aux)
    restaure_file(f, f_aux)
    return True
