def affiche_rec(m):
    """ Maillon -> None
    Affiche les valeurs de la chaîne dont le premier maillon est m. """
    if est_vide(m):
        print("x")
    else:
        print(tete(m), end = " - ")
        affiche_rec(queue(m))

affiche_rec(m3)

def affiche_i(m):
    """ Affiche les valeurs de la chaîne dont le premier maillon est m """
    maillon_courant = m
    while not est_vide(maillon_courant):
        print(tete(maillon_courant), end=' - ')
        maillon_courant = queue(maillon_courant)
    print("x")

affiche_i(m3)
affiche = affiche_i
