def arcs_vers_graphe_no(aretes):
    """ [(Sommet, Sommet)] -> Graphe
    Sommet peut être de type int ou str
    Construit le graphe non orienté dont on donne la liste des arêtes. """
    g = Graphe()
    for (source, destination) in aretes:
        g.ajouter_sommet(source)
        g.ajouter_sommet(destination)
        g.ajouter_arc(source, destination)
        g.ajouter_arc(destination, source)
    return g

def parcours_aleatoire(G, depart, nombre_sauts):
    """ Graphe, Sommet, int -> {Sommet: int}
    Parcourt aléatoirement le graphe G depuis le sommet depart
    en effectuant nombre_sauts choix de sommets. """
    visites = {s:0 for s in G.sommets()}
    sommet_courant = depart
    for _ in range(nombre_sauts):
        visites[sommet_courant] += 1
        destinations_possibles = G.voisins(sommet_courant)
        randi = randint(0, len(destinations_possibles) - 1)
        sommet_courant = destinations_possibles[randi]
    return visites

