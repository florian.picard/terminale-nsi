def parcours_largeur(a):
    """ Arbre -> [str]
    Renvoie la liste des étiquettes des nœuds de l'arbre a, telle qu'obtenue à l'aide d'un parcours en largeur """
    file_noeud = [a]
    liste_etiquettes = []
    while not file_noeud == []:
        n = file_noeud[0]
        liste_etiquettes.append(etiquette(n))
        file_noeud.pop(0)
        if not est_vide(gauche(n)):
            file_noeud.append(gauche(n))
        if not est_vide(droit(n)):
            file_noeud.append(droit(n))
    return liste_etiquettes
