def tab_est_trie(tab):
    """ [int] -> bool
    Détermine si le tableau tab est trié par ordre croissant. """
    for i in range(len(tab) - 1):
        if tab[i + 1] < tab[i]:
            return False
    return True

def liste_est_triee(l):
    """ Liste -> bool
    Détermine si la liste l est triée """
    if est_vide(l) or est_singleton(l):
        return True
    else:
        x1, x2 = tete(l), tete(queue(l))
        reste_trie = liste_est_triee(queue(queue(l)))
        return x1 <= x2 and reste_trie

def tri_insertion_iter(tab):
    """ [int] -> [int]
    Trie en place le tableau tab (tri par insertion) """
    n = len(tab)
    for i in range(n):
        # les éléments d'indice 0..i du tableau t
        # sont triés dans l'ordre croissant
        element = tab[i]
        trou = i
        # on cherche à insérer l'élément dans le tableau
        # constitué des éléments d'indice 0..i
        while tab[trou - 1] > element and trou > 0:
            tab[trou] = tab[trou - 1]
            trou = trou - 1
        # À la fin de la boucle soit
        # trou = 0 et tous les éléments de t sont supérieurs à élément
        # soit trou > 0, t[trou - 1] < element < t[i] pour tout i >= trou
        tab[trou] = element
        # le tableau est alors trié
    return tab # que se passe-t-il si on ne le met pas ?

def insere_trie(l, e):
    """ Liste, int -> Liste
    l est triée par ordre croissant
    Insère l'élément e dans la liste l. La liste résultante est triée"""
    if est_vide(l):
        return ajoute(l, e)
    else:
        if e <= tete(l):
            return ajoute(l, e)
        else:
            return ajoute(insere_trie(queue(l), e), tete(l))

def trie_insertion_rec(l):
    """ Liste -> Liste
    Trie la liste l à l'aide de l'algorithme du tri par insertion """
    if est_vide(l):
        return l
    else:
        premier = tete(l)
        reste_trié = trie_insertion_rec(queue(l))
        return insere_trie(reste_trié, premier)

def mini_a_partir(tab, i):
    """ [int], int -> int
    0 <= i < len(tab)
    Renvoie l'indice du plus petit élément de tab à partir de l'indice i """
    indice_mini = i
    for k in range(i, len(tab)):
        if tab[indice_mini] > tab[k]:
            indice_mini = k
    return indice_mini

def tri_selection_iter(tab):
    """ [int] -> [int]
    Trie le tableau tab (tri par sélection) """
    if tab == []:
        return []
    for i in range(len(tab)):
        indice_mini = mini_a_partir(tab, i)
        tab[i], tab[indice_mini] = tab[indice_mini], tab[i]
    return tab

def minimum(l):
    """ Liste -> int
    l est non vide
    Renvoie le plus petit élément de la liste l """
    if est_singleton(l):
        return tete(l)
    else:
        return min(tete(l), minimum(queue(l)))

def supprime(l, e):
    """ Liste, int -> Liste
    l est non vide, e appartient à l
    Renvoie la liste l où on a supprimé une occurence de e """
    if est_singleton(l):
        return creer_vide()
    else:
        if tete(l) == e:
            return queue(l)
        else:
            return ajoute(supprime(queue(l), e), tete(l))

def tri_selection_rec(l):
    """ Liste -> Liste
    Trie la liste l à l'aide de l'algorithme du tri par selection """
    if est_vide(l):
        return l
    else:
        mini = minimum(l)
        reste = supprime(l, mini)
        reste_trié = tri_selection_rec(reste)
        return ajoute(reste_trié, mini)

def diviser(l):
    """ Liste -> Liste, Liste
    Divise la liste en deux listes """
    if est_vide(l):
        return creer_vide(), creer_vide()
    elif est_singleton(l):
        return l, creer_vide()
    else:
        l1, l2 = diviser(queue(queue(l)))
        return ajoute(l1, tete(l)), ajoute(l2, tete(queue(l)))

def fusionner(l1, l2):
    """ Liste, Liste
    l1 et l2 sont triées par ordre croissant
    Renvoie la liste des éléments de l1 et l2 triés """
    if est_vide(l1):
        return l2
    elif est_vide(l2):
        return l1
    else:
        if tete(l1) < tete(l2):
            reste = fusionner(queue(l1), l2)
            return ajoute(reste, tete(l1))
        else:
            reste = fusionner(l1, queue(l2))
            return ajoute(reste, tete(l2))

def tri_fusion(l):
    """ Liste -> Liste
    Trie la liste l (tri fusion) """
    if est_vide(l):
        return l
    elif est_singleton(l):
        return l
    else:
        l1, l2 = diviser(l)
        l1_triée, l2_triée = tri_fusion(l1), tri_fusion(l2)
        return fusionner(l1_triée, l2_triée)

