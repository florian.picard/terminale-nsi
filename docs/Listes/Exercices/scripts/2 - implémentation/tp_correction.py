m1 = None

m2 = Maillon(-1, None)

m3_0 = Maillon(-1, None)
m3_1 = Maillon(9, m3_0)
m3_2 = Maillon(6, m3_1)
m3_2 = Maillon(5, m3_2)
m3 = m3_2

m4 = Maillon(-5, Maillon(9, Maillon(13, None)))

def creer_vide():
    """ () -> Maillon
    Renvoie un maillon vide """
    return None

def est_vide(m):
    """ Maillon -> bool
    Renvoie True si et seulement si le maillon m est le maillon vide """
    return m is None

def tete(m):
    """ Maillon -> int
    m est non vide
    Renvoie l'attribut valeur du maillon m """
    return m.valeur

def queue(m):
    """ Maillon -> Maillon
    m est non vide
    Renvoie le maillon suivant dans la chaine de maillon de tête m. """
    return m.suivant

def ajoute_debut(m, e):
    """ Maillon, int -> Maillon
    Renvoie le maillon dont l'attribut valeur est e et l'attribut suivant est m """
    return Maillon(e, m)

def longueur_r(m):
    """ Maillon -> int
    Compte le nombre de maillons présents dans la chaîne dont le premier maillon est m """
    # version récursive
    if est_vide(m):
        return 0
    else:
        return 1 + longueur_r(queue(m))

def longueur_i(m):
    """ Maillon -> int
    Détermine le nombre de maillons présents dans la chaine après le maillon m (inclu). """
    # version itérative
    courant = m
    count = 0
    while not est_vide(courant):
        count = count + 1
        courant = queue(courant)
    return count

longueur = longueur_i

def element_r(m, i):
    """ Maillon, int -> int
    m est non vide, 0 <= i < longueur(m)
    Renvoie l'élément d'indice i de la chaîne de maillons dont le premier est m """
    # version récursive
    if i == 0:
        return tete(m)
    else:
        return element_r(queue(m), i - 1)

def element_i(m, i):
    """ Maillon, int -> int
    m est non vide, 0 <= i < longueur(m)
    Renvoie l'élément d'indice i de la chaîne de maillons dont le premier est m """
    # version récursive
    courant = m
    count = 0
    while count < i:
        count = count + 1
        courant = queue(courant)
    return tete(courant)

def remplace_r2(m, i, e):
    """ Maillon, int, int -> Maillon
    m est non vide, 0 <= i < longueur(m)
    Renvoie le premier maillon de la chaine où la valeur d'indice i a été remplacée par e. """
    # version récursive, non mutable
    if i == 0:
        return Maillon(e, queue(m))
    else:
        return Maillon(tete(m), remplace_r(queue(m), i - 1, e))

def remplace_r(m, i, e):
    """ Maillon, int, int -> Maillon
    m est non vide, 0 <= i < longueur(m)
    Renvoie le premier maillon de la chaine où la valeur d'indice i a été remplacée par e. """
    # version récursive, mutable
    if i == 0:
        m.valeur = e
    else:
        remplace_r2(queue(m), i - 1, e)
    return m

def remplace_i(m, i, e):
    """ Maillon, int, int -> Maillon
    m est non vide, 0 <= i < longueur(m)
    Renvoie le premier maillon de la chaine où la valeur d'indice i a été remplacée par e. """
    # version impérative, mutable
    courant = m
    count = 0
    while count < i:
        count = count + 1
        courant = queue(courant)
    courant.valeur = e
    return m

def remplace_i2(m, i, e):
    """ Maillon, int, int -> Maillon
    m est non vide, 0 <= i < longueur(m)
    Renvoie le premier maillon de la chaine où la valeur d'indice i a été remplacée par e. """
    # version impérative, non mutable
    courant = m
    count = 0
    pile = []
    while count < i:
        count = count + 1
        pile.append(tete(courant))
        courant = queue(courant)
    nouveau = Maillon(e, queue(courant))
    while pile:
        e = pile.pop()
        nouveau = Maillon(e, nouveau)
    return nouveau

def ajoute_position(m, i, e):
    """ Maillon, int, int -> NoneType
    0 <= i <= longueur(m)
    Ajoute l'élément e en i-ième position de la chaine dont le premier maillon est m """
    courant = m
    count = 0
    while not est_vide(courant.suivant) and count < i - 1:
        count = count + 1
        courant = courant.suivant
    # on insère tout en bout de chaine
    if est_vide(courant.suivant) and count == i - 1:
        fin = Maillon(e, None)
        courant.suivant = fin
    # on insère en milieu de chaine
    else:
        milieu = Maillon(courant.valeur, courant.suivant)
        courant.valeur = e
        courant.suivant = milieu

def supprime(m, i):
    """ Maillon, int -> NoneType
    0 <= i < longueur(m)
    Supprime la valeur i de la chaine dont le premier maillon est m """
    # la liste est un singleton
    if est_vide(m.suivant):
        return None
    # la liste n'est pas un singleton, mais on supprime l'élément de tête :
    # on décale toutes les valeurs de la chaîne et on supprime le dernier
    # maillon. 
    if i == 0:
        courant = m
        while not est_vide(courant.suivant.suivant):
            courant.valeur = courant.suivant.valeur
            courant = courant.suivant
        courant.suivant = None
        return
    # cas général
    # on parcourt toute la liste, et on s'arrête au maillon juste
    # avant celui à supprimer : deux cas sont possibles :
    # soit on supprime le dernier soit un maillon interne.
    courant = m
    count = 0
    while not est_vide(courant.suivant) and count < i - 1:
        count = count + 1
        courant = courant.suivant
    # le prochain maillon à supprimer est courant.suivant
    # on supprime le dernier maillon
    if est_vide(courant.suivant):
        courant.suivant = None
    # on supprime un maillon interne de la liste chaînée
    else:
        courant.suivant = courant.suivant.suivant

