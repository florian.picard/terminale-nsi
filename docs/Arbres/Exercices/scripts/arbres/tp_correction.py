def est_feuille(a):
    """ Arbre -> bool
    Détermine si a est constitué d'un seul élément """
    return est_vide(gauche(a)) and est_vide(droit(a))

def taille(a):
    """ Arbre -> int
    Renvoie le nombre de nœuds de a """
    if est_vide(a):
        return 0
    return 1 + taille(gauche(a)) + taille(droit(a))

def somme(a):
    """ Arbre -> int
    Renvoie la somme des éléments de l'arbre a """
    if est_feuille(a):
        return etiquette(a)
    elif est_vide(gauche(a)):
        return etiquette(a) + somme(droit(a))
    elif est_vide(droit(a)):
        return etiquette(a) + somme(gauche(a))
    else:
        return etiquette(a) + somme(gauche(a)) + somme(droit(a))

def hauteur(a):
    """ Arbre -> int
    Renvoie la hauteur de l'arbre """
    if est_feuille(a):
        return 1
    elif est_vide(gauche(a)):
        return 1 + hauteur(droit(a)) 
    elif est_vide(droit(a)):
        return 1 + hauteur(gauche(a))
    else:
        return 1 + max(hauteur(gauche(a)), hauteur(droit(a)))

def affiche_infixe(a):
    """ Arbre -> Nonetype
    Affiche l'arbre a de manière infixe """
    if est_vide(a):
        pass
    else:
        print("(", end = "")
        affiche_infixe(gauche(a))
        print(f"{etiquette(a)}", end = "")
        affiche_infixe(droit(a))
        print(")", end = "")

def rechercher(a, e):
    """ Arbre, int -> bool
    Renvoie True si et seulement si e est une des étiquettes de a """
    if est_vide(a):
        return False
    return etiquette(a) == e or rechercher(gauche(a), e) or rechercher(droit(a), e)

def maximum(a):
    """ Arbre -> int
    Renvoie la plus grande étiquette de a """
    if est_feuille(a):
        return etiquette(a)
    elif est_vide(gauche(a)):
        return max(etiquette(a), maximum(droit(a)))
    elif est_vide(droit(a)):
        return max(etiquette(a), maximum(gauche(a)))
    else:
        return max(etiquette(a), maximum(gauche(a)), maximum(droit(a)))

def est_egal(a1, a2):
    """ Arbre, Arbre -> bool
    Détermine si les arbres a1 et a2 sont identiques """
    if est_vide(a1) and est_vide(a2):
        return True
    elif est_vide(a1) ^ est_vide(a2):
        return False
    return etiquette(a1) == etiquette(a2) and est_egal(gauche(a1), gauche(a2)) and est_egal(droit(a1), droit(a2))

def est_egalf(a1, a2):
    """ Arbre, Arbre -> bool
    Renvoie True si et seulement si les arbres a1 et a2 sont faiblement égaux """
    if est_vide(a1) and est_vide(a2):
        return True
    elif est_vide(a1) ^ est_vide(a2):
        return False
    else:
        return etiquette(a1) == etiquette(a2) and ((est_egalf(gauche(a1), gauche(a2)) and est_egalf(droit(a1), droit(a2))) or (est_egalf(gauche(a1), droit(a2)) and est_egalf(droit(a1), gauche(a2))))

def contenu(a, d):
    """ Arbre, dict -> Nonetype
    Ajoute à d le contenu de a """
    if est_vide(a):
        pass
    else:
        contenu(gauche(a), d)
        if not etiquette(a) in d:
            d[etiquette(a)] = 1
        else:
            d[etiquette(a)] += 1
        contenu(droit(a), d)

def est_dico_egal(d1, d2):
    """ dict, dict -> bool
    Détermine si les dictionnaires d1 et d2 sont égaux """
    for k in d1:
        if not k in d2:
            return False
    for k in d2:
        if not k in d1:
            return False
    for k in d1:
        if not d1[k] == d2[k]:
            return False
    return True

def est_egalc(a1, a2):
    """ Arbre, Arbre -> bool
    Renvoie True ssi les arbres a1 et a2 ont le même contenu """
    d1 = dict()
    d2 = dict()
    contenu(a1, d1)
    contenu(a2, d2)
    return est_dico_egal(d1, d2)

