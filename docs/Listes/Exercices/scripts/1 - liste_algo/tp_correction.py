l1 = creer_vide()
l2 = creer_vide()
l2 = ajoute(l2, -1)
l3 = creer_vide()
l3 = ajoute(l3, -1)
l3 = ajoute(l3, 9)
l3 = ajoute(l3, 6)
l3 = ajoute(l3, 5)
l4 = creer_vide()
l4 = ajoute(l4, 9)
l4 = ajoute(l4, -5)
l4 = ajoute(l4, 9)
l4 = ajoute(l4, 4)
l4 = ajoute(l4, 9)
l4 = ajoute(l4, -5)

def est_singleton(l):
    """ Liste -> bool
    Détermine si la liste est constituée d'un seul élément. """
    if est_vide(l):
        return False
    return est_vide(queue(l))

def singleton(n):
    """ int -> Liste
    Renvoie la liste (n) """
    l = creer_vide()
    return ajoute(l, n)

def nombres(n):
    """ int -> Liste
    Renvoie la liste (n, n-1, n-2, ..., 3, 2, 1) """
    if n <= 1:
        return singleton(1)
    else:
        apres = nombres(n - 1)
        return ajoute(apres, n)

def nombresII_aux(n, i):
    """ int, int -> liste
    Renvoie la liste de nombres (i, i + 1, ..., n-1, n) """
    if i >= n:
        return singleton(n)
    reste = nombresII_aux(n, i + 1)
    return ajoute(reste, i)

def nombresII(n):
    """ int, int -> liste
    Renvoie la liste de nombres (1, i + 1, ..., n-1, n) """
    return nombresII_aux(n, 1)

def longueur(l):
    """ Liste -> int
    Renvoie la longueur de la liste l """
    if est_vide(l):
        return 0
    else:
        return 1 + longueur(queue(l))

def appartient(l, e):
    """ Liste, int -> bool
    Détermine si l'élément e fait partie de la liste l """
    if est_vide(l):
        return False
    else:
        return (tete(l) == e) or appartient(queue(l), e)

def nombre_occurrences(l, e):
    """ Liste, int -> int
    Compte le nombre d'occurrences de e dans l """
    if est_vide(l):
        return 0
    else:
        if e == tete(l):
            return 1 + nombre_occurrences(queue(l), e)
        else:
            return nombre_occurrences(queue(l), e)

def somme(l):
    """ Liste -> int
    Calcule la somme des éléments de la liste l """
    if est_singleton(l):
        return tete(l)
    else:
        return tete(l) + somme(queue(l))

def maximum2(a, b):
    """ int, int -> int
    Calcule l'élément maximum parmis a et b
    """
    if a < b :
        return b
    else:
        return a
    
def maximum(l):
    """ liste -> int
    Calcule l'élément maximum parmis les éléments de l """
    if est_singleton(l):
        return tete(l)
    else:
        return maximum2(tete(l), maximum(queue(l)))

def supprime(l, e):
    """ Liste, int -> Liste
    Supprime la première occurrence de e de la liste l """
    if est_vide(l):
        return creer_vide()
    else:
        if tete(l) == e:
            return queue(l)
        else:
            queue_sans_e = supprime(queue(l), e)
            return ajoute(queue_sans_e, tete(l))

def insere(l, e, i):
    """ Liste, int, int -> Liste
    Insère l'élément e dans la liste l à l'indice i """
    if i == 0:
        return ajoute(l, e)
    else:
        e_dans_queue = insere(queue(l), e, i - 1)
        return ajoute(e_dans_queue, tete(l))

def concatene(l1, l2):
    """ Liste, Liste -> Liste
    Concatène les deux listes """
    if est_vide(l1):
        return l2
    else:
        queue_et_l2 = concatene(queue(l1), l2)
        return ajoute(queue_et_l2, tete(l1))

def premier(l):
    return tete(l)

def second(l):
    return tete(queue(l))

def est_2ton(l):
    if est_vide(l) or est_singleton(l):
        return False
    return est_vide(queue(queue(l)))

def divise(l):
    """ liste -> liste, liste
    Divise en deux listes la liste l """
    if est_vide(l):
        return creer_vide(), creer_vide()
    elif est_singleton(l):
        return l, creer_vide()
    elif est_2ton(l):
        return singleton(premier(l)), singleton(second(l))
    else:
        l1, l2 = divise(queue(queue(l)))
        return ajoute(l1, premier(l)), ajoute(l2, second(l))

def liste_sous_ensembles(E):
    """ set -> set
    Renvoie la liste de tous les sous-ensembles de E """
    if len(E) == 0:
        return [[]]
    else:
        e = E.pop()
        ssens_sans_e = liste_sous_ensembles(E)
        ssens_avec_e = []
        for sse in ssens_sans_e:
            ajoute_e = [e for e in sse] + [e]
            ssens_avec_e.append(ajoute_e)
        return ssens_sans_e + ssens_avec_e

