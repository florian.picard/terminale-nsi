def singleton(n):
    """ int -> Liste
    Renvoie la liste (n) """
    l = creer_vide()
    return ajoute(l, n)
