def est_egal(a1, a2):
    """ Arbre, Arbre -> bool
    Détermine si les arbres a1 et a2 sont identiques """
    if est_vide(a1) and est_vide(a2):
        return True
    elif est_vide(a1) ^ est_vide(a2):
        return False
    return etiquette(a1) == etiquette(a2) and est_egal(gauche(a1), gauche(a2)) and est_egal(droit(a1), droit(a2))
