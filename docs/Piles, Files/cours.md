---
title: Piles, files
---

Les piles et les files sont deux structures de données qui permettent de stocker séquentiellement des valeurs. On distingue deux modes de stockage :

-   LIFO (ou FILO) : Last In First Out (ou First In Last Out). Il s'agit des piles : le dernier élément à être ajouté à la pile sera le premier élément à en être supprimé.
-   FIFO : First In First Out. Il s'agit des files : le premier élément à être ajouté à la file sera le premier élément à en être supprimé.

Dans les deux cas, la structure de donnée doit supporter les opérations suivantes :

-   créer une structure de donnée initiallement vide
-   tester si la structure de donnée est vide
-   ajouter un élément à une structure
-   retirer et obtenir un élément de la structure.

<div class="exemple" markdown="1">
Dans chacun des cas ci-dessous, dire si la situation correspond à un mode de stockage LIFO ou FIFO.

-   des assiettes rangées dans un placard :
    
    il s'agit du mode LIFO (la dernière assiette rangée sera la première assiette sortie)
-   des élèves faisant la queue à la cantine : 
    
    il s'agit du mode FIFO (le premier élève arrivé sera le premier élève servi)
</div>


## Pile

On stocke les éléments selon le mode FILO. On ajoute et retire les éléments au **sommet** de la pile. Les éléments sont retirés d'une pile dans l'ordre **inverse** de leur ordre d'ajout.

![img](pile.png)

On ne considère que le cas où les éléments sont des entiers.
L'interface du type `Pile` est la suivante :

<table border="2" cellspacing="0" cellpadding="6" rules="groups" frame="hsides">


<colgroup>
<col  class="org-left" />

<col  class="org-left" />

<col  class="org-left" />
</colgroup>
<thead>
<tr>
<th scope="col" class="org-left">Notation fonction</th>
<th scope="col" class="org-left">Notation objet</th>
<th scope="col" class="org-left">Description</th>
</tr>
</thead>

<tbody>
<tr>
<td class="org-left"><code>creer_pile_vide() -&gt; Pile</code></td>
<td class="org-left"><code>Pile() -&gt; Pile</code></td>
<td class="org-left">Créer une pile vide</td>
</tr>


<tr>
<td class="org-left"><code>est_pile_vide(p:Pile) -&gt; bool</code></td>
<td class="org-left"><code>p.est_vide() -&gt; bool</code></td>
<td class="org-left">Détermine si la pile <code>p</code> est vide.</td>
</tr>


<tr>
<td class="org-left"><code>empiler(p:Pile, c:int) -&gt; None</code></td>
<td class="org-left"><code>p.empiler(c:int) -&gt; None</code></td>
<td class="org-left">Ajoute l'entier <code>c</code> au sommet de la pile <code>p</code></td>
</tr>


<tr>
<td class="org-left"><code>depiler(p:Pile) -&gt; int</code></td>
<td class="org-left"><code>p.depiler() -&gt; int</code></td>
<td class="org-left">Renvoie l'entier au sommet de la pile <code>p</code>, lorsque cela est possible.</td>
</tr>


<tr>
<td class="org-left"><code>affiche_pile(p:Pile) -&gt; None</code></td>
<td class="org-left"><code>p.affiche() -&gt; None</code></td>
<td class="org-left">Affiche la pile <code>p</code> sans la modifier.</td>
</tr>
</tbody>
</table>

Les opérations d'empilement et de dépilement **modifient** la pile à laquelle elles s'appliquent. On parle **d'effet de bord**. Avec cette interface, une pile est un objet **mutable**.

<div class="exemple" markdown="1">
1.  La pile `p` est constituée des éléments `1`, `2` et `3` (`3` est au sommet de la pile. Donner le contenu de `p2` après l'exécution du code suivant :
    
    ``` py linenums="1" 
    p2 = Pile()
    while not p.est_vide():
        p2.empiler(p.depiler())
    ```
    
    `p2` est la pile `3`, `2`, `1` (`1` est le sommet)

2.  Quel est l'affichage alors réalisé par le code
    
    ``` py linenums="1" 
    while not p2.est_vide():
        print(p2.depiler())   
    ```
    
    Cela affiche `1`, `2`, `3` (dans cet ordre).
</div>


## File

On stocke les éléments selon le mode FIFO. On ajoute en **fin** de file et on retire les éléments au **début** de la file. Les éléments sont retirés d'une file **dans le même ordre** que leur ordre d'ajout.

![img](repr_pile.png)

On ne considère que le cas où les éléments sont des entiers.
L'interface du type `File` est la suivante :

<table border="2" cellspacing="0" cellpadding="6" rules="groups" frame="hsides">


<colgroup>
<col  class="org-left" />

<col  class="org-left" />

<col  class="org-left" />
</colgroup>
<thead>
<tr>
<th scope="col" class="org-left">Notation fonction</th>
<th scope="col" class="org-left">Notation objet</th>
<th scope="col" class="org-left">Description</th>
</tr>
</thead>

<tbody>
<tr>
<td class="org-left"><code>creer_file_vide() -&gt; File</code></td>
<td class="org-left"><code>File() -&gt; File</code></td>
<td class="org-left">Créer une file vide</td>
</tr>


<tr>
<td class="org-left"><code>est_file_vide(f:File) -&gt; bool</code></td>
<td class="org-left"><code>f.est_vide() -&gt; bool</code></td>
<td class="org-left">Détermine si la file <code>f</code> est vide.</td>
</tr>


<tr>
<td class="org-left"><code>enfiler(f:File, c:int) -&gt; None</code></td>
<td class="org-left"><code>f.enfiler(c:int) -&gt; None</code></td>
<td class="org-left">Ajoute l'entier <code>c</code> à la fin de la file <code>f</code></td>
</tr>


<tr>
<td class="org-left"><code>defiler(f:File) -&gt; int</code></td>
<td class="org-left"><code>f.defiler() -&gt; int</code></td>
<td class="org-left">Renvoie l'entier au début de la file <code>f</code>, lorsque cela est possible.</td>
</tr>


<tr>
<td class="org-left"><code>affiche_file(f:File) -&gt; None</code></td>
<td class="org-left"><code>f.affiche() -&gt; None</code></td>
<td class="org-left">Affiche la file <code>f</code> sans la modifier.</td>
</tr>
</tbody>
</table>

Les opérations d'enfilement et de défilement **modifient** la file à laquelle elles s'appliquent. On parle **d'effet de bord**. Avec cette interface, une file est un objet **mutable**.

<div class="exemple" markdown="1">
1.  La file `f` est constituée des éléments `1`, `2` et `3` (`3` est à la fin de la file. Donner le contenu de `f2` après l'exécution du code suivant :
    
    ``` py linenums="1" 
    f2 = File()
    while not f.est_vide():
        f2.enfiler(f.defiler())
    ```
    
    `f2` est la file `1`, `2`, `3` (`3` est la fin de la file)

2.  Quel est l'affichage alors réalisé par le code
    
    ``` py linenums="1" 
    while not f2.est_vide():
        print(f2.defiler())   
    ```
    
    Cela affiche `1`, `2`, `3` (dans cet ordre).
</div>


## Implémentation

On implémente l'interface du type `Pile` et du type `File` à l'aide d'objets de type `Maillon`.

Voir code correspondant écrit en TP.

