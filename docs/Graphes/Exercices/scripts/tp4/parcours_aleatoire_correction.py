def parcours_aleatoire(G, depart, nombre_sauts):
    """ Graphe, Sommet, int -> {Sommet: int}
    Parcourt aléatoirement le graphe G depuis le sommet depart
    en effectuant nombre_sauts choix de sommets. """
    visites = {s:0 for s in G.sommets()}
    sommet_courant = depart
    for _ in range(nombre_sauts):
        visites[sommet_courant] += 1
        destinations_possibles = G.voisins(sommet_courant)
        randi = randint(0, len(destinations_possibles) - 1)
        sommet_courant = destinations_possibles[randi]
    return visites
