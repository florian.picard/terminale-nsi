---
title: Bases de données relationnelles
---

## Questions de cours

Deux relations modélisent la flotte de voitures d'un réseau de location de voitures.

-   Agence
    
    <table border="2" cellspacing="0" cellpadding="6" rules="groups" frame="hsides">
    
    
    <colgroup>
    <col  class="org-right" />
    
    <col  class="org-left" />
    
    <col  class="org-right" />
    </colgroup>
    <thead>
    <tr>
    <th scope="col" class="org-right">ida</th>
    <th scope="col" class="org-left">ville</th>
    <th scope="col" class="org-right">département</th>
    </tr>
    </thead>
    
    <tbody>
    <tr>
    <td class="org-right">1</td>
    <td class="org-left">Paris</td>
    <td class="org-right">75</td>
    </tr>
    
    
    <tr>
    <td class="org-right">2</td>
    <td class="org-left">Lyon</td>
    <td class="org-right">69</td>
    </tr>
    
    
    <tr>
    <td class="org-right">3</td>
    <td class="org-left">Marseille</td>
    <td class="org-right">13</td>
    </tr>
    
    
    <tr>
    <td class="org-right">4</td>
    <td class="org-left">Aubagne</td>
    <td class="org-right">13</td>
    </tr>
    </tbody>
    </table>

-   Voiture
    
    <table border="2" cellspacing="0" cellpadding="6" rules="groups" frame="hsides">
    
    
    <colgroup>
    <col  class="org-right" />
    
    <col  class="org-left" />
    
    <col  class="org-left" />
    
    <col  class="org-right" />
    
    <col  class="org-left" />
    
    <col  class="org-right" />
    </colgroup>
    <thead>
    <tr>
    <th scope="col" class="org-right">idv</th>
    <th scope="col" class="org-left">marque</th>
    <th scope="col" class="org-left">modèle</th>
    <th scope="col" class="org-right">km</th>
    <th scope="col" class="org-left">couleur</th>
    <th scope="col" class="org-right">ida</th>
    </tr>
    </thead>
    
    <tbody>
    <tr>
    <td class="org-right">1</td>
    <td class="org-left">Renault</td>
    <td class="org-left">Clio</td>
    <td class="org-right">12000</td>
    <td class="org-left">Rouge</td>
    <td class="org-right">2</td>
    </tr>
    
    
    <tr>
    <td class="org-right">2</td>
    <td class="org-left">Peugeot</td>
    <td class="org-left">205</td>
    <td class="org-right">22000</td>
    <td class="org-left">Noir</td>
    <td class="org-right">3</td>
    </tr>
    
    
    <tr>
    <td class="org-right">3</td>
    <td class="org-left">Toyota</td>
    <td class="org-left">Yaris</td>
    <td class="org-right">33000</td>
    <td class="org-left">Noir</td>
    <td class="org-right">3</td>
    </tr>
    
    
    <tr>
    <td class="org-right">4</td>
    <td class="org-left">Toyota</td>
    <td class="org-left">Yaris</td>
    <td class="org-right">65000</td>
    <td class="org-left">Verte</td>
    <td class="org-right">1</td>
    </tr>
    </tbody>
    </table>

1.  Combien la relation Voiture comporte-t-elle d'attributs ?
2.  Que vaut son cardinal ?
3.  Quel est le domaine de l'attribut ida dans la relation Voiture ?
4.  Quelle est la clé primaire de la relation Agence ?
5.  Quelle est la clé primaire de la relation Voiture ?
6.  Quelle est la clé étrangère de la relation Voiture ?
7.  Quel est le schéma relationnel de la relation Agence ?
8.  Quel est le schéma relationnel de la relation Voiture ?

## Modélisation redondante

Dans la table "Médailles" ci-dessous, on suppose que des sports différents ne peuvent pas avoir le même nom.

<table border="2" cellspacing="0" cellpadding="6" rules="groups" frame="hsides">


<colgroup>
<col  class="org-right" />

<col  class="org-left" />

<col  class="org-left" />

<col  class="org-left" />

<col  class="org-left" />

<col  class="org-left" />
</colgroup>
<thead>
<tr>
<th scope="col" class="org-right">Année</th>
<th scope="col" class="org-left">Sport</th>
<th scope="col" class="org-left">Épreuve</th>
<th scope="col" class="org-left">Sexe</th>
<th scope="col" class="org-left">Sportif</th>
<th scope="col" class="org-left">Médaille</th>
</tr>
</thead>

<tbody>
<tr>
<td class="org-right">2020</td>
<td class="org-left">Natation</td>
<td class="org-left">50m libre</td>
<td class="org-left">H</td>
<td class="org-left">C. Dressel</td>
<td class="org-left">Or</td>
</tr>


<tr>
<td class="org-right">2020</td>
<td class="org-left">Natation</td>
<td class="org-left">50m libre</td>
<td class="org-left">H</td>
<td class="org-left">F. Manaudou</td>
<td class="org-left">Argent</td>
</tr>


<tr>
<td class="org-right">2020</td>
<td class="org-left">Natation</td>
<td class="org-left">100m dos</td>
<td class="org-left">F</td>
<td class="org-left">K. Mckeown</td>
<td class="org-left">Or</td>
</tr>


<tr>
<td class="org-right">2020</td>
<td class="org-left">Natation</td>
<td class="org-left">100m dos</td>
<td class="org-left">F</td>
<td class="org-left">K. Masse</td>
<td class="org-left">Argent</td>
</tr>


<tr>
<td class="org-right">2016</td>
<td class="org-left">Natation</td>
<td class="org-left">100m dos</td>
<td class="org-left">F</td>
<td class="org-left">K. Masse</td>
<td class="org-left">Bronze</td>
</tr>


<tr>
<td class="org-right">2016</td>
<td class="org-left">Natation</td>
<td class="org-left">100m dos</td>
<td class="org-left">F</td>
<td class="org-left">F. Yanhui</td>
<td class="org-left">Bronze</td>
</tr>


<tr>
<td class="org-right">&#x2026;</td>
<td class="org-left">&#x2026;</td>
<td class="org-left">&#x2026;</td>
<td class="org-left">&#x2026;</td>
<td class="org-left">&#x2026;</td>
<td class="org-left">&#x2026;</td>
</tr>
</tbody>
</table>

1.  Quels attributs peut-on choisir comme clé primaire ? Justifier votre réponse.
    -   Ce n'est pas possible.
    -   Année
    -   (Année, Sportif, Épreuve)
    -   (Année, Épreuve, Médaille)
2.  Quel est le résultat de l'ordre `UPDATE Médailles SET Sportif = 'F. Manaudou' WHERE Année = 2020 AND Médaille = 'Or'` ?
3.  1.  Quelles sont les informations qui apparaissent plusieurs fois dans cette table ?
    2.  On souhaite éviter les informations redondantes. On décompose pour cela la relation `Médaille` en trois relations : `Sport`, `Sportif` et `Médaille`.
        1.  Donner les attributs de ces relations (on ajoutera un attribut `id` si nécessaire) .
        2.  Pour chacune des relations, expliquer quel attribut joue le rôle de clé primaire, quels attributs jouent le rôle de clé étrangère (s'il y en a).
        3.  En déduire le schéma relationnel complet de cette base de données.

## Tour de france

On donne ci-dessous les relations constituant la base de données Tour de France 2020.

![img](course.png)

En utilisant les données de l'énoncé, répondre aux questions suivantes :

1.  Donner le schéma relationnel des relations Équipe et Étape.

2.  1.  Quels sont les attributs de la relation Coureur ?
    2.  Quel attribut peut jouer le rôle de clé primaire pour cette relation ?
    3.  Quelle contrainte utilisateur est-il raisonnable d'imposer à l'attribut "dossard" ?
    4.  En déduire le schéma relationnel de la relation Coureur.
3.  1.  Expliquer pourquoi l'attribut dossard ne peut pas être choisi comme clé primaire de la relation Temps.
    2.  Y a-t-il des clés étrangères dans la relation Temps ? Si oui, préciser lesquelles.
    3.  En déduire le schéma relationnel de la relation Temps. On choisira comme clé primaire le coupe (dossard, numéroEtape).
4.  En déduire une représentation sous forme de diagramme synthétisant la composition des différentes tables ainsi que les relations entre elles.
5.  1.  Quel est le nom de l'équipe à laquelle appartient Nicolas EDET ?
    2.  Quel temps a réalisé Guillaume MARTIN sur l'étape Sisteron / Orcières-Merlette ?
    3.  Quelle est la vitesse moyenne de Julian ALAPHILIPPE lors de l'étape Gap-Privas ?
    4.  À l'arrivée à Privas, qui est arrivé en premier entre Primož ROGLIČ et Simone CONSONNI ?
    5.  Quels sont les numéros de dossard des coureurs de l'équipe "Cofidis, Solutions Crédit" ?
6.  1.  Écrire un ordre SQL permettant d'ajouter la coureuse Anaïs Baya à l'équipe Astana Pro Team.
    
    2.  Expliquer pourquoi l'ordre SQL ci-dessous soulève une erreur.
        
        ``` sql linenums="1" 
        DELETE FROM Coureur
        WHERE dossard = 41 ;
        ```
    
    3.  Expliquer comment faire pour supprimer ALAPHILIPPE Julian de la relation Coureur.

## Type bac

Un site permet à ses membres de proposer à la location du matériel et de louer du
matériel. Ceci permet de mutualiser du matériel entre membres et au propriétaire de
rentabiliser cet achat. Le temps d'utilisation du matériel s'en trouve ainsi augmenté et le nombre d'appareils diminué.

Le modèle relationnel est donné par le schéma ci-dessous.
La table `Membre` contient les informations de chaque utilisateur du site (nom, prénom et
code postal). La table `Objet` décrit le type d'objet à la location ainsi que son tarif de
location journalier.

La table `Reservation` répertorie toutes les réservations effectuées par les membres du
site avec notamment leur date de début et de fin de location. La table `Possede` permet
de lier les tables `Membre` et `Objet`.

![img](objets.png)

Conventions utilisées pour le schéma :

-   Les clés primaires et étrangères sont mises en gras ;
-   un symbole de clé identifie une clé primaire ;
-   une ligne entre deux attributs indique qu’ils doivent partager
    les mêmes valeurs et qu’ils sont reliés de la manière suivante : le côté avec un trait indique la clé primaire et le côté avec une pate d'oie indique la clé étrangère.

On donne ci-dessous le contenu de ces tables à un instant donné.

![img](tables_objets.png)

1.  1.  Donner les noms et prénoms du (des) personne(s) dont le code postal est 69003.
    2.  Donner les noms et prénoms du (des) personne(s) qui proposent à la location un appareil à raclette.
    3.  Donner le prénom et le nom du membre qui ne propose pas d’objet à la location.
2.  1.  Écrire une requête SQL permettant d'ajouter sans erreur Wendie Renard habitant à
        Villeurbanne (code postal 69100) dans la table `Membre`.
    2.  Écrire une requête permettant de modifier le tarif de location d’un nettoyeur à
        haute pression pour le passer à 15 € par jour au lieu de 20 € par jour.
    3.  Écrire la ou les requêtes permettant à Wendie Renard de proposer à la location son enceinte au tarif de 5 euros la journée.
3.  1.  Expliquer la limitation importante d’utilisation du service offert par le site si l'on utilisait le couple de clés étrangères (id<sub>objet</sub>, id<sub>membre</sub>) en tant que clé
        primaire de la relation `Reservation`.
    
    2.  Expliquer quelle erreur survient lorsque l'on exécute l'ordre 
        
        ``` sql linenums="1" 
        INSERT INTO Membre VALUES (1, "Luther King", "Martin");	
        ```
    
    3.  L'ordre suivant produit-il un erreur ? Justifier.
        
        ``` sql linenums="1" 
        INSERT INTO Membre VALUES (10, "Ali", "Mohamed");	
        ```
    
    4.  Mohamed Ali décide de ne plus être membre du site. Il faut donc le supprimer
        de la table `Membre` à l'aide de la requête :
        
        ``` sql linenums="1" 
        DELETE FROM Membre
        WHERE nom = "Ali" AND prenom ="Mohamed";
        ```
        
        Expliquer pourquoi cette requête produit une erreur.
    5.  Proposer une suite de requêtes utilisant le mot clé `DELETE`, précédant la requête
        ci-dessus pour supprimer correctement Mohamed Ali de la base de données.

