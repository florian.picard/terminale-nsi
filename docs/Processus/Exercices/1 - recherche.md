---
title: Activité de recherche
---


## Architecture matérielle

**Point de programme.** Identifier les principaux composants sur un schéma de circuit et les
avantages de leur intégration en terme de vitesse et de consommation électrique. Exemple du
circuit de téléphone.


### Historique

1.  De quand date le premier ordinateur moderne ? De quel composant était-il principalement composé ? Quelles étaient ses dimensions ?
    
    <div class="reponse" markdown="1">
    Les **Colossus** sont une série "d'ordinateurs" construits dans l'optique de réaliser la cryptanalyse du code de Lorenz (une méthode de chiffrement utilisée lors de la seconde guerre mondiale pour chiffrer des communications stratégiques). Il ne s'agit pas à proprement parler d'ordinateurs, mais plutôt de calculateurs dédiés à des opérations de cryptanalyse.
    
    L'**ENIAC** peut être considéré comme le premier ordinateur moderne, car Turing-complet. Il fut utilisé à des fins de calculs balistiques, et géré par une équipe constituée exclusivement de femmes mathématiciennes. 

2.  Que veut dire le terme "Turing-complet" ?
    
    <div class="reponse" markdown="1">
    Une entité (machine, programme) est dite Turing-complète lorsqu'elle permet la programmation et le calcul effectif de toute fonction calculable à l'aide d'un algorithme.
    </div>

3.  Quelle invention marqua le début de la miniaturisation des composants des ordinateurs ?
    
    <div class="reponse" markdown="1">
    Le transistor marqua le début de la miniaturisation des ordinateurs (les tubes à vides étaient parfois utilisés par la même fonctionnalité, mais ils étaient gros et fragiles).
    </div>

4.  Qu’est-ce qu’un circuit intégré ? Citer un circuit intégré présent sur tous les ordinateurs modernes. À partir de quand les ordinateurs sont-ils devenus "grand public" ?
    
    <div class="reponse" markdown="1">
    Un circuit intégré est la réunion sur un seul composant de plusieurs circuits électroniques. L'intérêt est le gain de place. En 1971, Intel réalise le premier microprocesseur : un circuit intégré comportant tous les composants d'un processeur (Unité Arithmétique et Logique (UAL), registres, Unité de Contrôle (UC), &#x2026;).
    </div>

5.  Qu’est-ce que la loi de Moore ? Cette loi est-elle toujours valide ? Citer une autre loi similaire, toujours valide actuellememt.
    
    <div class="reponse" markdown="1">
    La loi de Moore énonce que le nombre de transistors présents sur une puce double tous les deux ans. Elle est actuellement remise en question, car nous avons atteint les limites physiques de la miniaturisation : les plus petits transistors sont constituées de quelques atomes !
    
    La loi de Koomey postule quant à elle que le nombre de calcul effectué par unité d'énergie dépensée double tous les 18 mois environ. Ainsi, indépendemment du matériel utilisé, plus de calculs sont effectués pour la même dépense énergétique, ce qui est un gain en efficacité. 
    </div>


### Transistors

1.  Qu’est-ce qu’un transistor ? À quoi cela sert-il ?
    
    <div class="reponse" markdown="1">
    Un transistor est un composant électronique qui joue le rôle d'amplificateur et d'interrupteur logique dans les circuits électroniques.
    </div>

2.  Comment les circuits électriques permettent-ils de réaliser des opérations logiques, comme le NOT ou le NAND ?
    
    <div class="reponse" markdown="1">
    En associant à une tension positive la valeur $1$ et à une tension nulle la valeur $0$, on peut réaliser des opérations logiques à l'aide de circuits électroniques.
    </div>

3.  Se rendre sur <https://nandgame.com/>. Réaliser les portes logiques NAND et INVERT.
    
    <div class="reponse" markdown="1">
    **Si vous avez le temps.** Réaliser toutes les portes logiques sur le site Nandgame. Vous pouvez également faire la partie "Arithmetics" (plus dure) : cela explique comment réaliser des opérations arthmétiques uniquement avec des portes logiques.
    
    Idée cadeau pour Noël : le jeu [Turing Complete](https://store.steampowered.com/app/1444480/Turing_Complete/?l=french) (présent sur Steam) vous apprend l'architecture d'un processeur sous forme de jeu avec des énigmes. 
    </div>


### Composants électroniques.

1.  1.  Qu’est-ce qu’un microprocesseur ? À quoi cela sert-il ? De quoi est-il composé ?
        
        <div class="reponse" markdown="1">
        Un microprocesseur est une puce électronique (= circuit intégré) dédié aux calcul : elle réalise des opérations arithmétiques, des opérations logiques, peut intéragir avec la mémoire, et exécuter des instructions. 
        
        Il est habituellement composé de plusieurs unités : l'UAL (unité arithmétique et logique), l'UC (unité de contrôle), de registres (permet de stocker de la donnée sur laquelle le microprocesseur doit opérer).
        </div>
    
    2.  Citer un autre composant essentiel au fonctionnement d’un ordinateur ?
        
        <div class="reponse" markdown="1">
        La mémoire est l'autre composant essentiel au fonctionnement d'un ordinateur : mémoire de stockage (disque dur&#x2026;), mémoire vive (RAM&#x2026;). 
        </div>
    
    3.  Donner un nom de microprocesseur.
        
        <div class="reponse" markdown="1">
        Le microprocesseur Intel 404 est le premier microprocesseur, fabriqué en 1971. 
        </div>

2.  1.  Qu’est-ce qu’un microcontrôleur ? De quoi est-il composé ?
        
        <div class="reponse" markdown="1">
        Un microcontrôleur est une puce électronique composée d'un processeur, de différents types de mémoire, et d'interface d'entrée-sortie. 
        </div>
    
    2.  Dans quel contexte utilise-t-on ce genre de puce électronique ?
        
        <div class="reponse" markdown="1">
        Ils sont la plupart du temps utilisé dans des systèmes embarqués (contrôleurs de moteurs, télécommandes, appareil photo, manettes de jeu etc.) pour des tâche spécialisées. 
        </div>
    
    3.  Donner un nom de microcontrôleur.
        
        <div class="reponse" markdown="1">
        ATMega328 est le microcontrôleur utilisé sur les cartes Arduino. 
        </div>

3.  1.  Qu’est-ce qu’un System On Chip (SoC) ? De quoi est-il composé ?
        
        <div class="reponse" markdown="1">
        Un SoC rassemble sur un seul composant tous les composants qui constituent habituellement un ordinateur : CPU, RAM, ROM, unités de calcul spécialisées (cryptographie, calcul matriciel pour les algorithmes d'intelligence artificielle&#x2026;), puce GPS, capteurs, circuits radio (wifi, bluetooth&#x2026;).
        </div>
    
    2.  Dans quel contexte utilise-t-on ce genre de puce électronique ?
        
        <div class="reponse" markdown="1">
        Ils sont utilisés dans le cas des systèmes embarqués, et surtout dans les smartphones et tablettes. 
        </div>
    
    3.  Quels sont les avantages des SoC sur les microprocesseurs et les microcontrôleurs ?
        
        <div class="reponse" markdown="1">
        **Énergie**, **coût**, **sécurité**.
        </div>
    
    4.  Quels sont les inconvénients des SoC par rapport aux microprocesseurs et aux microcontrôleurs ?
        
        <div class="reponse" markdown="1">
        **Pas de modularité.**
        </div>
    
    5.  Donner un nom de SoC.
        
        <div class="reponse" markdown="1">
        Google Tensor, Snapdragon, etc.
        </div>

4.  1.  Qu’est-ce que l’architecture de Von Neumann ? L’architecture de Harvard ? Quelle est la différence principale entre ces deux architectures ?
        
        <div class="reponse" markdown="1">
        Dans l'architecture de Von Neumann, il n'y a qu'un type de mémoire qui sert à la fois pour les programmes et pour la donnée.
        
        Dans l'architecture de Harvard, il y a deux types de mémoire : une mémoire pour les programmes, un mémoire pour les données.
        </div>
    
    2.  Quelle est l’architecture la plus utilisée pour les microprocesseurs ? Pour les microcontrôleurs ? Pour les SoC ?
        
        <div class="reponse" markdown="1">
        Les microprocesseurs et les SoC sont le plus souvent construits suivant l'architecture de Von Neumann, tandis que les microcontrôleurs sont plutôt construits suivant l'architecture de Harvard. 
        </div>


## Processus

**Point de programme.** Décrire la création d’un processus, l’ordonnancement de plusieurs
processus par le système d’exploitation. Mettre en évidence le risque d’interblocage. Observer les
processus actifs ou en attente sur une machine.


### Système d’exploitation

1.  Qu’est-ce qu’un système d’exploitation ? Quels rôles remplit-il ? Détailler le plus possible.
2.  1.  Qu’est-ce qu’un système d’exploitation multitâche ?
    2.  Citer un système d’exploitation monotâche "célèbre".
    3.  Citer un système d’exploitation multitâche célèbre.
3.  1.  Qu’est-ce que les droits d’accès à un fichier ?
    2.  Quelle commande permet d’afficher les droits d’un fichier sur un sytème Unix ?
    3.  Quelle commande permet de modifier les droits d’un fichier sur un sytème Unix ?


### Processus

1.  Qu’est-ce qu’un processus ? Quelle est la différence avec un programme ?
2.  Expliquer comment le système d’exploitation permet l’exécution "simultanée" de plusieurs
    processus ?
3.  Citer le nom de la commande qui permet de lister les processus actifs sur une session Windows.
    Sur une session Unix.
4.  Lister les processus actuellement actif sur votre session.
5.  Qu’est-ce que le PID d’un processus ? Son PPID ?


### Ordonnancement

1.  Qu’est-ce qu’un ordonnanceur ? Quel est son rôle ?
2.  Dans quels états peut se trouver un processus ? Décrire leur rôle.
3.  Qu’est-ce que la commutation de contexte ?
4.  Qu’est-ce qu’un quantum ?


### Interblocage

1.  Qu’est-ce qu’une ressource préemptible ? Une ressource non préemptible ?
2.  Qu’est-ce qu’un interblocage ?
3.  Donner une situation de la vie quotidienne dans laquelle on peut dire qu’il y a interblocage.
    Expliquer.

