#--- HDR ---#
class Bonhomme:
    def __init__(self, t):
        self.taille = t

    def __repr__(self):
        return f"Bonh. {self.taille}"

    def affiche(self):
        print(self)

    def content(self, c):
        return c.taille == self.taille

class File:
    def __init__(self, max_elt = float('inf')):
        self.contenu = []
        self.max_elt = max_elt

    def __repr__(self):
        return "(Début file) [" + ", ".join(b.__repr__() for b in self.contenu) + "]"

    def __len__(self):
        return len(self.contenu)

    def affiche(self):
        print(self)

    def est_vide(self):
        return self.contenu == []

    def est_pleine(self):
        return len(self) == self.max_elt 

    def enfiler(self, b):
        # assert isinstance(b, Bonhomme)
        if self.est_pleine():
            raise IndexError("Capacité maximale de la file atteinte")
        self.contenu.append(b)

    def defiler(self):
        if self.est_vide():
            raise IndexError("Impossible de défiler : la file est vide.")
        return self.contenu.pop(0)
#--- HDR ---#
# Exécuter ce bloc AVANT de traiter les exercices.
# Charge les classes Bonhomme et File.

c = Bonhomme(1)
f = File()
f.enfiler(c)
f.affiche()
print(f.defiler())
