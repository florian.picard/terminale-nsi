def ajoute_position(m, i, e):
    """ Maillon, int, int -> NoneType
    0 <= i <= longueur(m)
    Ajoute l'élément e en i-ième position de la chaine dont le premier maillon est m """
    courant = m
    count = 0
    while not est_vide(courant.suivant) and count < i - 1:
        count = count + 1
        courant = courant.suivant
    # on insère tout en bout de chaine
    if est_vide(courant.suivant) and count == i - 1:
        fin = Maillon(e, None)
        courant.suivant = fin
    # on insère en milieu de chaine
    else:
        milieu = Maillon(courant.valeur, courant.suivant)
        courant.valeur = e
        courant.suivant = milieu
