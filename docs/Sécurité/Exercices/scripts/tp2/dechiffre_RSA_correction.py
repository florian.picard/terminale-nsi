def dechiffre_RSA(C, sk):
    """ int, (int, int)
    Déchiffre le message chiffré C à l'aide de la clé secrète """
    n, d = sk
    return (C ** d) % n
