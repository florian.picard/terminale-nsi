def test_tete():
    """ Tests pour la fonction tete """
    assert tete(Maillon(1, None)) == 1
    assert tete(Maillon(2, Maillon(1, None))) == 2
    print("Tests de tete passés avec succès.")
    return True

# from tp import tete
test_tete()
