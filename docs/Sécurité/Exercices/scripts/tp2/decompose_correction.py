def decompose(n):
    """ int -> {int:int}
    Décompose n en produit de facteurs premiers """
    divs = diviseurs_premiers(n)
    decomp = dict()
    for p in divs:
        e = 1
        while n%p**e == 0:
            e += 1
        decomp[p] = e - 1   
    return decomp
