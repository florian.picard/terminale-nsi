---
title: Rappels, POO (correction)
---

<div class="exo" markdown="1">
Compléter l'encadré avec le code python répondant aux questions de l'énoncé.

1.  Définir une classe `Animal` vide (sans méthode `__init__`).
    
    ``` py linenums="1" 
    class Animal:
        pass
    ```

2.  Écrire une fonction `créer_animal` qui prenne en entrée trois chaînes de caractères `e`, `n` et `c` et qui renvoie l'objet de type `Animal` dont les attributs `espèce`, `nom` et `cri` ont été initialisés avec les valeurs respectives `e`, `n`, et `c`.
    
    ``` py linenums="1" 
    def créer_animal(e, n, c):
        a = Animal()
        a.espèce = e
        a.nom = n
        a.cri = c
        return a
    ```

3.  Utiliser cette fonction pour initialiser une variable `canard` de type `Animal` dont les attributs `espèce`, `nom` et `cri` sont respectivement `"cairina moschata"`, `"Donald"` et `"coin"`.
    
    ``` py linenums="1" 
    canard = créer_animal("cairina moschata", "Donald", "coin")
    ```

4.  Écrire une fonction `crier` qui prend en entrée un objet `ani` de type `Animal` de telle sorte que l'instruction `crier(canard)` affiche le texte :
    
    ``` py linenums="1" 
    def crier(animal):
        print(f"Le {animal.espèce} {animal.nom} fait {animal.cri}")
    crier(canard)
    ```
</div>

<div class="exo" markdown="1">
Écrire une fonction `indices_occurrences` qui étant donné un tableau (éventuellement vide) `tab` d'entiers de type quelconque et un entier `e`, détermine la liste des indices (**rangés par ordre décroissant**) des éléments de `tab` qui sont égaux à `e`.

**Rappel.** Vous pourrez sans justification supplémentaire utiliser (si besoin) les instructions `nombre_occurrences(tab, e)`, et `tab.append(e)`.

``` py linenums="1" 
def indices_occurrences(tab, e):
    """ [int], int -> [int] """
    lst = []
    for i in range(len(tab) - 1, -1, -1):
        if tab[i] == e:
            lst.append(i)
    return lst
```
</div>

