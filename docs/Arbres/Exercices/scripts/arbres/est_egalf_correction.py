def est_egalf(a1, a2):
    """ Arbre, Arbre -> bool
    Renvoie True si et seulement si les arbres a1 et a2 sont faiblement égaux """
    if est_vide(a1) and est_vide(a2):
        return True
    elif est_vide(a1) ^ est_vide(a2):
        return False
    else:
        return etiquette(a1) == etiquette(a2) and ((est_egalf(gauche(a1), gauche(a2)) and est_egalf(droit(a1), droit(a2))) or (est_egalf(gauche(a1), droit(a2)) and est_egalf(droit(a1), gauche(a2))))
