def somme(a):
    """ Arbre -> int
    Renvoie la somme des éléments de l'arbre a """
    if est_feuille(a):
        return etiquette(a)
    elif est_vide(gauche(a)):
        return etiquette(a) + somme(droit(a))
    elif est_vide(droit(a)):
        return etiquette(a) + somme(gauche(a))
    else:
        return etiquette(a) + somme(gauche(a)) + somme(droit(a))
