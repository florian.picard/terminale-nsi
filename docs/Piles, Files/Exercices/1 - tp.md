---
title: Certains s'empilent, d'autres se défilent...
---


## Des chapeaux et des bonhommes de neige

Cinq chapeaux empilés sont distribués, en commençant en haut et allant vers le bas, à cinq bons hommes de neige en commençant à gauche et finissant à droite. À la fin, chaque bonhomme de neige devrait recevoir un chapeau à sa taille.

![img](chapeaux_neige.png)

<div class="question" markdown="1">
Quelle pile de chapeaux correspond à quelle rangée de bonshommes de neige ?
</div>


### Une pile de chapeaux

On représente un chapeau par un objet de type `Chapeau` qui possède un attribut `taille` qui représente sa taille : ainsi le plus petit chapeau a une taille de 1, et le plus grand chapeau a une taille de 5. On modélise une **pile** de chapeaux par un objet de type `Pile`.

Les interfaces des classes `Chapeau` et `Pile` sont les suivantes :

-   classe `Chapeau`
    
    <table border="2" cellspacing="0" cellpadding="6" rules="groups" frame="hsides">
    
    
    <colgroup>
    <col  class="org-left" />
    
    <col  class="org-left" />
    
    <col  class="org-left" />
    </colgroup>
    <thead>
    <tr>
    <th scope="col" class="org-left">Fonction</th>
    <th scope="col" class="org-left">Signature</th>
    <th scope="col" class="org-left">Description</th>
    </tr>
    </thead>
    
    <tbody>
    <tr>
    <td class="org-left"><code>Chapeau(c)</code></td>
    <td class="org-left"><code>int -&gt; Chapeau</code></td>
    <td class="org-left">Renvoie un chapeau de taille <code>c</code></td>
    </tr>
    
    
    <tr>
    <td class="org-left"><code>c.affiche()</code></td>
    <td class="org-left"><code>() -&gt; Nonetype</code></td>
    <td class="org-left">Affiche le chapeau <code>c</code></td>
    </tr>
    </tbody>
    </table>

-   classe `Pile`
    
    <table border="2" cellspacing="0" cellpadding="6" rules="groups" frame="hsides">
    
    
    <colgroup>
    <col  class="org-left" />
    
    <col  class="org-left" />
    
    <col  class="org-left" />
    </colgroup>
    <thead>
    <tr>
    <th scope="col" class="org-left">Fonction</th>
    <th scope="col" class="org-left">Signature</th>
    <th scope="col" class="org-left">Description</th>
    </tr>
    </thead>
    
    <tbody>
    <tr>
    <td class="org-left"><code>Pile()</code></td>
    <td class="org-left"><code>() -&gt; Pile</code></td>
    <td class="org-left">Créer une pile vide</td>
    </tr>
    
    
    <tr>
    <td class="org-left"><code>p.est_vide()</code></td>
    <td class="org-left"><code>() -&gt; Bool</code></td>
    <td class="org-left">Détermine si la pile <code>p</code> est vide.</td>
    </tr>
    
    
    <tr>
    <td class="org-left"><code>p.empiler(c)</code></td>
    <td class="org-left"><code>Chapeau -&gt; Nonetype</code></td>
    <td class="org-left">Déposer le chapeau <code>c</code> au sommet de la pile <code>p</code></td>
    </tr>
    
    
    <tr>
    <td class="org-left"><code>p.depiler()</code></td>
    <td class="org-left"><code>() -&gt; Chapeau</code></td>
    <td class="org-left">Renvoie le chapeau au sommet de la pile <code>p</code>,</td>
    </tr>
    
    
    <tr>
    <td class="org-left">&#xa0;</td>
    <td class="org-left">&#xa0;</td>
    <td class="org-left">lorque cela est possible.</td>
    </tr>
    
    
    <tr>
    <td class="org-left"><code>p.affiche()</code></td>
    <td class="org-left"><code>() -&gt; Nonetype</code></td>
    <td class="org-left">Affiche la pile <code>p</code> sans la modifier.</td>
    </tr>
    </tbody>
    </table>

Exécuter le bloc ci-dessous pour charger les classes `Chapeau` et `Pile`.

{{ IDE('scripts/tp/pile_chapeau_exemple') }}

<div class="question" markdown="1">
Écrire le code python d'une fonction `init_piles` qui renvoie les 4 piles `p1`, `p2`, `p3` et `p4` représentant respectivement les piles de chapeaux 1, 2, 3, et 4. 
</div>

{{ IDE('scripts/tp/pile_exemple') }}


### Une file de bonshommes de neige

On représente un bonhomme de neige par un objet de type `Bonhomme` muni d'un attribut `taille` qui représente sa taille : ainsi le plus petit bonhomme de neige a une taille de 1, et le plus grand bonhomme de neige a une taille de 5. Si `b` est un objet de type `Bonhomme` et `c` un objet de type `Chapeau`, alors `b.content(c)` renvoie `True` si et seulement si le chapeau `c` est celui du bonhomme `b`.

On modélise une **file** de bonshommes de neige par un objet de type `File`, qui contient les bonshommes de neige.

Les interfaces des classes `Bonhomme` et `File` sont les suivantes :

-   classe `Bonhomme`
    
    <table border="2" cellspacing="0" cellpadding="6" rules="groups" frame="hsides">
    
    
    <colgroup>
    <col  class="org-left" />
    
    <col  class="org-left" />
    
    <col  class="org-left" />
    </colgroup>
    <thead>
    <tr>
    <th scope="col" class="org-left">Fonction</th>
    <th scope="col" class="org-left">Signature</th>
    <th scope="col" class="org-left">Description</th>
    </tr>
    </thead>
    
    <tbody>
    <tr>
    <td class="org-left"><code>Bonhomme(b)</code></td>
    <td class="org-left"><code>int -&gt; Bonhomme</code></td>
    <td class="org-left">Renvoie un bonhomme de taille <code>b</code></td>
    </tr>
    
    
    <tr>
    <td class="org-left"><code>b.content(c)</code></td>
    <td class="org-left"><code>Chapeau -&gt; Bool</code></td>
    <td class="org-left">Renvoie <code>True</code> si le chapeau <code>c</code> convient au bonhomme <code>b</code></td>
    </tr>
    
    
    <tr>
    <td class="org-left"><code>b.affiche()</code></td>
    <td class="org-left"><code>() -&gt; Nonetype</code></td>
    <td class="org-left">Affiche le bonhomme <code>b</code></td>
    </tr>
    </tbody>
    </table>

-   classe `File`
    
    <table border="2" cellspacing="0" cellpadding="6" rules="groups" frame="hsides">
    
    
    <colgroup>
    <col  class="org-left" />
    
    <col  class="org-left" />
    
    <col  class="org-left" />
    </colgroup>
    <thead>
    <tr>
    <th scope="col" class="org-left">Fonction</th>
    <th scope="col" class="org-left">Signature</th>
    <th scope="col" class="org-left">Description</th>
    </tr>
    </thead>
    
    <tbody>
    <tr>
    <td class="org-left"><code>File()</code></td>
    <td class="org-left"><code>() -&gt; File</code></td>
    <td class="org-left">Créer une file vide</td>
    </tr>
    
    
    <tr>
    <td class="org-left"><code>f.est_vide()</code></td>
    <td class="org-left"><code>() -&gt; Bool</code></td>
    <td class="org-left">Détermine si la file <code>f</code> est vide.</td>
    </tr>
    
    
    <tr>
    <td class="org-left"><code>f.enfiler(c)</code></td>
    <td class="org-left"><code>Bonhomme -&gt; Nonetype</code></td>
    <td class="org-left">Déposer le bonhomme <code>b</code> à la fin de la file <code>f</code></td>
    </tr>
    
    
    <tr>
    <td class="org-left"><code>f.defiler()</code></td>
    <td class="org-left"><code>() -&gt; Chapeau</code></td>
    <td class="org-left">Renvoie le premier bonhomme de la file <code>f</code>,</td>
    </tr>
    
    
    <tr>
    <td class="org-left">&#xa0;</td>
    <td class="org-left">&#xa0;</td>
    <td class="org-left">lorque cela est possible.</td>
    </tr>
    
    
    <tr>
    <td class="org-left"><code>f.affiche()</code></td>
    <td class="org-left"><code>() -&gt; Nonetype</code></td>
    <td class="org-left">Affiche la file <code>f</code> sans la modifier.</td>
    </tr>
    </tbody>
    </table>

Exécuter le bloc ci-dessous pour charger les classes `Bonhomme` et `File`.

{{ IDE('scripts/tp/file_bonhomme_exemple') }}

<div class="question" markdown="1">
Écrire le code python d'une fonction `init_files` qui renvoie les 4 files `f1`, `f2`, `f3` et `f4` représentant respectivement les files de bonshommes de neige 1, 2, 3, et 4. 
</div>

{{ IDE('scripts/tp/file_exemple') }}


### Compatiblité entre pile de chapeaux et file de bonshommes

On dit qu'une pile de chapeaux est **compatible** avec une file de bonshommes de neige si et seulement si tous les bonhommes de neige sont content lorsqu'ils prennent le premier chapeau de la pile dans l'ordre imposé par la file.

On se propose de résoudre le problème de l'association d'une pile de chapeaux avec une file de bonshommes de neige. On souhaite écrire une fonction `compatibles` qui étant donné une pile de chapeaux `p` et une file de bonshommes de neige `f`, renvoie `True` si et seulement si les deux structures sont compatibles.

{{ IDE('scripts/tp/compatibles') }}


### Le problème de la mutabilité et solution du problème

<div class="question" markdown="1">
1.  Afficher le contenu des piles `p1` et `f1` **après** exécution de la fonction `compatibles`.
2.  Que constate-t-on ? À quoi cela est-il dû ?
3.  Quel problème cela va-t-il éventuellement poser ?
</div>


#### Modification de la fonction `compatibles`

On souhaite modifier le code de la fonction `compatibles` afin qu'à la fin de l'exécution de l'instruction `compatibles(p, f)`, les piles `p` et `f` soient dans le même état qu'au début. 

Pour cela, lors de l'exécution de la fonction `compatibles`, à chaque dépilement (resp. défilement) de la pile `p` (resp. `f`) on empilera le chapeau `c` (resp. enfilera le bonhomme `b`) obtenu dans une pile `pile_aux` (resp. une file `file_aux`), et on s'assurera de restaurer l'état initial avant toute instruction `return`.

On écrira pour cela  une fonction `restaure_pile(p, p_aux)` (resp. `restaure_file(f, f_aux)`) restaurant l'état initial de `p` à l'aide de `p_aux` (resp. `f` à l'aide de `f_aux`). On donne ci-dessous un schéma explicatif de l'algorithme de la fonction `restaure_pile`. Une idée similaire est à adapter pour la fonction `restaure_file`.

![img](schéma_reinit.png)

{{ IDE('scripts/tp/restaure_pile') }}

{{ IDE('scripts/tp/restaure_pile') }}


#### Calcul de la solution du problème

Écrire une fonction `solution` qui renvoie la liste des couples solution au problème.

Par exemple, si la pile de chapeau numéro 1 correspond à la file de bonhomme de neige 2, alors on ajoutera l'entrée `(1, 2)` à la liste des couples solution.

{{ IDE('scripts/tp/solution') }}


## Une rampe de billes

Sur une rampe, il y a 10 billes numérotées dans l'ordre. Le long de la rampe, il y a trois trous A, B et C : le trou A peut contenir trois billes au maximum, le trou B deux billes et le trou C une seule bille au maximum.

Quand les billes roulent sur la rampe, elles tombent successivement dans les trous jusqu'à ce qu'elles les remplissent (les billes 1, 2 et 3 tombent dans le trou A, les billes 4 et 5 tombent dans le trou B et la bille 6 tombe dans le trou C). Les autres billes passent par-dessus et continuent leur chemin jusqu'à la fin de la rampe.

Quand toutes les billes ont parcouru la rampe, les ressorts, placés dans les trous A à C, éjectent les billes qu'ils contenaient : d'abord, les trois billes du trou A, ensuite, celles du trou B et finalement, celle du trou C. Les billes sont ainsi poussées sur la rampe. On attend que toutes les autres billes aient passé avant qu'un ressort ne soit relâché.

![img](rampe.png)

<div class="question" markdown="1">
1.  Dans quel ordre les billes de la séquence 1 à 10 seront-elles alignées à la fin ?
2.  Sur une installation du même type, dix billes initialement ordonnées de 1 à 10 sur la rampe arrivent dans l'ordre : 8, 9, 10, 4, 3, 2, 1, 5, 7, 6.
    
    Combien de trous y avait-il sur le parcours, et quelle est leur profondeur ? On justifiera la réponse.

3.  Compléter la fonction `arrivee_rampe(profondeurs)` qui prend en paramètre une liste d'entiers donnant la profondeur de chacun des trous sur une installation du même type que celle vue précédemment, dans l'ordre dans lequel ils sont positionnés sur le parcours, et qui renvoie la liste des entiers 1 à 10, dans l'ordre dans lequel les billes 1 à 10 arriveraient sur la rampe.
    
    On utilisera pour cela les classes `Pile` et `File` définies précédemment. Ces classes ont toutes les deux été enrichies d'un attribut `max_elt` et d'une méthode `est_pleine` qui renvoie `True` si et seulement si la pile (resp. file) contient `max_elt` éléments. Lors d'un empilement (resp. enfilement), on vérifie d'abord que la pile (resp. file) n'est pas pleine. Si la pile (resp. file) est pleine, alors on soulève une exception. On appelle ce genre de pile (resp. files) des piles (resp. files) **bornées**.
    
    <table border="2" cellspacing="0" cellpadding="6" rules="groups" frame="hsides">
    
    
    <colgroup>
    <col  class="org-left" />
    
    <col  class="org-left" />
    
    <col  class="org-left" />
    </colgroup>
    <thead>
    <tr>
    <th scope="col" class="org-left">Fonction</th>
    <th scope="col" class="org-left">Signature</th>
    <th scope="col" class="org-left">Description</th>
    </tr>
    </thead>
    
    <tbody>
    <tr>
    <td class="org-left"><code>Pile(max_elt = n)</code></td>
    <td class="org-left"><code>int -&gt; Pile</code></td>
    <td class="org-left">Renvoie une pile bornée de capacité maximale <code>n</code></td>
    </tr>
    
    
    <tr>
    <td class="org-left"><code>p.est_pleine()</code></td>
    <td class="org-left"><code>() -&gt; Bool</code></td>
    <td class="org-left">Détermine si la pile a atteint sa capacité maximale.</td>
    </tr>
    
    
    <tr>
    <td class="org-left"><code>File(max_elt = n)</code></td>
    <td class="org-left"><code>int -&gt; File</code></td>
    <td class="org-left">Renvoie une file bornée de capacité maximale <code>n</code></td>
    </tr>
    
    
    <tr>
    <td class="org-left"><code>f.est_pleine()</code></td>
    <td class="org-left"><code>() -&gt; Bool</code></td>
    <td class="org-left">Détermine si la file a atteint sa capacité maximale.</td>
    </tr>
    </tbody>
    </table>
</div>

{{ IDE('scripts/tp/arrive_rampe') }}


## Conclusion

<div class="question" markdown="1">
En anglais, l'acronyme `FILO` signifie `Firt In Last Out` (premier entré dernier sorti), et l'acronyme `FIFO` signifie `Fist In First Out` (premier entré premier sorti).

Quel acronyme peut s'appliquer à une pile ? À une file ?
</div>


## Documents

-   [Piles FILO, files FIFO (sujet)](tp.pdf)
-   Fichiers python :
    -   [templates](scripts/tp/tp.py)
    -   [fichier `ds.py`](scripts/tp/ds.py)

