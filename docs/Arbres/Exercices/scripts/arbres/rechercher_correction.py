def rechercher(a, e):
    """ Arbre, int -> bool
    Renvoie True si et seulement si e est une des étiquettes de a """
    if est_vide(a):
        return False
    return etiquette(a) == e or rechercher(gauche(a), e) or rechercher(droit(a), e)
