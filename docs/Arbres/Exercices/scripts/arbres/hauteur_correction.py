def hauteur(a):
    """ Arbre -> int
    Renvoie la hauteur de l'arbre """
    if est_feuille(a):
        return 1
    elif est_vide(gauche(a)):
        return 1 + hauteur(droit(a)) 
    elif est_vide(droit(a)):
        return 1 + hauteur(gauche(a))
    else:
        return 1 + max(hauteur(gauche(a)), hauteur(droit(a)))
