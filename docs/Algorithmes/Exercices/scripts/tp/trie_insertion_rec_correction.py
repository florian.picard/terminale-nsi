def trie_insertion_rec(l):
    """ Liste -> Liste
    Trie la liste l à l'aide de l'algorithme du tri par insertion """
    if est_vide(l):
        return l
    else:
        premier = tete(l)
        reste_trié = trie_insertion_rec(queue(l))
        return insere_trie(reste_trié, premier)
