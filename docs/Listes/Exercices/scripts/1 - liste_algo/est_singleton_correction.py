def est_singleton(l):
    """ Liste -> bool
    Détermine si la liste est constituée d'un seul élément. """
    if est_vide(l):
        return False
    return est_vide(queue(l))
