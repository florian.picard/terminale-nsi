def tri_selection_iter(tab):
    """ [int] -> [int]
    Trie le tableau tab (tri par sélection) """
    if tab == []:
        return []
    for i in range(len(tab)):
        indice_mini = mini_a_partir(tab, i)
        tab[i], tab[indice_mini] = tab[indice_mini], tab[i]
    return tab
