def test_est_singleton():
    """ Tests pour la fonction est_singleton """
    print("Tests de est_singleton passés avec succès.")
    assert est_singleton(creer_vide()) == False
    assert est_singleton(ajoute(creer_vide(), 3)) == True
    assert est_singleton(ajoute(ajoute(creer_vide(), 3), 2)) == False
    return True

# from tp import est_singleton
test_est_singleton()
