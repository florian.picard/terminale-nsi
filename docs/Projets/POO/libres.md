---
title: Projets libres
---

Quelques idées de projets de difficulté variable.


## Yahtzee

Implémenter le [Yahtzee](https://fr.wikipedia.org/wiki/Yahtzee). Structure possible :

-   une classe `Dé`
-   une classe `FeuilleScore`
-   une classe `Joueur`
-   une classe `Jeu`


## Plateau d'échecs

Implémenter les [échecs](https://fr.wikipedia.org/wiki/%C3%89checs). Implémenter les fonctionnalités les unes après les autres :

-   représenter les différentes pièces du jeu
-   initialiser un plateau
-   déplacer une pièce (lister les déplacements autorisé pour une pièce)
-   vérifier si une case est attaquée par une pièce de l'adversaire
-   vérifier un mat

