def recherche_noeud(a, nom):
    """ Arbre, str -> Arbre
    Renvoie le sous-arbre de a dont la racine a pour étiquette nom et est de profondeur minimale dans a """
    file_noeud = [a]
    liste_etiquettes = []
    while not file_noeud == []:
        n = file_noeud[0]
        if etiquette(n) == nom:
            return n
        file_noeud.pop(0)
        if not est_vide(gauche(n)):
            file_noeud.append(gauche(n))
        if not est_vide(droit(n)):
            file_noeud.append(droit(n))
    return None
