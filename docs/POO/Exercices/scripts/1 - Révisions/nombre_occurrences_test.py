def test_nombre_occurrences():
    # Tests pour la fonction nombre_occurrences
    assert nombre_occurrences([], 0) == 0
    assert nombre_occurrences([0, 1, 2, 3], 0) == 1
    assert nombre_occurrences([0, 1, 2, 0], 0) == 2
    assert nombre_occurrences([0, 1, 2, 3], 2) == 1
    assert nombre_occurrences([0, 1, 2, 3], 4) == 0
    return True

# from tp import nombre_occurrences
test_nombre_occurrences()
