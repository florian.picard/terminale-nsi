def test_nb_joueurs():
    """ Tests pour la fonction nb_joueurs """
    print("Tests de nb_joueurs passés avec succès.")
    return True

# from tp import nb_joueurs
# test_nb_joueurs()
def test_nb_rounds():
    """ Tests pour la fonction nb_rounds """
    print("Tests de nb_rounds passés avec succès.")
    return True

# from tp import nb_rounds
# test_nb_rounds()
def test_occurrences():
    """ Tests pour la fonction occurrences """
    assert occurrences(a, "Kamel") == 3
    assert occurrences(a, "Carine") == 2
    assert occurrences(a, "Joris") == 1
    assert occurrences(a, "Abdou") == 1
    assert occurrences(a, "Ada") == 0
    print("Tests de occurrences passés avec succès.")
    return True

# from tp import occurrences
# test_occurrences()
def test_nombre_matchs():
    """ Tests pour la fonction nombre_matchs """
    assert nombre_matchs(a, "Kamel") == 2
    assert nombre_matchs(a, "Carine") == 2
    assert nombre_matchs(a, "Joris") == 1
    assert nombre_matchs(a, "Abdou") == 1
    assert nombre_matchs(a, "Ada") == 0
    print("Tests de nombre_matchs passés avec succès.")
    return True

# from tp import nombre_matchs
# test_nombre_matchs()
def test_liste_joueurs():
    """ Tests pour la fonction liste_joueurs """
    for j in ["Kamel", "Carine", "Joris", "Abdou"]:
        assert j in liste_joueurs(a)
    assert not "ada" in liste_joueurs(a)
    print("Tests de liste_joueurs passés avec succès.")
    return True

# from tp import liste_joueurs
# test_liste_joueurs()
def test_niveau():
    """ Tests pour la fonction niveau """
    print("Tests de niveau passés avec succès.")
    return True

# from tp import niveau
# test_niveau()
def test_vaincus_joueur():
    """ Tests pour la fonction vaincus_joueur """
    print("Tests de vaincus_joueur passés avec succès.")
    return True

# from tp import vaincus_joueur
# test_vaincus_joueur()
def test_parcours_largeur():
    """ Tests pour la fonction parcours_largeur """
    print("Tests de parcours_largeur passés avec succès.")
    return True

# from tp import parcours_largeur
# test_parcours_largeur()
def test_recherche_noeud():
    """ Tests pour la fonction recherche_noeud """
    print("Tests de recherche_noeud passés avec succès.")
    return True

# from tp import recherche_noeud
# test_recherche_noeud()
def test_joueurs_vaincus():
    """ Tests pour la fonction joueurs_vaincus """
    print("Tests de joueurs_vaincus passés avec succès.")
    return True

# from tp import joueurs_vaincus
# test_joueurs_vaincus()
