def insere(l, e, i):
    """ Liste, int, int -> Liste
    Insère l'élément e dans la liste l à l'indice i """
    if i == 0:
        return ajoute(l, e)
    else:
        e_dans_queue = insere(queue(l), e, i - 1)
        return ajoute(e_dans_queue, tete(l))
