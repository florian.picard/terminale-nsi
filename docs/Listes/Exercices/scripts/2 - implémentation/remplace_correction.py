def remplace_r2(m, i, e):
    """ Maillon, int, int -> Maillon
    m est non vide, 0 <= i < longueur(m)
    Renvoie le premier maillon de la chaine où la valeur d'indice i a été remplacée par e. """
    # version récursive, non mutable
    if i == 0:
        return Maillon(e, queue(m))
    else:
        return Maillon(tete(m), remplace_r(queue(m), i - 1, e))

def remplace_r(m, i, e):
    """ Maillon, int, int -> Maillon
    m est non vide, 0 <= i < longueur(m)
    Renvoie le premier maillon de la chaine où la valeur d'indice i a été remplacée par e. """
    # version récursive, mutable
    if i == 0:
        m.valeur = e
    else:
        remplace_r2(queue(m), i - 1, e)
    return m

def remplace_i(m, i, e):
    """ Maillon, int, int -> Maillon
    m est non vide, 0 <= i < longueur(m)
    Renvoie le premier maillon de la chaine où la valeur d'indice i a été remplacée par e. """
    # version impérative, mutable
    courant = m
    count = 0
    while count < i:
        count = count + 1
        courant = queue(courant)
    courant.valeur = e
    return m

def remplace_i2(m, i, e):
    """ Maillon, int, int -> Maillon
    m est non vide, 0 <= i < longueur(m)
    Renvoie le premier maillon de la chaine où la valeur d'indice i a été remplacée par e. """
    # version impérative, non mutable
    courant = m
    count = 0
    pile = []
    while count < i:
        count = count + 1
        pile.append(tete(courant))
        courant = queue(courant)
    nouveau = Maillon(e, queue(courant))
    while pile:
        e = pile.pop()
        nouveau = Maillon(e, nouveau)
    return nouveau
