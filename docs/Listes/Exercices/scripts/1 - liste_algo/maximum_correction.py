def maximum2(a, b):
    """ int, int -> int
    Calcule l'élément maximum parmis a et b
    """
    if a < b :
        return b
    else:
        return a
    
def maximum(l):
    """ liste -> int
    Calcule l'élément maximum parmis les éléments de l """
    if est_singleton(l):
        return tete(l)
    else:
        return maximum2(tete(l), maximum(queue(l)))
