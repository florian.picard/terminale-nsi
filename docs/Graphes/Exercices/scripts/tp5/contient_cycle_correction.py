def contient_cycle(G):
    """ Graphe -> bool
    Renvoie True ssi le graphe orienté G contient un cycle """
    couleurs = {s:BLANC for s in G.sommets()}
    for s in G.sommets():
        if couleurs[s] == NOIR:
            continue
        trouve_cycle_depuis_s = parcours_cycle(G, s, couleurs)
        if trouve_cycle_depuis_s:
            return True
    return False
