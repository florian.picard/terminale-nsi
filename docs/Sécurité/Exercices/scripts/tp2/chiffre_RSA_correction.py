def chiffre_RSA(M, pk):
    """ int, (int, int) -> int
    Chiffre le message M à l'aide de la clé publique """
    n, e = pk
    return (M ** e) % n
