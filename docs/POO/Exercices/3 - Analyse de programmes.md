---
title: Analyse de programme
---

<div class="exo" markdown="1">
1.  Créez une classe `Voiture` avec deux attributs d’instance:
    -   `couleur`, qui stocke la couleur de la voiture sous forme de chaîne de caractères
    -   `kilometrage`, qui stocke le nombre de kilomètres de la voiture sous forme d’entier.

2.  Instanciez deux objets de type `Voiture` : une voiture bleue de 20 000 kilomètres et une voiture rouge de 30 000 kilomètres.

3.  1.  Écrire une méthode `affiche` de la classe `Voiture` : cette méthode affiche les attributs de la voiture `self` correspondant.
    
    2.  Comment utiliser cette méthode pour faire afficher :
        
        ```
        La voiture rouge a 20000 kilomètres
        La voiture bleue a 30000 kilomètres
        ```
</div>

<div class="exo" markdown="1">
On considère le programme suivant :

``` py linenums="1" 
class Chat:
    def bonjour(self):
        print("miaou")

class Chien:
    def bonjour(self):
        print("ouaf")

c = Chat()
c.bonjour()
c = Chien()
c.bonjour()
```

1.  Qu'affiche le programme ?
2.  Expliquer le résultat.
</div>

<div class="exo" markdown="1">


1.  1.  Écrire le code d'une classe `Hero` qui comporte trois attributs : `nom`, `cri_victoire`, `cri_defaite` (des chaines de caractères), `force` (un entier compris entre 0 et 100), et `pdv` (un entier valant 10 par défaut, représentant les points de vie du héro).
    
    2.  Instancier deux variables de type `Hero` :
        -   `hercule`, représentant le héros Hercule de force 99, criant "HAN" dans la victoire, mais "Ouch" il est blessé.
        
        -   `limace`, représentant le héros dit "la limace folle" de force 100, criant "GNIII" lorsqu'elle terrasse ses ennemis et "Blup" lorsqu'elle est blessée.

2.  Écrire le code des méthodes  `crier_victoire` et `crier_defaite` (de la classe `Hero`) qui affichent respectivement le `cri_victoire` et le `cri_defaite` de l'objet `self`.
    
    ``` py linenums="1" 
    def crier_victoire(self):
        """ Hero -> None"""
        pass
    ```
    
    ``` py linenums="1" 
    def crier_defaite(self):
        """ Hero -> None"""
        pass
    ```

3.  Écrire une méthode `recoit_blessure` (de la classe `Hero`) qui prend en argument un entier `val` et qui diminue les points de vie du héros `self` de ce montant, puis affiche le cri de défaite du héros `self`.
    
    ``` py linenums="1" 
    def recoit_blessure(self, val):
        """ Hero, int -> None """
    ```

4.  Écrire une méthode `attaquer` de la classe `Hero` qui prent en argument un objet `other` de type `Hero` et qui simule l'attaque de `other` par le héros `self` :
    -   si la force de `other` est supérieure ou égale à celle de `self`, alors elle affiche `Rien ne se passe`.
    
    -   sinon, elle affiche le cri de victoire de l'objet `self` et inflige 1 point de blessure à l'objet `other`.
        
        ``` py linenums="1" 
        def attaquer(self, other):
            """ Hero, Hero -> None """
        ```

5.  Qu'affiche le code suivant ?
    
    ``` py linenums="1" 
    hercule.attaquer(limace)
    limace.attaquer(hercule)
    print(hercule.pdv, limace.pdv)
    ```
</div>

<div class="exo" markdown="1">
On considère le code python ci-dessous.

``` py linenums="1" 
class Personne:
    def __init__(self, nom, année_naissance, lieu_naissance):
        """ str, int, str -> None """
        self.nom = nom
        self.année_naissance = année_naissance
        self.lieu_naissance = lieu_naissance

class Film:
    def __init__(self, titre, réalisateur):
        """ str, Personne -> None """
        self.titre = titre
        self.réalisateur = réalisateur
```

1.  Comment créer une instance de la classe `Personne` appelée `real` pour le réalisateur David Lynch né en 1946 à Missoula ?

2.  1.  Écrire une méthode `__str__` de la classe `Personne` qui de telle sorte que `real.__str__()` renvoie `David Lynch est né(e) en 1946 à Missoula`.
    
    1.  Exécuter l'instruction `print(real)`. Que constate-t-on ? À quoi sert la méthode `__str__` ?

3.  1.  Quelle instruction python permet de créer une instance de la classe `Film` appelée `flm` pour le film Dune, sorti en 1984 et réalisé par David Lynch.
    
    2.  Qu'affichent les instructions suivantes :
        
        ``` py linenums="1" 
        print(flm.titre)
        print(flm.réalisateur.nom)
        print(flm.réalisateur)
        ```
    
    3.  Écrire une méthode `__str__` de la classe `Film` de telle sorte que l'instruction `print(flm)` affiche `Dune a été réalisé par David Lynch originaire de Missoula`.
</div>

<div class="exo" markdown="1">
On considère la classe `Point` suivante :

``` py linenums="1" 
class Point:
    def __init__(self, x, y):
        self.x = x
        self.y = y

    def deplace(self, dx, dy):
        self.x = self.x + dx
        self.y = self.y + dy

    def symetrique(self):
        return Point(-self.x, -self.y)

    def __str__(self):
        return f"Point({self.x}, {self.y})"
```

1.  1.  Comment instancier une variable `a` de type `Point` qui représente le point  $A(2 ; 4)$ ?
    
    2.  Quels sont les attributs et les méthodes de cette classe ?
    3.  Donner la signature complète des méthodes `deplace` et `symetrique` de la classe `Point`.
    4.  Qu'affiche le code ci-dessous ? Justifier votre réponse. 
        
        ``` py linenums="1" 
        b = Point(1, 2) # représente B(1 ; 2)
        print(b)
        b.deplace(3, 5)
        print(b)
        b.symetrique()
        print(b)
        ```

2.  1.  Écrire une classe `Segment` d'attributs `debut` et `fin`, tous deux de type `Point`. Puis, écrire l'instruction python qui permet d'instancier une variable `s1` représentant le segment $[AB]$.
    
    2.  Écrire une méthode `deplace` de la classe `Segment` qui prend en entrée deux nombres flottants `dx` et `dy` et qui translate le segment `self` selon le vecteur $\begin{pmatrix} \texttt{dx} \\ \texttt{dy} \end{pmatrix}$.
        
        ``` py linenums="1" 
        def deplace(self, dx, dy):
            """ Segment, float, float -> None """
            pass
        ```
    
    3.  Écrire une méthode `symetrique` de la classe `Segment` qui renvoie le segment symétrique du segment `self` par rapport à l'origine du repère. 
        
        ``` py linenums="1" 
        def symetrique(self):
            """ Segment -> Segment """
            pass
        ```
    
    4.  La méthode `__str__` de la classe `Segment` est définie de la manière suivante :
        
        ``` py linenums="1" 
        def __str__(self):
            return f"Segment {self.debut.x, self.debut.y} -- {self.fin.x, self.fin.y}" 
        ```
        
        Quel affichage réalise le code suivant ? Justifier votre réponse.
        
        ``` py linenums="1" 
        print(s1)
        s1.deplace(8, 2)
        print(s1)
        s1.symetrique()
        print(s1)
        print(a, b)
        ```
</div>

<div class="exo" markdown="1">


1.  1.  Définir une classe `Carte` qui représente une carte à jouer, définie par sa valeur (de 1 à 13), et sa couleur (pique, cœur, carreau, trèfle).
    
    2.  Instancier une variable `c` de type `Carte` représentant la dame de pique.

2.  1.  Définir une méthode `nom` de la classe `Carte` qui renvoie le nom de la carte. Par exemple, pour la carte `c`, la méthode renverra `"Dame de pique"`.
    
    2.  Utiliser cette méthode pour afficher dans la console le nom de la carte `c`.

3.  Définir une classe `Paquet` qui contient les cartes d'un paquet de 52 cartes.

4.  1.  Définir une méthode `tirer` de la classe `Paquet` qui tire au hasard une carte du paquet et qui renvoie son nom. On utilisera la fonction `randint` de la biblithèque `random` qui prend deux arguments `a` et `b` et renvoie un nombre entier aléatoire compris entre `a` et `b` (inclus). La carte tirée sera supprimée du paquet une fois tirée. On pourra utiliser l'instruction `lst.pop(indice)` qui supprime (en renvoyant) l'élément d'indice `indice` de la liste `lst`.
    
    2.  Utiliser cette méthode pour tirer toutes les cartes du paquet dans un ordre aléatoire. On affichera les noms des cartes ainsi tirées.
</div>

