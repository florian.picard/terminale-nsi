def test_tri_selection():
    """ Tests pour la fonction tri_selection """
    data = [
        [[0], [0]],
        [[9,4,6,1], [1,4,6,9]],
        [[1, 52, 6, -9, 12], [-9, 1, 6, 12, 52]],
        [[1, 2], [1, 2]]
    ]
    for tab, tab_tri in data:
        out = tri_selection(tab)
        assert out == tab_tri, f"\ Le test suivant n'a pas été validé.\nEntrée : {tab}\nSortie : {out}\nAttendu : {tab_tri}"
    print("Tests de tri_selection passés avec succès.")
    return True

# from tp import tri_selection
test_tri_selection()
