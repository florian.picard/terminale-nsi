class ABR(ArbreBinaire):
    """ Représentation dun ABR (Arbre Binaire de Recherche) """
    def inserer(self, e):
        """ ABR, int -> NoneType
        Insère l'élément e dans l'ABR self """
        if self.est_vide():
            self.racine = Noeud(e, ABR(), ABR())
        elif e < self.etiquette():
            self.racine.gauche.inserer(e)
        else:
            self.racine.droit.inserer(e)
    
    def appartient(self, e):
        """ ABR, int -> Nonetype
        Détermine si l'élément e est une des étiquettes de l'ABR self """
        if self.est_vide():
            return False
        elif self.etiquette() == e:
            return True
        elif e < self.etiquette():
            return self.gauche().appartient(e)
        else:
            return self.droit().appartient(e)
    
    def minimum(self):
        """ ABR -> int
        Renvoie la plus petite étiquette de self """
        if self.est_vide():
            return -float("inf")
        elif self.est_feuille():
            return self.etiquette()
        else:
            return self.gauche().minimum()
