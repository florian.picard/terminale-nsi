---
title: Listes et algorithmes récursifs
---


## Présentation

Stocker les données est un problème fondamental en informatique. Nous verrons au cours de cette année différentes manières de le faire. Étant donné une collection finie de valeurs $a$, $b$, $c$, $\ldots$, on décide de les stocker de manière *séquentielle* : le premier élément de la liste est $a$, le second $b$, etc.

On utilise pour cela la structure de *liste*. Si $l$ est une liste de nombres, on dispose alors des fonctions suivantes :

<table border="2" cellspacing="0" cellpadding="6" rules="groups" frame="hsides">


<colgroup>
<col  class="org-left" />

<col  class="org-left" />
</colgroup>
<thead>
<tr>
<th scope="col" class="org-left">Fonction</th>
<th scope="col" class="org-left">Description</th>
</tr>
</thead>

<tbody>
<tr>
<td class="org-left"><code>creer_vide()</code></td>
<td class="org-left">Renvoie une liste vide</td>
</tr>


<tr>
<td class="org-left"><code>est_vide(l)</code></td>
<td class="org-left">Renvoie <code>True</code> si et seulement si la liste est vide</td>
</tr>


<tr>
<td class="org-left"><code>tete(l)</code></td>
<td class="org-left">Renvoie le premier élément de la liste \(l\) (l'élément dit <b>de tête</b>)</td>
</tr>


<tr>
<td class="org-left"><code>queue(l)</code></td>
<td class="org-left">Renvoie la liste constituée de tous les éléments de \(l\) à l'exception du premier</td>
</tr>


<tr>
<td class="org-left"><code>ajoute(l, e)</code></td>
<td class="org-left">Renvoie la liste constituée de l'élément \(e\), suivi des éléments de \(l\)</td>
</tr>


<tr>
<td class="org-left"><code>affiche(l)</code></td>
<td class="org-left">Affiche la liste des éléments de \(l\) sur la sortie standard, séparés par le caractère <code>-</code>.</td>
</tr>


<tr>
<td class="org-left">&#xa0;</td>
<td class="org-left">La liste vide est représentée par <code>x</code>.</td>
</tr>
</tbody>
</table>

<div class="exo" markdown="1">
On considère la liste $l = (0, 1, 42, "Bonjour", \pi)$.

1.  Que renvoie `est_vide(l)` ?
2.  Que renvoie `tete(l)` ?
3.  Que renvoie `queue(l)` ? Quelle est le type de `queue(l)` ?
4.  Que renvoie `ajoute(l, 93)` ?
5.  Que fait l'instruction `affiche(l)` ?
</div>

**Remarque.** On peut donner une définition récursive de la structure de liste. En effet, une liste c'est : soit la liste vide ; soit un élément (la **tête**) suivit d'une liste (la **queue**).


### Interface

Exécuter le bloc ci-dessous pour charger l'interface du type `Liste`.

{{ IDE('scripts/1 - liste_algo/exemple') }}

Commenter l'affichage réalisé.


### Premier exemple

Écrire les instructions python permettant de stocker les listes

-   $l_1 = ()$
-   $l_2 = (-1)$
-   $l_3 = (5, 6, 9, -1)$
-   $l_4 = (-5, 9, 4, 9, -5, 9)$.

Vous utiliserez uniquement les fonctions `creer_vide`, `ajoute`. Vous n'avez pas le droit d'utiliser des tableaux. 

{{ IDE('scripts/1 - liste_algo/premier_exemple') }}


## Exercices

Toutes les questions suivantes doivent être traitées en utilisant uniquement les fonctions décrites dans le paragraphe d'introduction. Vous n'avez pas le droit d'utiliser les tableaux. Vous n'avez pas non plus le droit d'utiliser de boucle `for`, ou de boucle `while`. 


### Liste singleton

Écrire une fonction `est_singleton` qui prend en argument une liste `l` et qui renvoie `True` si la liste est constituée d'un seul élément, `False` dans tous les autres cas.

{{ IDE('scripts/1 - liste_algo/est_singleton') }}


### Création de liste I

Écrire une fonction `singleton` qui prend en argument un entier `n` et qui renvoie la liste ayant pour seul élément le nombre `n`.

{{ IDE('scripts/1 - liste_algo/singleton') }}


### Création de liste II

Écrire une fonction récursive `nombres` qui prend en argument un entier `n` supérieur à 1 et qui renvoie la liste des entiers de `n` (inclu) à 1 (inclu) dans l'ordre décroissant.

{{ IDE('scripts/1 - liste_algo/nombres') }}


### Création de liste III

On se propose dans cet exercice d'écrire une fonction `nombresII` qui prend en argument un entier `n` supérieur à 1 et qui renvoie la liste des entiers de 1 (inclus) à `n` (inclu) dans l'ordre croissant.

-   Écrire une fonction `nombresII_aux` qui étant donné deux entiers `n` et `i`, renvoie la liste constituée des entiers de `i` à `n` dans l'ordre croissant.
-   En déduire une fonction `nombresII` qui renvoie la liste dans l'ordre croissant des entiers de `1` à `n`. Vous appellerez pour cela la fonction `nombresII_aux` avec des arguments bien choisis.

{{ IDE('scripts/1 - liste_algo/nombresII') }}


### Longueur d'une liste

On définit la **longueur** d'une liste comme étant le nombre d'éléments constituant cette liste. Par exemple, si $l = (0, 1, 5, -59)$, la longueur de $l$ est 4. Par convention, la longueur de la liste vide est 0. Écrire une fonction python récursive `longueur` qui étant donné une liste, en renvoie la longueur.

{{ IDE('scripts/1 - liste_algo/longueur') }}


### Appartenance à une liste

Écrire une fonction python récursive `appartient` qui étant donné une liste `l` et un élément `e`, renvoie `True` si et seulement si l'élément `e` est un des éléments de la liste `l`. 

{{ IDE('scripts/1 - liste_algo/appartient') }}


### Nombre d'occurrences

Écrire une fonction python récursive `nombre_occurrences` qui étant donné une liste `l` et un élément `e` détermine le nombre d'occurrences de l'élément `e` dans la liste `l`.

Quelle est l'avantage de l'instruction `appartient(l, e)` par rapport à l'instruction `nombre_occurrences(l, e) == 0` pour déterminer si l'élément `e` appartient à la liste `l` ?

{{ IDE('scripts/1 - liste_algo/nombre_occurrences') }}


### Somme des éléments d'une liste

Écrire une fonction python récursive `somme` qui étant donnée une liste `l` d'entiers non vide en renvoie la somme. 

{{ IDE('scripts/1 - liste_algo/somme') }}


### Maximum des éléments d'une liste

Écrire une fonction `maximum2` qui étant donné deux entiers `a` et `b` renvoie le plus grand entier parmi `a` et `b`.

En déduire une fonction python récursive `maximum` qui étant donnée une liste d'entiers non vide en renvoie le plus grand. 

{{ IDE('scripts/1 - liste_algo/maximum') }}


## Pour aller plus loin


### Suppression d'un élément

Écrire une fonction `supprime` qui prend en argument une liste `l` et un entier `e`, et renvoie la liste des éléments de `l` dans laquelle la première occurrence de `e` a été supprimée. On suppose que l'élément `e` appartient à la liste `l`. La liste initiale ne sera pas modifiée. 

{{ IDE('scripts/1 - liste_algo/supprime') }}


### Insérer dans une liste

Écrire une fonction `insere` qui étant donné une liste `l`, un élément `e` et un entier `i` renvoie la liste constituée des éléments de `l` dans lequel l'élément `e` a pour indice `i`. On supposera que `i` est un entier tel que $0 \leq i \leq  \texttt{len}(l)$. Aucun comportement particulier n'est attendu dans le cas où `i` ne vérifierait pas ces conditions. La liste initiale `l` ne sera pas modifiée.

{{ IDE('scripts/1 - liste_algo/insere') }}


### Concaténation de deux listes

Si $l_1$ et $l_2$ sont deux listes, on appelle **concaténation de $l1$ avec $l2$** la liste constituée des éléments de $l_1$ puis des éléments de $l_2$. Par exemple, la concaténation des listes $l_1 = (0, 1, 6)$ et $l_2 = (9, 4, 1)$ est la liste $(0, 1, 6, 9, 4, 1)$ ; par contre la concaténation des listes $l_{2}$ et $l_1$ (dans cet ordre) est la liste $(9, 4, 1, 0, 1, 6)$. La concaténation de la liste vide avec la liste $l_1$ est la liste $(0, 1, 6)$.

Écrire une fonction python récursive `concatene` qui étant donné deux listes `l1` et `l2` renvoie la concaténation de ces deux listes. Les listes `l1` et `l2` ne seront pas modifiées.

{{ IDE('scripts/1 - liste_algo/concatene') }}


### Division de listes

<div class="exo" markdown="1">
1.  Écrire une fonction `est_2ton` qui prend en argument une liste `l`  et qui renvoie `True` si et seulement si la liste `l` est constituée d'exactement deux éléments.
2.  On souhaite écrire une fonction `divise` qui divise en deux une liste `l`. Les longueurs des deux listes résultantes doivent différer au plus d'un, et tous les éléments de la liste `l` doivent se retrouver dans l'une uniquement des deux listes de sortie. Ainsi si `divise(l)`, renvoie les listes `l1` et `l2`, on a : `concatene(l1, l2)` qui contient les mêmes éléments que `l`.
    1.  `l` est la liste $(1, 2, 3, 4, 5)$. Parmi les propositions ci-dessous, déterminer celle qui peuvent-être renvoyées par l'instruction `divise(l)` :
        
        -   `l1 = (1, 2, 3)`, `l2 = (3, 4, 5)`
        -   `l1 = (1, 3, 5)`, `l2 = (2, 4)`
        -   `l1 = (1, 3)`, `l2 = (2, 4)`
        -   `l1 = (1, 3, 4, 5)`, `l2 = (2)`
        
        Justifier votre réponse.
    
    2.  En déduire une fonction divise qui réponde au problème posé. Pour le cas général, on remarquera que si une liste `l` est constituée d'au moins trois éléments, on peut la diviser en deux de la manière suivante :
        -   on met à part les deux premiers éléments `x1` et `x2` de la liste `l` ;
        -   on divise récursivement en deux listes `l1` et `l2`  les éléments restants ;
        -   on ajoute à `l1` l'élément `x1` et à `l2` l'élément `x2`.
</div>

{{ IDE('scripts/1 - liste_algo/divise') }}


## Pour les plus fous

L'objectif ici est de déterminer l'ensemble des parties d'un ensemble $E$ à $n$ éléments, que l'on note $\mathcal{P}(E)$. Par exemple, si l'ensemble $E$ est $\{1, 2, 3\}$, l'ensemble des parties de $E$ est :

$$
\mathcal{P}(E) = \{ \{\}, \{1\}, \{2\}, \{3\}, \{1, 2\}, \{1, 3\}, \{2, 3\}, \{1, 2, 3\}\}
$$

On remarquera plusieurs choses :

-   les éléments de $\mathcal{P}(E)$ sont des ensembles ;
-   $\emptyset \in \mathcal{P}(E)$ (on note parfois l'ensemble vide $\{\}$ ou $\emptyset$) et $E \in \mathcal{P}(E)$ ;

<div class="exo" markdown="1">
1.  Si $E = \emptyset$, que vaut $\mathcal{P}(E)$ ?
2.  Soit $E = \{1, 2, 3\}$, et soit $e = 3$. 
    1.  Déterminer la liste de tous les sous-ensembles de $E$ qui ne contiennent pas $e$. On note cette liste $E'$.
    2.  Déterminer la liste de tous les sous-ensembles de $E$ qui contiennent $e$. On note cette liste $E''$.
    3.  Écrire $E$ en fonction de $E'$ et de $E''$. On pourra utiliser les opérations ensemblistes $\cup$ et $\cap$.
3.  Déduire des fonctions suivantes une fonction python `liste_sous_ensembles` qui étant donné un ensemble $E$, détermine la liste de tous ses sous-ensembles. On utilisera pour cela le type python `list`, ainsi que les méthodes `pop`, et `append`. On supposera que tous les éléments des listes manipulées sont uniques.
4.  Si $E$ possède $n$ éléments, combien d'éléments possède $\mathcal{P}(E)$ ?
</div>

{{ IDE('scripts/1 - liste_algo/liste_sous_ensembles') }}


## Documents

-   [Listes et algorithmes récursifs (sujet)](1 - liste_algo_sujet.pdf)
-   Fichiers python :
    -   [templates](scripts/1 - liste_algo/tp.py)
    -   [Module Liste.py](scripts/1 - liste_algo/Liste.py)

