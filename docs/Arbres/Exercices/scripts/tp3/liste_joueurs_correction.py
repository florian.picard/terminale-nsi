def liste_joueurs(a):
    """ Arbre -> [str]
    Renvoie la liste des joueurs ayant participé à la compétition d'arbre a """
    if est_vide(a):
        return []
    elif est_feuille(a):
        return [etiquette(a)]
    else:
        return liste_joueurs(gauche(a)) + liste_joueurs(droit(a))
