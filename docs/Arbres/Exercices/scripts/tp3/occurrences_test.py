def test_occurrences():
    """ Tests pour la fonction occurrences """
    assert occurrences(a, "Kamel") == 3
    assert occurrences(a, "Carine") == 2
    assert occurrences(a, "Joris") == 1
    assert occurrences(a, "Abdou") == 1
    assert occurrences(a, "Ada") == 0
    print("Tests de occurrences passés avec succès.")
    return True

# from tp import occurrences
test_occurrences()
