import random

def genere_RSA(p, q):
    """ int, int -> (int, int), (int, int)
    Génère un couple de clé privée/clé publique pour RSA """
    n = p*q
    phi = (p - 1)*(q - 1)
    e = random.choice([i for i in range(phi) if premiers_entre_eux(i, phi)])
    d = inverse_modulo(phi, e)
    return (n, e), (n, d)
