def rendu_monnaie_rec(systeme, montant):
    """ [int], int -> int
    montant >= 0
    Renvoie le nombre de pièces minimales à utiliser pour rendre
le montant avec les pièces de systeme """
    if ...:
        ...
    else:
        # on détermine de manière récursive le nombre minimum
        # de pièce à rendre pour rendre montant - p, ou p est
        # une des pièces du systeme.
        liste = []
        for p in systeme:
            ... # à compléter
        # On renvoie le minimum de la liste, plus 1.
        ...
