def decale_car(car, cle):
    """ str, int -> str
    Si car est une majuscule,
    renvoie le caractère correspondant à car, décalé de cle """
    if not ord('A') <= ord(car) <= ord('Z'):
        return car
    i = (ord(car) - ord('A') + cle) % 26
    return chr(i + ord('A'))
