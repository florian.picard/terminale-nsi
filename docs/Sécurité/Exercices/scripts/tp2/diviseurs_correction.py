def diviseurs_premiers(n):
    """ int -> {int}
    Renvoie l'ensemble des diviseurs de n """
    return {p for p in nombres_premiers(n) if n%p == 0}
