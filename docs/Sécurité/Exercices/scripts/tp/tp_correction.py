def code_ascii(texte):
    """ str -> [int]
    Renvoie la liste des codes ascii des caractères du texte """
    return [ord(c) for c in texte]

def decode_ascii(codes):
    """ [int] -> str
    Renvoie la chaîne de caractères correspondant aux codes ascii """
    return "".join([chr(c) for c in codes])

def genere_alphabet_majuscule():
    """ () -> [str]
    Renvoie la liste des 26 lettres de l'alphabet latin en majuscule """
    return [chr(i) for i in range(ord('A'), ord('Z') + 1)]

def decale_car(car, cle):
    """ str, int -> str
    Si car est une majuscule,
    renvoie le caractère correspondant à car, décalé de cle """
    if not ord('A') <= ord(car) <= ord('Z'):
        return car
    i = (ord(car) - ord('A') + cle) % 26
    return chr(i + ord('A'))

def chiffre_cesar(clair, cle):
    """ str, int -> str
    Chiffre texte avec le chiffrement de César (clé = décalage) """
    chiffre = ""
    for c in clair:
        chiffre += decale_car(c, cle)
    return chiffre

def dechiffre_cesar(chiffre, cle):
    """ str, int -> str
    Renvoie le texte clair vérifiant chiffre = chiffre_cesar(clair, cle) """
    clair = ""
    for c in chiffre:
        clair += decale_car(c, -cle)
    return clair

def chiffre_vigenere(clair, cle):
    """ str, [int] -> str
    Chiffrement de Vigenère """
    s = ""
    for i in range(len(clair)):
        c = clair[i]
        s += chiffre_cesar(c, cle[i%len(cle)])
    return s

