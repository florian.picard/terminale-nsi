class OrdonnanceurFIFO:
    """Représente un ordonnanceur"""
    def __init__(self, q=1):
        """OrdonnanceurFIFO, int -> Nonetype"""
        self.chrono = Chronogramme()
        self.file_processus = File()
        self.quantum = q

    def ajoute_processus(self, p_list):
        """ OrdonnanceurFIFO, [Processus] -> Nonetype
        Ajoute à self.file_processus les processus de p_list par ordre d'arrivée """
        for _ in range(len(p_list)):
            premier = renvoie_et_supprime_premier(p_list)
            self.file_processus.enfiler(premier)
    
    def exec_quantum(self):
        """ OrdonnanceurFIFO -> Nonetype
        Simule l'exécution d'un quantum """
        tps = self.chrono.temps_ecoule()
        p = self.file_processus.examine()
        self.chrono.enregistrer(p, self.quantum)
        p.avancer(self.quantum, tps)
    
        if p.est_termine():
            self.file_processus.defiler()
    
    def exec(self):
        """ OrdonnanceurFIFO -> Chronogramme
        Simule l'exécution des processus de la file et en renvoie le chronogramme """
        while not self.file_processus.est_vide():
            self.exec_quantum()
        return self.chrono
