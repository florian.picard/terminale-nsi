---
title: Bienvenue
---

Les codes écrits en cours se trouvent sur [github](https://github.com/marzikill/nsi-codes). Vous pouvez partager en cours vos codes via [l'espace d'écriture collaboratif](https://codimd.apps.education.fr/EV3QqVm4Tcyo0OcWul2d3w#).


## Libertés fondamentales

Les quatre libertés fondamentales du programmeur sont :

-   la liberté de faire fonctionner le programme comme vous voulez, pour n'importe quel usage (liberté 0) ;
-   la liberté d'étudier le fonctionnement du programme, et de le modifier pour qu'il effectue vos tâches informatiques comme vous le souhaitez (liberté 1) ; l'accès au code source est une condition nécessaire ;
-   la liberté de redistribuer des copies, donc d'aider les autres (liberté 2) ;
-   la liberté de distribuer aux autres des copies de vos versions modifiées (liberté 3) ; en faisant cela, vous donnez à toute la communauté une possibilité de profiter de vos changements ; l'accès au code source est une condition nécessaire.


## Éditeurs python

{{ IDE('intro') }}

Il y a d'autres moyens d'exécuter du code python (listés par ordre de préférence personnelle) :

-   [Thonny](https://thonny.org/) : l'éditeur python utilisé en cours. Toutes les corrections, déboggages et analyse de code seront effectuées sur cet éditeur. Si vous n'avez pas les droits pour l'installer sur votre machine, utiliser la [version portable](https://github.com/thonny/thonny/releases/download/v4.1.2/thonny-4.1.2-windows-portable.zip).
-   [basthon](https://basthon.fr/) : un éditeur en ligne. Préferer l'outil "Console" pour l'écriture de code, l'utilisation de notebook demande une certaine expertise&#x2026;
-   capytale : accessible sur l'ENT du lycée dans l'onglet "Mes applications", pour le moment indisponible.
-   [vscode](https://code.visualstudio.com/) : un éditeur de code professionnel, très populaire et multiplateforme. Beaucoup de plugins sont développés par la communauté et le rendent très efficace dans tout type de situation.


## Progression : les thèmes de l'année

En NSI en terminale, le programme est exigent et plein de nouveautés.


### Programmation orientée objet


### Fonctions récursives


### Structure de données : les listes


### Bases de données : modèle relationnel et langage SQL

-   [Denormalizing DB for Justin Bieber](https://youtube.com/shorts/OW85ZAaQBSE?si=1AYrrfEldSUMJpo5)


### Structure de données : les piles et les files


### Système d'exploitation et processus


### Structures de données : les arbres


### Réseaux informatiques, protocoles de routage


### Structures de données : les graphes


### Sécurité des communications

-   [Je malmène ta prod en direct avec 15 failles de sécu](https://www.youtube.com/watch?v=2MQ7F3oq0wc)


### Algorithmes avancés


## Les épreuves du bac


### Épreuve écrite.

Durée : 3h30. Trois (?) exercices sur feuille, au mois de juin 2024, portant sur tout ou partie du programme de NSI de l'année de terminale et de première. 


### Épreuve pratique.

Durée : 1h. Composée de deux exercices : ils sont tous accessibles en ligne **avant** les épreuves.

-   exercice 1 : "programmer un algorithme figurant explicitement au programme, ne présentant pas de difficulté particulière, dont on fournit une spécification"
-   exercice 2 : compléter un programme pour résoudre un problème.

Ressources :

-   Site officiel de l'éducation nationale. <https://cyclades.education.gouv.fr/delos/public/listPublicECE>
-   Version officieuse et améliorée par Gilles Lassus : <https://glassus.github.io/terminale_nsi/T6_6_Epreuve_pratique/BNS_2023/>
-   E-nsi : <https://e-nsi.forge.aeif.fr/pratique/>


### Grand oral

L’épreuve du Grand Oral est une épreuve du baccalauréat qui nécessite pendant l’année, de prépare deux "questions" en lien avec les enseignements de spécialité. Le jour de l'oral, le jury choisit une des deux questions que le candidat présente a l'oral. 

Cette présentation dure 10 minutes, pendant lesquelles le candidat expose les motivations qui l’ont conduit à choisir cette question, puis présente la réponse qu’il a élaborée.

À l’issue de ces 10 premières minutes, le candidat et le jury échangent durant 10 minutes. A cette occasion, le jury amène le candidat à préciser et approfondir sa pensée. C’est une opportunité à saisir, pour apporter des compléments à son propos, et montrer sa capacité à écouter, dialoguer, expliciter et argumenter.

Cette épreuve nécessite de s'interroger tout au long de l'année sur les cours, de vous poser des questions existentielles, et d'essayer d'aller un peu plus loin sur les questions qui vous intéressent. Il s'agit principalement une épreuve de curiosité et de passion. Pour cela les projets personnels et l'autonomie tout au long de l'année (en faisant des exercices présents sur [e-nsi](https://e-nsi.forge.aeif.fr/pratique/) par exemple) vous donneront une idée des thèmes/sujets que vous pouvez aborder. 

Sur l'année :

-   premier trimestre : être curieux ;
-   second trimestre : être curieux et commencer à réfléchir à un thème, une question ;
-   troisième trimestre : préparer effectivement deux questions


## Formation en ligne

**Énormément** de ressources sont disponibles en ligne, et permettent d'étudier l'informatique en autonomie. 


### Sécurité

-   [Hackropole](https://hackropole.fr/fr/) : une plateforme regroupant toutes les épreuves du France Cybersecurity Challenge (FCSC) organisé par l'ANSSI (Agence nationale de la sécurité des systèmes d'information)
-   [The OSINT project (TOP)](https://the-osint-project.fr/) : une plateforme (en français) d'introduction à l'OSINT (Open Source INTelligence : renseignement en source ouverte).
-   Le [wiki](https://wiki.nobrackets.fr/) et le [discord](https://discord.gg/68T7cnFYeK) du CTF NoBrackets, organisé par l'ENSIBS (École Nationale Supérieure d'Ingénieurs de Bretagne Sud)
-   [PicoCTF](https://picoctf.org/) : une compétition de type CTF (en anglais), destinée aux lycéens et lycéennes. Des ressources (tutoriels, le livre <span class="underline">The CTF Primer</span>, et *beaucoup* d'exemples de challenges) sont disponibles sur la plateforme, et en font un endroit idéal pour s'entraîner à résoudre des problèmes de cybersécurité.
-   [Rootme](https://www.root-me.org/) : plus avancé, mais une référence dans le monde de la cybersécurité.
-   [Tryhackme](https://tryhackme.com) (essayer *Introduction to Cyber Security*) : un bon site d'introduction aux problématiques de cybersécurité.
-   [Hackthebox](https://www.hackthebox.com/) : pour les experts !
-   [cryptohack](https://cryptohack.org/) : à tester ?


### Programmation

-   [CodEx](https://codex.forge.apps.education.fr/) (anciennement [e-nsi](https://e-nsi.forge.aeif.fr/pratique/)) : pour s'entraîner en autonomie sur le programme de terminal. Un must !
-   [france-ioi](https://www.france-ioi.org/algo/chapters.php) : le site de l'association française d'informatique, célèbre pour les multiples concours qu'elle organise (Castor, Algoréa, Alkindi&#x2026;). Il est possible de suivre les cours disponibles sur le site et de s'entraîner sur les exercices.
-   [PyDefis](https://pydefis.callicode.fr/user/liste_defis) Une liste de challenges de programmation, en français.
-   [Advent Of Code](https://adventofcode.com/) : des exercices de difficulté variée pour s'entraîner à la programmation pendant les vacances de Noël.
-   [hackerrank](https://www.hackerrank.com/) : un bon site pour s'initier à des sujets variés.
-   [codingame](https://www.codingame.com/start/) : un site de compétitions informatiques
-   [leetcode](https://leetcode.com/) : pour s'entraîner à la programmation, une mine sans fond de problèmes algorithmiques. Certains sont tout à faits intéressants pour des élèves de terminale, d'autres sont destinés à préparer des entretiens de programmation.


### Expressions régulières

Cet outil n'est pas au programme de NSI mais est indispensable dans la vie quotidienne d'un programmeur.
Les expressions régulières permettent de rechercher des motifs ("patterns" en anglais) dans une chaine de caractères.
Elles sont particulièrement utile pour résoudre les challenges de programmation.

-   [Regex Learn](https://regexlearn.com/) (anglais) : un tutoriel sur les expressions régulières
-   [RegexOne](https://regexone.com/) (anglais) : tutoriel et exercices sur les expressions régulières.
-   [Regex Tutorials](http://regextutorials.com/index.html) : encore plus de tutoriels, encore plus d'exercices.
-   [Regex (HackerRank)](https://www.hackerrank.com/domains/regex) : des exercices sur les expressions régulières, appliquées à Python (module `re`).
-   [regexr](https://regexr.com/), [regex101](https://regex101.com/) : interface en ligne "intuitive" pour construire et tester des expressions rationnelles.


### Commandes Linux

-   [Terminus](http://luffah.xyz/bidules/Terminus/) : un jeu dans un terminal UNIX.
-   <http://nsi42.net/> : un jeu développé par des élèves de NSI
-   [GameShell](https://github.com/phyver/GameShell) : un jeu à installer sur votre ordinateur personnel


## Concours informatiques en ligne

Une foule de concours sont dédiés aux élèves de NSI ou de SNT. Parmi les plus intéressants, citons :

<table border="2" cellspacing="0" cellpadding="6" rules="groups" frame="hsides">


<colgroup>
<col  class="org-left" />

<col  class="org-left" />

<col  class="org-left" />

<col  class="org-left" />

<col  class="org-left" />
</colgroup>
<thead>
<tr>
<th scope="col" class="org-left">Nom</th>
<th scope="col" class="org-left">Type</th>
<th scope="col" class="org-left">Niveau</th>
<th scope="col" class="org-left">Date</th>
<th scope="col" class="org-left">Adresse</th>
</tr>
</thead>

<tbody>
<tr>
<td class="org-left">Algorea</td>
<td class="org-left">Programmation</td>
<td class="org-left">Seconde</td>
<td class="org-left">Janvier</td>
<td class="org-left"><a href="https://algorea.org/#/">https://algorea.org/#/</a></td>
</tr>


<tr>
<td class="org-left">Alkindi</td>
<td class="org-left">Cryptographie</td>
<td class="org-left">Seconde</td>
<td class="org-left">~Janvier</td>
<td class="org-left"><a href="https://concours-alkindi.fr/main.html#/">https://concours-alkindi.fr/main.html#/</a></td>
</tr>


<tr>
<td class="org-left">Nobrackets ctf</td>
<td class="org-left">CTF</td>
<td class="org-left">Terminale</td>
<td class="org-left">11-12 Novembre</td>
<td class="org-left"><a href="https://discord.gg/68T7cnFYeK">https://discord.gg/68T7cnFYeK</a></td>
</tr>


<tr>
<td class="org-left">CSAW'23</td>
<td class="org-left">CTF</td>
<td class="org-left">Terminale</td>
<td class="org-left">\(\rightarrow\) 16 Octobre</td>
<td class="org-left"><a href="https://esisar.grenoble-inp.fr/fr/l-ecole/red-team">https://esisar.grenoble-inp.fr/fr/l-ecole/red-team</a></td>
</tr>


<tr>
<td class="org-left">La nuit du code</td>
<td class="org-left">Programmation</td>
<td class="org-left">Première Terminale</td>
<td class="org-left">Mai</td>
<td class="org-left"><a href="https://www.nuitducode.net/">https://www.nuitducode.net/</a></td>
</tr>


<tr>
<td class="org-left">Passe ton hack</td>
<td class="org-left">CTF</td>
<td class="org-left">Lycée</td>
<td class="org-left">Avril</td>
<td class="org-left"><a href="https://www.defense.gouv.fr/ema/actualites/passe-ton-hack-dabord">https://www.defense.gouv.fr/ema/actualites/passe-ton-hack-dabord</a></td>
</tr>
</tbody>
</table>

