def est_pair(bits):
    """ [int] -> bool
    len(bits) > 0
    Détermine si le nombre dont l'écriture en base 2 est donnée par le tableau bits est pair.  """
    pass
def est_plus_grand_kbits(bits):
    """ [int] -> bool
    k = len(bits) > 0
    Détermine si le tableau bits correspond au plus  grand entier que l'on  puisse écrire sur k bits.  """
    pass
def appartient(tab, e):
    """ [int], int -> bool
    Détermine si l'élément e est présent dans le tableau tab.  """
    pass
def nombre_occurrences(tab, e):
    """ [int], int -> int
    Détermine le nombre d'éléments de tab qui sont égaux à e.  """
    pass
def indices_occurrences(tab, e):
    """ [int], int -> [int]
    Détermine les indices des occurrences de e dans le tableau tab.  """
    pass
def maximum(tab):
    """ [int], int -> int, int
    len(tab) > 0
    Détermine le maximum des éléments de tab, ainsi que le premier indice pour lequel ce maximum est atteint.  """
    pass
def tri_selection(tab):
    """ [int] -> [int]
    Trie en place le tableau tab, à l'aide de l'algorithme du tri par sélection """
    pass
def est_pair(bits):
    """ [int] -> bool
    len(bits) > 0
    Détermine si le nombre dont l'écriture en base 2 est donnée par le tableau bits est pair.  """
    pass
