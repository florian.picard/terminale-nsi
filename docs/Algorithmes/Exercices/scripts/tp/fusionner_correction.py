def fusionner(l1, l2):
    """ Liste, Liste
    l1 et l2 sont triées par ordre croissant
    Renvoie la liste des éléments de l1 et l2 triés """
    if est_vide(l1):
        return l2
    elif est_vide(l2):
        return l1
    else:
        if tete(l1) < tete(l2):
            reste = fusionner(queue(l1), l2)
            return ajoute(reste, tete(l1))
        else:
            reste = fusionner(l1, queue(l2))
            return ajoute(reste, tete(l2))
