def chiffre_vigenere(clair, cle):
    """ str, [int] -> str
    Chiffrement de Vigenère """
    s = ""
    for i in range(len(clair)):
        c = clair[i]
        s += chiffre_cesar(c, cle[i%len(cle)])
    return s
