def existe_chemin(G, sommet, destination):
    """ Graphe, Sommet, Sommet -> bool 
    sommet != destination
    Renvoie True ssi il existe un chemin entre source et destination dans G. """
    atteignables = parcours_profondeur(G, sommet)
    return destination in atteignables
