def premier(l):
    return tete(l)

def second(l):
    return tete(queue(l))

def est_2ton(l):
    if est_vide(l) or est_singleton(l):
        return False
    return est_vide(queue(queue(l)))

def divise(l):
    """ liste -> liste, liste
    Divise en deux listes la liste l """
    if est_vide(l):
        return creer_vide(), creer_vide()
    elif est_singleton(l):
        return l, creer_vide()
    elif est_2ton(l):
        return singleton(premier(l)), singleton(second(l))
    else:
        l1, l2 = divise(queue(queue(l)))
        return ajoute(l1, premier(l)), ajoute(l2, second(l))
