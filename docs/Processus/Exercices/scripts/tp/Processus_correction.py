class Processus:
    """Représente un processus"""
    def __init__(self, pid, arrivee, duree):
        """Processus, int, int, int -> Nonetype"""
        self.pid = pid
        self.arrivee = arrivee
        # self.debut = -1
        # self.terminaison = -1
        self.duree = duree
        self.duree_restante = duree

    def __repr__(self):
        """ Processus -> str
        Renvoie une chaine de caractère représentant le processus self """
        return f"<P{self.pid}>"
    
    def avancer(self, d, t):
        """ Processus, int, int -> Nonetype
        Le processus (re)prend son exécution à la date t pour une durée d """
        assert t >= self.arrivee, "Impossible d'exécuter un processus inexistant"
        self.duree_restante -= d
    
    def est_termine(self):
        """ Processus -> bool
        Détermine si le processus p a terminé son exécution """
        return self.duree_restante <= 0
    
    def __init__(self, pid, arrivee, duree):
        """ Initialise un processus """
        self.pid = pid
        self.arrivee = arrivee
        self.debut = -1
        self.fin = -1
        self.duree = duree
        self.duree_restante = duree
    
    def avancer(self, d, t):
       """ Processus, int, int -> Nonetype
       Avancer un processus en mettant à jour les dates de début et de fin """
       if self.debut == -1:
          self.debut = t
       self.duree_restante -= d
       if self.est_termine():
          self.fin = d + t 
    
    def sejour(self):
        """ Renvoie le temps de séjour de self """
        if self.fin == -1:
            raise AttributeError("Le processus n'est pas terminé")
        return self.fin - self.arrivee
    
    def execution(self):
        """ Renvoie le temps d'exécution de self """
        return self.duree - self.duree_restante
    
    def attente(self):
        """ Renvoie le temps d'attente de self """
        if self.fin == -1:
            raise AttributeError("Le processus n'est pas terminé")
        return self.sejour() - self.execution()
    
    def latence(self):
        """ Renvoie le temps de latence de self """
        if self.debut == -1:
            raise AttributeError("Le processus n'a pas encore accédé au processeur")
        return self.debut - self.arrivee


p1 = Processus(1, 0, 10)
p2 = Processus(2, 3, 4)
p3 = Processus(3, 2, 8)
