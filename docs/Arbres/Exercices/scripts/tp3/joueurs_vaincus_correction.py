def joueurs_vaincus(a, nom):
    """ Arbre, str -> [str]
    Renvoie la liste des joueurs vaincus par nom dans la compétition d'arbre a """
    if etiquette(a) == nom:
        return vaincus_gagnant(a)
    else:
        n = recherche_noeud(a, nom)
        return vaincus_gagnant(n)
