def nombre_chemins_dyn(n, m):
    """ int, int -> int
    Renvoie le nombre de chemins de (haut, gauche) à (bas, droite) dans une grille n×m """
    T = [ [None for j in range(m)] for i in range(n)]
    for i in range(n):
        T[...][...] = 1
    for j in range(m):
        ...
    for i in range(1, n):
        for j in range(1, m):
            T[i][j] = ...
    return ...
