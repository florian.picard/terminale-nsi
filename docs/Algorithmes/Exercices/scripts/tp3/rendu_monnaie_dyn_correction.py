def rendu_monnaie_dyn(systeme, montant):
    """ [int], int -> int
    Renvoie le nombre minimum de pièces de systeme à utiliser pour rendre montant """
    # dans le pire des cas on rend i avec i pièces de 1
    T = [i for i in range(montant + 1)] 
    for i in range(montant + 1):
        for p in systeme:
            if i + p <= montant and T[i + p] > T[i] + 1:
                T[i + p] = T[i] + 1
    return T[montant]
