def test_est_plus_grand_kbits():
    # Tests pour la fonction est_plus_grand_kbits
    for i in range(1024):
        bits = list(map(int, list(bin(i)[2:])))
        reponse = est_plus_grand_kbits(bits) 
        expected = all(bits)
        assert reponse == expected, f"\ Le test suivant n'a pas été validé.\nEntrée : {bits}\nSortie : {reponse}\nAttendu : {expected}"
    return True

test_est_plus_grand_kbits()
