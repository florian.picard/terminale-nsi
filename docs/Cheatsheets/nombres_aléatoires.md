---
title: Module random
---

Le module `random` implémente des générateurs de nombres pseudo-aléatoires pour différentes distributions. On l'importe à l'aide de l'instruction :

``` py linenums="1" 
import random    
```


## Fonction `randbytes`

Génère `n` octets aléatoires. Cette méthode ne doit pas être utilisée pour générer des jetons de sécurité. 

``` py linenums="1" 
octets = random.randbytes(10)
print(octets)
print(list(octets))
```

```
b'\x9fa\xd3"\rv.\xf8\x99\xb3'
[159, 97, 211, 34, 13, 118, 46, 248, 153, 179]
```


## Génération aléatoire de nombres


### Fonction `randrange`

Renvoie un élément aléatoire depuis `range(start, stop, step)` (`stop` est exclu).

-   `randrange(stop)` sélectionne un nombre aléatoirement depuis `range(stop)`
-   `randrange(start, stop)` sélectionne un nombre aléatoirement depuis `range(start, stop)`

``` py linenums="1" 
liste_essais = [random.randrange(4, 10, 2) for _ in range(10)]
print(liste_essais)
```

```
[8, 6, 4, 8, 8, 6, 6, 6, 8, 8]
```


### Fonction `randint`

Renvoie un entier aléatoire `N` tel que `a <= N <= b`. Alias pour `randrange(a, b+1)~`.

``` py linenums="1" 
print(random.randint(0, 10))    
```

```
9
```


## Listes et aléatoire

Les fonctions suivantes s'appliquent **uniquement aux listes**.


### Choix dans une séquence


#### Fonction `choice`

Renvoie un élément aléatoire de la séquence non vide `seq`. Si `seq` est vide, lève `IndexError`.

``` py linenums="1" 
liste = ["maths", "nsi", "physique"]    
print(random.choice(liste))
```

```
maths
```


#### Fonction `choices`

Renvoie une liste de taille `k` d'éléments choisis dans la `liste` avec remise. Si la `liste` est vide, lève `IndexError`.

``` py linenums="1" 
print(random.choices(liste, k = 3))
```

```
['physique', 'physique', 'maths']
```


#### Fonction `sample`

Renvoie une liste de taille `k` d'éléments `uniques` choisis depuis la `liste` sans remise. Si `liste` est vide, lève `IndexError`. 

``` py linenums="1" 
print(random.sample(liste, k = 3))
```

```
['nsi', 'maths', 'physique']
```


### Mélange d'une séquence

Mélange la `liste` sans créer de nouvelle instance (`liste` est modifiée sur place).

``` py linenums="1" 
liste = ["alice", "boubakar", "calice"]    
random.shuffle(liste)    
print(liste)
```

```
['alice', 'boubakar', 'calice']
```

