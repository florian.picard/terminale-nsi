def affiche_infixe(a):
    """ Arbre -> Nonetype
    Affiche l'arbre a de manière infixe """
    if est_vide(a):
        pass
    else:
        print("(", end = "")
        affiche_infixe(gauche(a))
        print(f"{etiquette(a)}", end = "")
        affiche_infixe(droit(a))
        print(")", end = "")
